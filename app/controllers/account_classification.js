'use strict';

const dataDao = require('../daos/account_classification');

const async = require('async');
const _ = require("lodash");

function AccountClassification() {
    this.dataDao = dataDao;
}

AccountClassification.prototype.getAllAccountClassification = (params, next) => {
  dataDao.getAllAccountClassification(params, (err, response) => {
      if (err) {
          return next({
              msg: err.msg,
              result: err,
              success: false
          }, null)
      }

      return next(null, {
          msg: '',
          result: response,
          success: true
      });
  });
};

exports.AccountClassification = AccountClassification;
