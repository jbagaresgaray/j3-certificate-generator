'use strict';

const dataDao = require('../daos/account_groups');

const async = require('async');
const _ = require("lodash");

function AccountGroups() {
    this.dataDao = dataDao;
}

AccountGroups.prototype.getAllAccountGroups = (params, next) => {
  dataDao.getAllAccountGroups(params, (err, response) => {
      if (err) {
          return next({
              msg: err.msg,
              result: err,
              success: false
          }, null)
      }

      return next(null, {
          msg: '',
          result: response,
          success: true
      });
  });
};

exports.AccountGroups = AccountGroups;
