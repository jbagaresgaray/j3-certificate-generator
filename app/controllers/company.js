'use strict';

const dataDao = require('../daos/company');

const async = require('async');
const _ = require("lodash");

function Company() {
    this.dataDao = dataDao;
}

Company.prototype.createCompany = (user, data, next) => {
    async.waterfall([
        (callback) => {
            dataDao.checkCompanyName(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Company already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            dataDao.createCompany(user, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully saved!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Company.prototype.updateCompany = (companyId, data, next) => {
    async.waterfall([
        (callback) => {
            dataDao.checkCompanyNameOnUpdate(companyId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Company already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            dataDao.updateCompany(companyId, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully updated!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Company.prototype.getCompanyDetails = (companyId, next) => {
    dataDao.getCompanyDetails(companyId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Company.prototype.deleteCompany = (companyId, next) => {
    async.waterfall([
        callback => {
            dataDao.isCompanyHasTrainee(companyId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (!_.isEmpty(response)) {
                    return next(null, {
                        msg: 'WARNING! Unable to delete, Company already have Traiee records!',
                        result: null,
                        success: false
                    });
                } else {
                    return callback();
                }
            });
        },
        callback => {
            dataDao.deleteCompany(companyId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                return callback(null, {
                    msg: 'Record successfully deleted!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Company.prototype.getAllCompanies = (params, next) => {
    dataDao.getAllCompanies(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

exports.Company = Company;