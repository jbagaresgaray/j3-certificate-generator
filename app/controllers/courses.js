'use strict';

var dataDao = require('../daos/courses');

var async = require('async');

function Courses() {
    this.dataDao = dataDao;
}

Courses.prototype.createCourse = (user, data, next) => {
    /* async.waterfall([
        (callback) => {
            dataDao.checkCourseCode(data, (err, response) => {
                if (err) {
                    console.log("err: ", err);
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Course Code already existed',
                        result: null,
                        success: false
                    });
                }
                return callback();
            });
        },
        (callback) => {
            
        }
    ], next); */

    dataDao.createCourse(user, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: 'Record successfully saved!',
            result: response,
            success: true
        });
    });
};

Courses.prototype.updateCourse = (courseId, data, next) => {
    /* async.waterfall([
        (callback) => {
            dataDao.checkCourseCodeUpdate(courseId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Course Code already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            
        }
    ], next); */

    dataDao.updateCourse(courseId, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: 'Record successfully updated!',
            result: response,
            success: true
        });
    });
};

Courses.prototype.getCourseDetails = (courseId, next) => {
    dataDao.getCourseDetails(courseId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Courses.prototype.deleteCourse = (courseId, next) => {
    dataDao.deleteCourse(courseId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: 'Record successfully deleted!',
            result: response,
            success: true
        });
    });
};

Courses.prototype.getAllCourses = (params, next) => {
    dataDao.getAllCourses(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

exports.Courses = Courses;