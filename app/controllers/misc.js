'use strict';

const dataDao = require('../daos/misc');
const async = require('async');

function Misc() {
    this.dataDao = dataDao;
}

Misc.prototype.getDashboardReport = (next) => {
    dataDao.getDashboardReport((err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

exports.Misc = Misc;
