"use strict";

const dataDao = require("../daos/trainee");
const config = require("../../config/config");
const func = require("../utils/functions");

const async = require("async");
const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");
const sharp = require("sharp");
const AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: config.SPACES_ACCESS_KEY_ID,
  secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint
});


function Trainee() {
  this.dataDao = dataDao;
}

Trainee.prototype.uploadTraineeImage = async (uuid, data, next) => {
  const pathThumb = path.resolve("./public/uploads/trainee/");
  const pathResizeThumb = path.resolve("./public/uploads/trainee_resize/");

  if (!fs.existsSync(pathThumb)) {
    try {
      mkdirp.sync(pathThumb);
      console.log("pathThumb directory created");
    } catch (error) {
      console.log("pathThumb err: ", error);
    }
  }

  if (!fs.existsSync(pathResizeThumb)) {
    try {
      mkdirp.sync(pathResizeThumb);
      console.log("pathResizeThumb directory created");
    } catch (error) {
      console.log("pathResizeThumb err: ", error);
    }
  }


  const fileName = "400_" + data.traineeId + "_" + data.img_name;
  const fileObj = {
    traineeId: data.traineeId,
    img_name: fileName,
    img_type: data.img_type,
    img_size: data.img_size
  };
  const savePath = pathResizeThumb + "/" + fileName;
  const fileKey = config.PCLOUD_APP_FOLDER + "/" + config.PCLOUD_APP_TRAINEE_FOLDER + "/" + fileName;
  await sharp(data.img_path)
    .resize(400, 400, {
      kernel: sharp.kernel.lanczos2,
      fit: sharp.fit.contain,
      withoutEnlargement: true
    })
    .toFile(savePath);

  const fileData = await fs.readFileSync(savePath);
  const params = {
    Bucket: config.SPACES_BUCKET,
    Key: fileKey,
    Body: fileData,
    ACL: "public-read"
  };
  s3.upload(params, function(err, data) {
    if (err) {
      console.log("s3.putObject: ", err);
      return next(err);
    }

    console.log("Successfully uploaded data: ", data);
    fileObj.img_path = data.Location;
    fileObj.img_key = data.key;
    fileObj.folderid = null;
    fileObj.fileid = data.ETag;

    console.log("fileObj: ", fileObj);
    dataDao.uploadTraineeImage(uuid, fileObj, (err, response) => {
      if (err) {
        return next({
            msg: err.msg,
            result: err,
            success: false
          },
          null
        );
      }
      return next(null, {
        msg: "Image successfully uploaded!",
        result: response,
        success: true
      });
    });

  });
};

Trainee.prototype.createTrainee = (user, data, next) => {
  async.waterfall(
    [
      callback => {
        dataDao.checkTraineeName(data, (err, response) => {
          if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }
          if (response && response.length > 0) {
            return next(null, {
              msg: "Trainee already existed",
              result: null,
              success: false
            });
          }
          return callback();
        });
      },
      callback => {
        dataDao.createTrainee(user, data, (err, response) => {
          if (err) {
            return callback({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }
          return next(null, {
            msg: "Record successfully saved!",
            result: response,
            success: true
          });
        });
      }
    ],
    next
  );
};

Trainee.prototype.updateTrainee = (traineeId, data, next) => {
  async.waterfall(
    [
      callback => {
        dataDao.checkTraineeNameOnUpdate(traineeId, data, (err, response) => {
          if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }

          if (response && response.length > 0) {
            return next(null, {
              msg: "Trainee already existed",
              result: null,
              success: false
            });
          }
          callback();
        });
      },
      callback => {
        dataDao.updateTrainee(traineeId, data, (err, response) => {
          if (err) {
            return callback({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }
          return next(null, {
            msg: "Record successfully updated!",
            result: response,
            success: true
          });
        });
      }
    ],
    next
  );
};

Trainee.prototype.getTraineeDetails = (traineeId, next) => {
  dataDao.getTraineeDetails(traineeId, (err, response) => {
    if (err) {
      return next({
          msg: err.msg,
          result: err,
          success: false
        },
        null
      );
    }

    if (_.isEmpty(response)) {
      return next(null, {
        msg: "No trainee information could be found on the records!",
        result: {},
        success: false
      });
    }

    return next(null, {
      msg: "",
      result: response,
      success: true
    });
  });
};

Trainee.prototype.deleteTrainee = (traineeId, next) => {
  dataDao.deleteTrainee(traineeId, (err, response) => {
    if (err) {
      return next({
          msg: err.msg,
          result: err,
          success: false
        },
        null
      );
    }

    return next(null, {
      msg: "Record successfully deleted!",
      result: response,
      success: true
    });
  });
};

Trainee.prototype.getAlltrainees = (params, next) => {
  dataDao.getAlltrainees(params, (err, response) => {
    if (err) {
      return next({
          msg: err.msg,
          result: err,
          success: false
        },
        null
      );
    }

    return next(null, {
      msg: "",
      result: response,
      success: true
    });
  });
};

Trainee.prototype.deleteTraineeImage = (uuid, data, next) => {
  dataDao.deleteTraineeImage(uuid, data, (err, response) => {
    if (err) {
      return next({
          msg: err.msg,
          result: err,
          success: false
        },
        null
      );
    }

    return next(null, {
      msg: "Image successfully deleted!",
      result: response,
      success: true
    });
  });
};

exports.Trainee = Trainee;