'use strict';

const dataDao = require('../daos/trainee_training');
const ledgerDao = require('../daos/trainee_training_ledger');
const traineeDao = require('../daos/trainee');

const async = require('async');
const _ = require('lodash');

function TraineeTraining() {
	this.dataDao = dataDao;
}



TraineeTraining.prototype.markTraineeAsPass = (uuid, trainingId, next) => {
	async.waterfall([
		(callback) => {
			ledgerDao.checkTraineeTrainingPaid(uuid, trainingId, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}
				console.log("checkTraineeTrainingPaid: ", response);

				if (response) {
					return callback();
				} else {
					return next(null, {
						msg: 'Warning. Unable to mark as pass! Trainee is currently unpaid of the selected training.',
						result: null,
						success: false
					});
				}
			});
		},
		(callback) => {
			dataDao.markTraineeAsPass(uuid, trainingId, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}

				return callback(null, {
					msg: 'Record successfully updated!',
					result: response,
					success: true
				});
			});
		}
	], next);
};

TraineeTraining.prototype.markTraineeAsAttended = (uuid, trainingId, next) => {
	async.waterfall([
		(callback) => {
			ledgerDao.checkTraineeTrainingPaid(uuid, trainingId, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}
				console.log("checkTraineeTrainingPaid: ", response);

				if (response) {
					return callback();
				} else {
					return next(null, {
						msg: 'Warning. Unable to mark as pass! Trainee is currently unpaid of the selected training.',
						result: null,
						success: false
					});
				}
			});
		},
		(callback) => {
			dataDao.markTraineeAsAttended(uuid, trainingId, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}

				return callback(null, {
					msg: 'Record successfully updated!',
					result: response,
					success: true
				});
			});
		}
	], next);
};


TraineeTraining.prototype.createTraineeTraining = (uuid, data, next) => {
	async.waterfall([
		(callback) => {
			dataDao.checkTraineeOnTraining(uuid, data.trainingId, (err, response) => {
				if (err) {
					return next(err);
				}

				if (!_.isEmpty(response)) {
					return next(null, {
						msg: 'Trainee record already added on the training!',
						result: {},
						success: false
					});
				} else {
					return callback();
				}
			});
		},
		(callback) => {
			dataDao.createTraineeTraining(uuid, data, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}

				return callback(null, {
					msg: 'Record successfully saved!',
					result: response,
					success: true
				});
			});
		}
	], next);
};

TraineeTraining.prototype.getTraineeTrainings = (uuid, next) => {
	dataDao.getTraineeTrainings(uuid, (err, response) => {
		if (err) {
			return next({
				msg: err.msg,
				result: err,
				success: false
			}, null)
		}

		return next(null, {
			msg: '',
			result: response,
			success: true
		});
	});
};

TraineeTraining.prototype.deleteTraineeTraining = (uuid, trainingId, next) => {
	async.waterfall([
		(callback) => {
			ledgerDao.checkTraineeTrainingPaid(uuid, trainingId, (err, isPaid) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}
				console.log("checkTraineeTrainingPaid: ", isPaid);

				if (isPaid) {
					return next(null, {
						msg: 'Warning. Unable to remove paid trainee',
						result: null,
						success: false
					});
				} else {
					return callback();
				}
			});
		},
		(callback) => {
			dataDao.deleteTraineeTraining(uuid, trainingId, (err, response) => {
				if (err) {
					return next({
						msg: err.msg,
						result: err,
						success: false
					}, null)
				}

				return callback(null, {
					msg: 'Record successfully updated!',
					result: response,
					success: true
				});
			});
		}
	], next);
};

TraineeTraining.prototype.getTraineeTrainingCertificate = (traineeId, trainingId, next) => {
	dataDao.getTraineeTrainingCertificate(traineeId, trainingId, (err, response) => {
		if (err) {
			return next({
				msg: err.msg,
				result: err,
				success: false
			}, null)
		}

		return next(null, {
			msg: '',
			result: response,
			success: true
		});
	});
};

TraineeTraining.prototype.getTraineeTrainingCertificates = (traineeId, next) => {
	dataDao.getTraineeTrainingCertificates(traineeId, (err, response) => {
		if (err) {
			return next({
				msg: err.msg,
				result: err,
				success: false
			}, null)
		}

		return next(null, {
			msg: '',
			result: response,
			success: true
		});
	});
};

exports.TraineeTraining = TraineeTraining;