"use strict";

const dataDao = require("../daos/trainee_training_ledger");
const config = require("../../config/config");
const func = require("../utils/functions");

const async = require("async");
const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");
const sharp = require("sharp");
const AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: config.SPACES_ACCESS_KEY_ID,
  secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint
});

function TraineeTrainingLedger() {
  this.dataDao = dataDao;
}

TraineeTrainingLedger.prototype.payTraineeTraining = (
  currentUser,
  traineeId,
  trainingId,
  data,
  next
) => {
  console.log("currentUser: ", currentUser);
  async.waterfall(
    [
      callback => {
        if (!_.isEmpty(data.orNumber)) {
          dataDao.checkORNumber(data.orNumber, trainingId, (err, response) => {
            if (err) {
              return next({
                  msg: err.msg,
                  result: err,
                  success: false
                },
                null
              );
            }

            if (!_.isEmpty(response)) {
              return next(null, {
                msg: "OR Number already existed on the record. Please contact the Administrator to validate this issue!",
                result: null,
                success: false
              });
            } else {
              return callback();
            }
          });
        } else {
          return callback();
        }
      },
      callback => {
        dataDao.checkTraineeTrainingPaid(traineeId, trainingId, (err, response) => {
          if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }

          if (response) {
            return next(null, {
              msg: "Warning. No more payments accepted. Trainee already paid!",
              result: null,
              success: false
            });
          } else {
            return callback();
          }
        });
      },
      callback => {
        dataDao.payTraineeTraining(currentUser, traineeId, trainingId, data, (err, response) => {
          if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
              },
              null
            );
          }

          return callback(null, {
            msg: "Payment successfully saved!",
            result: response,
            success: true
          });
        });
      }
    ],
    next
  );
};

TraineeTrainingLedger.prototype.uploadTraineeTrainingPaymentProof = async (uuid, trainingId, data, next) => {
  const pathThumb = path.resolve("./public/uploads/payment/");
  const pathResizeThumb = path.resolve("./public/uploads/payment_resize/");

  if (!fs.existsSync(pathThumb)) {
    try {
      mkdirp.sync(pathThumb);
    } catch (error) {
      console.log("pathThumb err: ", error);
    }
  }

  if (!fs.existsSync(pathResizeThumb)) {
    try {
      mkdirp.sync(pathResizeThumb);
    } catch (error) {
      console.log("pathResizeThumb err: ", error);
    }
  }

  const fileName = uuid + "_" + trainingId + "_" + data.img_name;
  const fileKey = config.PCLOUD_APP_FOLDER + "/" + config.PCLOUD_APP_TRAINING_PAYMENT_FOLDER + "/" + fileName;
  const fileObj = {
    traineeTrainingId: data.traineeTrainingId,
    trainingId: trainingId,
    uuid: uuid,
    img_name: fileName,
    img_type: data.img_type,
    img_size: data.img_size
  };
  const savePath = pathResizeThumb + "/" + fileName;
  await sharp(data.img_path)
    .resize(400, 400, {
      kernel: sharp.kernel.lanczos2,
      fit: sharp.fit.contain,
      withoutEnlargement: true
    })
    .toFile(savePath);

  const fileData = await fs.readFileSync(savePath);
  const params = {
    Bucket: config.SPACES_BUCKET,
    Key: fileKey,
    Body: fileData,
    ACL: "public-read"
  };

  s3.upload(params, function(err, data) {
    if (err) {
      console.log("s3.putObject: ", err);
      return next(err);
    }

    console.log("Successfully uploaded data: ", data);
    fileObj.img_path = data.Location;
    fileObj.img_key = data.key;
    fileObj.folderid = null;
    fileObj.fileid = data.ETag;

    console.log("fileObj: ", fileObj);
    dataDao.uploadTraineeTrainingPaymentProof(uuid, trainingId, fileObj, (err, response) => {
      if (err) {
        return next({
          msg: err.msg,
          result: err,
          success: false
        }, null)
      }

      return next(null, {
        msg: 'Image successfully uploaded!',
        result: response,
        success: true
      });
    });
  });

};

TraineeTrainingLedger.prototype.deleteTraineeTrainingPaymentProof = (uuid, trainingId, imageId, next) => {
  dataDao.deleteTraineeTrainingPaymentProof(uuid, trainingId, imageId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: 'Image successfully delete!',
      result: response,
      success: true
    });
  });
};

TraineeTrainingLedger.prototype.getTraineeTrainingPayments = (traineeId, trainingId, next) => {
  dataDao.getTraineeTrainingPayments(traineeId, trainingId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: '',
      result: response,
      success: true
    });
  });
};

exports.TraineeTrainingLedger = TraineeTrainingLedger;