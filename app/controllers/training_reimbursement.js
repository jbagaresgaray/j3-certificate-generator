"use strict";

const dataDao = require("../daos/training_reimbursement");
const config = require("../../config/config");

const async = require("async");
const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");
const sharp = require("sharp");
const AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: config.SPACES_ACCESS_KEY_ID,
  secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint
});

function TrainingReimbursement() {
  this.dataDao = dataDao;
}

TrainingReimbursement.prototype.uploadTraineeTrainingReimbursementProof = async (transId, trainingId, data, next) => {
  const pathThumb = path.resolve("./public/uploads/reimbursement/");
  const pathResizeThumb = path.resolve("./public/uploads/reimbursement_resize/");

  if (!fs.existsSync(pathThumb)) {
    try {
      mkdirp.sync(pathThumb);
    } catch (error) {
      console.log("pathThumb err: ", error);
    }
  }

  if (!fs.existsSync(pathResizeThumb)) {
    try {
      mkdirp.sync(pathResizeThumb);
    } catch (error) {
      console.log("pathResizeThumb err: ", error);
    }
  }

  const fileName = transId + "_" + trainingId + "_" + data.img_name;
  const fileObj = {
    traineeId: data.traineeId,
    trainingId: trainingId,
    traineeTrainingId: data.traineeTrainingId,
    uuid: transId,
    img_name: fileName,
    img_type: data.img_type,
    img_size: data.img_size
  };
  const savePath = pathResizeThumb + "/" + fileName;
  const fileKey = config.PCLOUD_APP_FOLDER + "/" + config.PCLOUD_APP_TRAINING_REIMBURSEMENT_FOLDER + "/" + fileName;
  await sharp(data.img_path)
    .resize(400, 400, {
      kernel: sharp.kernel.lanczos2,
      fit: sharp.fit.contain,
      withoutEnlargement: true
    })
    .toFile(savePath);

  const fileData = await fs.readFileSync(savePath);
  const params = {
    Bucket: config.SPACES_BUCKET,
    Key: fileKey,
    Body: fileData,
    ACL: "public-read"
  };
  s3.upload(params, function(err, data) {
    if (err) {
      console.log("s3.putObject: ", err);
      return next(err);
    }

    console.log("Successfully uploaded data: ", data);
    fileObj.img_path = data.Location;
    fileObj.img_key = data.key;
    fileObj.folderid = null;
    fileObj.fileid = data.ETag;

    console.log("fileObj: ", fileObj);
    dataDao.uploadTraineeTrainingReimbursementProof(transId, trainingId, fileObj, (err, response) => {
      if (err) {
        return next({
          msg: err.msg,
          result: err,
          success: false
        }, null)
      }

      return next(null, {
        msg: 'Image successfully uploaded!',
        result: response,
        success: true
      });
    });
  });
};



TrainingReimbursement.prototype.insertReimbursement = (user, trainingId, data, next) => {
  dataDao.insertReimbursement(user, trainingId, data, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: 'Reimbursement successfully saved!',
      result: response,
      success: true
    });
  });
};

TrainingReimbursement.prototype.updateReimbursement = (transId, trainingId, data, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfReimbursementIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Reimbursement Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.updateReimbursement(transId, trainingId, data, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return next(null, {
          msg: 'Reimbursement successfully updated!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};

TrainingReimbursement.prototype.getTrainingReimbursementDetail = (transId, trainingId, next) => {
  dataDao.getTrainingReimbursementDetail(transId, trainingId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: '',
      result: response,
      success: true
    });
  });
};

TrainingReimbursement.prototype.deleteReimbursement = (transId, trainingId, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfReimbursementIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Reimbursement Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.deleteReimbursement(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return next(null, {
          msg: 'Reimbursement successfully deleted!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};


TrainingReimbursement.prototype.postTransactionReimbursement = (user, transId, trainingId, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfReimbursementIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Reimbursement Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.postTransactionReimbursement(user, transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return callback(null, {
          msg: 'Reimbursement successfully posted and locked!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};


TrainingReimbursement.prototype.getAllTrainingReimbursements = (params, trainingId, next) => {
  dataDao.getAllTrainingReimbursements(params, trainingId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: '',
      result: response,
      success: true
    });
  });
};

TrainingReimbursement.prototype.deleteTraineeTrainingReimbursementProof = (uuid, transId, imageId, next) => {
  dataDao.deleteTraineeTrainingReimbursementProof(uuid, transId, imageId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: 'Image successfully delete!',
      result: response,
      success: true
    });
  });
};


exports.TrainingReimbursement = TrainingReimbursement;