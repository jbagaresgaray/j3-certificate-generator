'use strict';

var dataDao = require('../daos/training_speakers');

var async = require('async');

function Speakers() {
    this.dataDao = dataDao;
}

Speakers.prototype.createTrainingSpeaker = (user, data, next) => {
    async.waterfall([
        (callback) => {
            dataDao.checkSpeakerName(data, (err, response) => {
                if (err) {
                    console.log("err: ", err);
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Speaker already existed',
                        result: null,
                        success: false
                    });
                }
                return callback();
            });
        },
        (callback) => {
            dataDao.createTrainingSpeaker(user, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully saved!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Speakers.prototype.updateTrainingSpeaker = (speakerId, data, next) => {
    async.waterfall([
        (callback) => {
            dataDao.checkSpeakerNameOnUpdate(speakerId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Speaker already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            dataDao.updateTrainingSpeaker(speakerId, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully updated!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Speakers.prototype.getSpeakerDetails = (speakerId, next) => {
    dataDao.getSpeakerDetails(speakerId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Speakers.prototype.deleteTrainingSpeaker = (speakerId, next) => {
    dataDao.deleteTrainingSpeaker(speakerId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: 'Record successfully deleted!',
            result: response,
            success: true
        });
    });
};

Speakers.prototype.getAllSpeakersByTraining = (params, next) => {
    dataDao.getAllSpeakersByTraining(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

exports.Speakers = Speakers;