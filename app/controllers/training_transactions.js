"use strict";

const dataDao = require("../daos/training_transactions");

const async = require("async");
const _ = require("lodash");

function TrainingTransactions() {
  this.dataDao = dataDao;
}


TrainingTransactions.prototype.insertIncomeExpense = (user, trainingId, data, next) => {
  dataDao.insertIncomeExpense(user, trainingId, data, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: 'Income/Expense successfully saved!',
      result: response,
      success: true
    });
  });
};

TrainingTransactions.prototype.updateIncomeExpense = (transId, trainingId, data, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfTransactionIncomeExpenseIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Income/Expense Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.updateIncomeExpense(transId, trainingId, data, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return next(null, {
          msg: 'Income/Expense successfully updated!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};

TrainingTransactions.prototype.getTrainingTransaction = (transId, trainingId, next) => {
  dataDao.getTrainingTransaction(transId, trainingId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: '',
      result: response,
      success: true
    });
  });
};

TrainingTransactions.prototype.deleteIncomeExpense = (transId, trainingId, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfTransactionIncomeExpenseIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Income/Expense Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.deleteIncomeExpense(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return next(null, {
          msg: 'Income/Expense successfully deleted!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};


TrainingTransactions.prototype.postTransactionIncomeExpense = (user, transId, trainingId, next) => {
  async.waterfall([
    callback => {
      dataDao.checkIfTransactionIncomeExpenseIsPosted(transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        if (response) {
          return next(null, {
            msg: 'Income/Expense Transaction already posted. Delete and updates are no longer allowed!',
            result: response,
            success: false
          });
        } else {
          return callback();
        }
      });
    },
    callback => {
      dataDao.postTransactionIncomeExpense(user, transId, trainingId, (err, response) => {
        if (err) {
          return next({
            msg: err.msg,
            result: err,
            success: false
          }, null)
        }

        return callback(null, {
          msg: 'Income/Expense successfully posted and locked!',
          result: response,
          success: true
        });
      });
    }
  ], next);
};


TrainingTransactions.prototype.getAllTrainingTransactions = (params, trainingId, next) => {
  dataDao.getAllTrainingTransactions(params, trainingId, (err, response) => {
    if (err) {
      return next({
        msg: err.msg,
        result: err,
        success: false
      }, null)
    }

    return next(null, {
      msg: '',
      result: response,
      success: true
    });
  });
};


exports.TrainingTransactions = TrainingTransactions;