'use strict';

const dataDao = require('../daos/trainings');
const func = require("../utils/functions");
const config = require("../../config/config");


const async = require('async');
const _ = require('lodash');
const ejs = require('ejs');
const fs = require("fs");
const path = require("path");
const pdf = require('html-pdf');
const mkdirp = require("mkdirp");
const moment = require('moment');


function Trainings() {
    this.dataDao = dataDao;
}

Trainings.prototype.createTrainings = (user, data, next) => {
    dataDao.createTrainings(user, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: 'Record successfully saved!',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.updateTrainings = (trainingId, data, next) => {
    dataDao.updateTrainings(trainingId, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: 'Record successfully updated!',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.updateTrainingStatus = (trainingId, data, next) => {
    dataDao.updateTrainingStatus(trainingId, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        return next(null, {
            msg: 'Record successfully updated!',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.getTrainingDetails = (trainingId, next) => {
    dataDao.getTrainingDetails(trainingId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.deleteTraining = (trainingId, next) => {
    async.waterfall([
        (callback) => {
            dataDao.getTrainingDetails(trainingId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                }

                console.log("getTrainingDetails response: ", response);

                if (!_.isEmpty(response)) {
                    if (response.status == func.TRAINING_STATUS.OPEN) {
                        if (response.total_trainees > 0) {
                            return next(null, {
                                msg: 'Unable to delete! Training has registered trainees.',
                                result: null,
                                success: false
                            });
                        } else if (response.total_paid_trainess > 0) {
                            return next(null, {
                                msg: 'Unable to delete! Training has registered and paid trainees.',
                                result: null,
                                success: false
                            });
                        } else {
                            return callback();
                        }
                    } else if (response.status == func.TRAINING_STATUS.CLOSED) {
                        return next(null, {
                            msg: 'Unable to delete! Training already close for registration.',
                            result: null,
                            success: false
                        });
                    } else if (response.status == func.TRAINING_STATUS.DONE) {
                        return next(null, {
                            msg: 'Unable to delete! Training already done and has trainee records.',
                            result: null,
                            success: false
                        });
                    }
                } else {
                    return next(null, {
                        msg: 'Unable to delete! No training record found.',
                        result: null,
                        success: false
                    });
                }
            });
        },
        (callback) => {
            console.log("deleteTraining");
            dataDao.deleteTraining(trainingId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return callback(null, {
                    msg: 'Record successfully deleted!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Trainings.prototype.getAllTrainings = (params, next) => {
    dataDao.getAllTrainings(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.getTrainingTrainees = (trainingId, next) => {
    dataDao.getTrainingTrainees(trainingId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.createTrainingSpeakers = (trainingId, data, next) => {
    dataDao.createTrainingSpeakers(trainingId, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: 'Speaker successfully added!',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.deleteTrainingSpeakers = (trainingId, speakerId, next) => {
    dataDao.deleteTrainingSpeakers(trainingId, speakerId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        return next(null, {
            msg: 'Speaker successfully deleted on the training!',
            result: response,
            success: true
        });
    });
};

Trainings.prototype.getTrainingTraineesCertificates = (trainingId, params, next) => {
    dataDao.getTrainingTraineesCertificates(trainingId, params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }


        const filepath = path.resolve("./public/reports/training/");
        const filename = trainingId + ".pdf";
        const _filenamePath = path.join(filepath, filename)
        if (!fs.existsSync(filepath)) {
            try {
                mkdirp.sync(filepath);
                console.log("filepath directory created");
            } catch (error) {
                console.log("filepath err: ", error);
            }
        }
        const ejstemplate = path.join(__dirname, '../', 'views/certificates.ejs');

        _.each(response, certificate => {
            let training_speaker = "";
            let courseCode = "";

            if (!_.isEmpty(certificate.training.training_speakers)) {
                certificate.training_speaker = certificate.training.training_speakers[0];
                if (certificate.training_speaker) {
                    training_speaker = certificate.training_speaker
                        .trainingSpeaker;
                }
            }

            if (!_.isEmpty(certificate.training.course)) {
                certificate.course = certificate.training.course;
                if (certificate.course) {
                    courseCode = certificate.course.courseCode;
                }
            }

            certificate.qr_value =
                certificate.course.courseName +
                " (" +
                certificate.course.courseSlug +
                ") " +
                "\nCertificate Holder: " +
                _.capitalize(certificate.firstName) +
                " " +
                certificate.middleName.charAt(0) +
                ". " +
                _.capitalize(certificate.lastName) +
                "\nCertificate No: " +
                courseCode +
                "-" +
                certificate.traineeNum +
                "\nConsultant: " +
                _.upperCase(training_speaker) +
                "\nIssued No. 0 " +
                "\nDate Issued: " +
                moment(certificate.training.trainingSchedTo).format("MM/DD/YYYY") +
                "\nwww.j3solutions.com.ph";
        });

       /* const ejsdata = {
            trainingsArr: response,
            moment: moment
};    const pdfoptions = {
            format: 'Letter',
            base: config.api_host_url + ":" + config.port,
            orientation: "portrait",
            border: "0in",
            zoomFactor: "0"
        };

        ejs.renderFile(ejstemplate, ejsdata, (err, html) => {
            if (err) {
                console.log(err); // Handle error
            }
            // pdf.create(html, pdfoptions).toFile(_filenamePath, function(err, res) {
            //     console.log("pdf.create err: ", err);
            //     console.log("filename: ", res.filename);
            // });
        });*/

        return next(null, {
            msg: '',
            result: response,
            // reportsHtml: html,
            success: true
        });
    });
};

exports.Trainings = Trainings;