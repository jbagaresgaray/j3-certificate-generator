'use strict';

const usersDao = require('../daos/users');

const async = require('async');
const _ = require("lodash");
const path = require("path");
const moment = require("moment");
const func = require("../utils/functions");
const config = require("../../config/config");

function Users() {
    this.usersDao = usersDao;
}

Users.prototype.inviteUser = (data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUserEmail(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Email Address already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            usersDao.inviteUser(data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                const template = path.join(__dirname, '../', 'views/emails/invitation.ejs');
                const ejstemp = {
                    firstname: data.firstname,
                    lastname: data.lastname,
                    email: data.email,
                    phone: data.phone,
                    verificationCode: response.verificationCode,
                    uuid: response.uuid,
                    joinUrl: config.app_host_url + "/verify?action=invite&verificationCode=" + response.verificationCode + "&uuid=" + response.uuid,
                    workspaceURL: config.app_host_url
                };
                console.log("ejstemp: ", ejstemp);
                func.sendMailTemplate(template, ejstemp, 'train@j3solutions.com.ph', data.email, 'J3 Trainers and Consultants, Inc. has invited you to join TMS App');

                return next(null, {
                    msg: 'Record successfully saved!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.createUserProfile = (userId, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUserEmailUpdate(userId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Email Address already existed',
                        result: null,
                        success: false
                    });
                }
                callback();
            });
        },
        (callback) => {
            usersDao.createUserProfile(userId, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully saved!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.createUserAccount = (code, uuid, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserStatus(code, uuid, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                if (response) {
                    if (response.isVerify == 1 && !_.isEmpty(response.dateVerify)) {
                        return next(null, {
                            msg: 'User Account already verified!',
                            result: response,
                            success: true
                        });
                    } else {
                        return callback(null, response);
                    }
                } else {
                    return next(null, {
                        msg: 'No User account found!',
                        result: response,
                        success: false
                    });
                }
            });
        },
        (user, callback) => {
            usersDao.checkUserUsername(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Username already existed',
                        result: null,
                        success: false
                    });
                }
                return callback(null, user);
            });
        },
        (user, callback) => {
            usersDao.createUserAccount(user.uuid, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully saved!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.getAllUsers = (params, next) => {
    usersDao.getAllUsers(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.checkUserStatus = (code, uuid, next) => {
    usersDao.checkUserStatus(code, uuid, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }

        if (response) {
            if (response.isVerify == 1) {
                return next(null, {
                    msg: 'User Account already verified!',
                    result: response,
                    success: true
                });
            } else {
                return next(null, {
                    msg: 'User Account not yet verified!',
                    result: response,
                    success: false
                });
            }
        } else {
            return next(null, {
                msg: 'No User account found!',
                result: response,
                success: false
            });
        }
    });
};

Users.prototype.getUser = (user_id, next) => {
    usersDao.getUser(user_id, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        if (response) {
            delete response.password;
        }
        return next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.deleteUser = (user_id, next) => {
    usersDao.deleteUser(user_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        next(null, {
            msg: 'Record successfully deleted!',
            result: response,
            success: true
        });
    });
};

Users.prototype.updateUser = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUserEmailUpdate(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Email Address already existed',
                        result: null,
                        success: false
                    });
                }
                return callback();
            });
        },
        (callback) => {
            usersDao.updateUserProfile(user_id, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'Record successfully updated!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.updateUserAccount = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUsernameUpdate(userId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Username already existed',
                        result: null,
                        success: false
                    });
                }
                return callback();
            });
        },
        (callback) => {
            usersDao.updateUserAccount(user_id, data, (err, response) => {
                if (err) {
                    return callback({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                return next(null, {
                    msg: 'User Account successfully updated!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};


Users.prototype.updateUserName = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUsernameUpdate(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }

                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Username already existed',
                        result: null,
                        success: false
                    });
                }
                return callback();
            });
        },
        (callback) => {
            usersDao.updateUserName(user_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                }
                next(null, {
                    msg: 'Username successfully updated!',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.updateUserPassword = (user_id, data, next) => {
    usersDao.updateUserPassword(user_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        }
        next(null, {
            msg: 'Password successfully updated!',
            result: response,
            success: true
        });
    });
};

Users.prototype.forgotPassword = (data, next) => {
    usersDao.getUserByUsernameAndEmail(data.email, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        if (response) {
            console.log("response: ", response);
            const verificationCode = func.generateString(42);
            usersDao.resetPasswordRequest(response.uuid, verificationCode, (err1, result) => {
                if (err1) {
                    next({
                        result: err1,
                        msg: err1.message,
                        success: true
                    }, null);
                }

                const template = path.join(__dirname, '../', 'views/emails/forgot.ejs');
                const ejstemp = {
                    firstname: response.firstname,
                    lastname: response.lastname,
                    username: response.username,
                    email: response.email,
                    phone: response.phone,
                    verificationCode: verificationCode,
                    uuid: response.uuid,
                    resetLink: config.app_host_url + '/reset?action=reset&code=' + verificationCode + '&uuid=' + response.uuid,
                    workspaceURL: config.app_host_url
                };
                func.sendMailTemplate(template, ejstemp, 'train@j3solutions.com.ph', response.email, 'Reset your password');
                return next(null, {
                    msg: 'We have e-mailed your reset password link! Please check your email for the next step.',
                    result: {
                        email: response.email,
                        firstname: response.firstname,
                        lastname: response.lastname
                    },
                    success: true
                });
            });
        } else {
            next(null, {
                result: null,
                msg: 'Email address not exist with the given info.',
                success: false
            });
        }
    });
};

Users.prototype.checkResetPassword = (uuid, code, next) => {
    usersDao.getUserByUUIDChangeCode(uuid, code, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        if (response) {
            return next(null, {
                result: null,
                msg: '',
                success: true
            });
        } else {
            return next(null, {
                result: null,
                msg: 'That link isn’t working. Password reset links expires once validated or used!',
                success: false
            });
        }
    });
};

Users.prototype.resetPassword = (data, next) => {
    usersDao.getUserByUUIDChangeCode(data.uuid, data.code, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        if (response) {
            usersDao.updateUserPassword(response.userId, data, (err1, result) => {
                if (err1) {
                    next({
                        result: err1,
                        msg: err1.message,
                        success: true
                    }, null);
                }

                const template = path.join(__dirname, '../', 'views/emails/reset.ejs');
                const ejstemp = {
                    firstname: response.firstname,
                    lastname: response.lastname,
                    username: response.username,
                    email: response.email,
                    phone: response.phone,
                    Date: moment().format('MM-DD-YYYY hh:mm:ss A Z'),
                    workspaceURL: config.app_host_url
                };
                func.sendMailTemplate(template, ejstemp, 'train@j3solutions.com.ph', response.email, 'Your password has been changed');
                return next(null, {
                    msg: 'Password successfully changed.',
                    result: {
                        email: response.email,
                        firstname: response.firstname,
                        lastname: response.lastname
                    },
                    success: true
                });
            });
        } else {
            next(null, {
                result: null,
                msg: 'User does not exist with the given info.',
                success: false
            });
        }
    });
};

Users.prototype.resendEmailVerification = (user_id, next) => {
    usersDao.getUserByUUID(user_id, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        if (response) {
            if (response.isVerify == 1 && !_.isEmpty(response.dateVerify)) {
                return next(null, {
                    msg: 'User Account already verified!',
                    result: null,
                    success: false
                });
            }

            const template = path.join(__dirname, '../', 'views/emails/invitation.ejs');
            const ejstemp = {
                firstname: response.firstname,
                lastname: response.lastname,
                email: response.email,
                phone: response.phone,
                verificationCode: response.verificationCode,
                uuid: response.uuid,
                joinUrl: config.app_host_url + "/verify?action=invite&verificationCode=" + response.verificationCode + "&uuid=" + response.uuid,
                workspaceURL: config.app_host_url
            };
            func.sendMailTemplate(template, ejstemp, 'train@j3solutions.com.ph', response.email, 'J3 Trainers and Consultants, Inc. has invited you to join TMS App');

            return next(null, {
                msg: 'Email successfully sent!',
                result: null,
                success: true
            });
        } else {
            next(null, {
                result: null,
                msg: 'User does not exist with the given info.',
                success: false
            });
        }
    });
};


exports.Users = Users;