"use strict";

const Database = require("../../app/utils/database").Database;
const db = new Database();
const func = require("../utils/functions");
const _ = require("lodash");
const async = require("async");

const knex = require("knex")({
  client: "mysql",
  connection: db.configuration
});

exports.getAllAccountClassification = (params, next) => {
  if (!_.isEmpty(params)) {
    if (params.limit <= func.PAGINATE_MAX_COUNT) {
      params.limit = func.PAGINATE_MAX_COUNT;
    }

    if (params.startWith == 1) {
      params.startWith = 0;
    }
  } else {
    params.limit = func.PAGINATE_MAX_COUNT;
    params.startWith = 0;
  }

  let numRows;
  const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
  let page = parseInt(params.startWith) || 1;
  page = page - 1;
  let numPages;
  const skip = page * numPerPage;
  const limit = skip + "," + numPerPage;

  async.waterfall(
    [
      callback => {
        let strSQL = knex("account_classification").count("* as numRows");
        if (params.search) {
          strSQL.where(sql => {
            sql
              .where("class_code", "LIKE", "%" + params.search + "%")
              .orWhere("class_name", "LIKE", "%" + params.search + "%")
              .orWhere("accountGroupID", params.search);
          });
        }
        strSQL.asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }
          numRows =
            results && results[0] && results[0].numRows ?
            results[0].numRows :
            0;
          numPages = Math.ceil(numRows / numPerPage);
          return callback(null, numRows, numPages);
        });
      },
      (numRows, numPages, callback) => {
        let strSQL = knex.select("*").from("account_classification");
        if (params.search) {
          strSQL.where(sql => {
            sql
              .where("class_code", "LIKE", "%" + params.search + "%")
              .orWhere("class_name", "LIKE", "%" + params.search + "%")
              .orWhere("accountGroupID", params.search);
          });
        }
        if (params.limit && params.startWith) {
          strSQL = strSQL.limit(numPerPage).offset(skip);
        } else if (params.limit) {
          strSQL = strSQL.limit(numPerPage);
        }

        strSQL.asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }
          return callback(null, numRows, numPages, results);
        });
      },
      (numRows, numPages, dataArr, callback) => {
        knex.select("*").from("account_groups")
          .asCallback((err, accountGroups) => {
            if (err) {
              return next(err, null);
            }

            _.each(dataArr, (row) => {
              row["accountGroup"] = _.find(accountGroups, {
                "account_groupId": row.accountGroupID
              }) || {};
            });

            return callback(null, numRows, numPages, dataArr);
          });
      },
      (numRows, numPages, dataArr, callback) => {
        if (params.limit && params.startWith) {
          if (page < numPages) {
            return callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page + 1,
                page_size: numPerPage,
                maxResult: numPages
              }
            });
          } else {
            return callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages || 0
              }
            });
          }
        } else {
          return callback(null, {
            data: dataArr,
            pagination: {
              total: numRows,
              startAt: 0,
              page_size: 0,
              maxResult: 0
            }
          });
        }
      }
    ],
    next
  );
};