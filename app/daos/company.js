'use strict';

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const _ = require("lodash");
const async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkCompanyName = (data, next) => {
    knex('companies').where("companyName", data.companyName).asCallback(next);
};

exports.isCompanyHasTrainee = (companyId, next) => {
    knex('trainees').where("companyId", companyId).asCallback(next);
};

exports.checkCompanyNameOnUpdate = (companyId, data, next) => {
    knex('companies').where("companyName", data.companyName).andWhere("companyId", "<>", companyId).asCallback(next);
};

exports.createCompany = (user, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId').from('users').where("uuid", user.uuid).first().asCallback((err, result) => {
                if (err) {
                    return next(err);
                }

                console.log("result: ", result);
                return callback(null, result.userId);
            });
        },
        (userId, callback) => {
            knex.transaction(sql => {
                return sql.insert({
                        companyName: data.companyName,
                        contactPerson: data.contactPerson,
                        contactNum: data.contactNum,
                        contactEmail: data.contactEmail,
                        companyTIN: data.companyTIN,
                        uuid: knex.raw('UUID()'),
                        date_created: knex.raw('NOW()'),
                        createdBy: userId
                    })
                    .into('companies')
                    .then((resp) => {
                        return resp[0];
                    })
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.updateCompany = (companyId, data, next) => {
    knex.transaction(trx => {
        return trx.update({
                companyName: data.companyName,
                contactPerson: data.contactPerson,
                contactNum: data.contactNum,
                contactEmail: data.contactEmail,
                companyTIN: data.companyTIN
            })
            .from('companies')
            .where(sql => {
                sql.where("companyId", companyId).orWhere("uuid", companyId)
            })
            .asCallback(next);
    });
};

exports.getCompanyDetails = (companyId, next) => {
    knex('companies').where("companyId", companyId).first().asCallback(next);
};

exports.deleteCompany = (companyId, next) => {
    knex.transaction(trx => {
        return trx.from('companies').where("companyId", companyId).delete().asCallback(next);
    });
};

exports.getAllCompanies = (params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= func.PAGINATE_MAX_COUNT) {
            params.limit = func.PAGINATE_MAX_COUNT;
        }
    } else {
        params.limit = func.PAGINATE_MAX_COUNT;
        params.startWith = 0;
    }

    let numRows;
    const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
    let page = parseInt(params.startWith) || 1;
    page = page - 1;
    let numPages;
    const skip = page * numPerPage;
    const limit = skip + "," + numPerPage;

    if (page == 0) {
        page = 1;
    } else {
        page += 1;
    }

    async.waterfall(
        [
            callback => {
                let strSQL = knex.select(knex.raw('COUNT(*) AS numRows')).from("companies");
                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("companyName", "LIKE", "%" + params.search + "%")
                            .orWhere("contactPerson", "LIKE", "%" + params.search + "%")
                            .orWhere("contactNum", "LIKE", "%" + params.search + "%")
                            .orWhere("contactEmail", "LIKE", "%" + params.search + "%")
                            .orWhere("companyTIN", "LIKE", "%" + params.search + "%")
                    });
                }
                strSQL.asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }
                    numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("companies");

                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("companyName", "LIKE", "%" + params.search + "%")
                            .orWhere("contactPerson", "LIKE", "%" + params.search + "%")
                            .orWhere("contactNum", "LIKE", "%" + params.search + "%")
                            .orWhere("contactEmail", "LIKE", "%" + params.search + "%")
                            .orWhere("companyTIN", "LIKE", "%" + params.search + "%")
                    });
                }

                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }
                strSQL.orderBy("date_created", "desc");

                console.log("strSQL: ", strSQL.toString());
                strSQL.asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }
                    return callback(null, numRows, numPages, results);
                });
            },
            (numRows, numPages, dataArr, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        data: dataArr,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                }
            }
        ],
        next
    );
};