"use strict";

const Database = require("../../app/utils/database").Database;
const db = new Database();
const func = require("../utils/functions");
const _ = require("lodash");
const async = require("async");

const knex = require("knex")({
  client: "mysql",
  connection: db.configuration
});

exports.checkCourseCode = (data, next) => {
  knex("course")
    .where("courseCode", data.courseCode)
    .asCallback(next);
};

exports.checkCourseCodeUpdate = (courseId, data, next) => {
  knex("course")
    .where("courseCode", data.courseCode)
    .andWhere("courseId", "<>", courseId)
    .asCallback(next);
};

exports.createCourse = (user, data, next) => {
  async.waterfall(
    [
      callback => {
        knex
          .select("userId")
          .from("users")
          .where("uuid", user.uuid)
          .first()
          .asCallback((err, result) => {
            if (err) {
              return next(err);
            }
            return callback(null, result.userId);
          });
      },
      (userId, callback) => {
        knex.transaction(sql => {
          return sql
            .insert({
              courseName: data.courseName,
              courseCode: data.courseCode,
              courseSlug: data.courseSlug,
              courseCitation: data.courseCitation,
              courseHours: data.courseHours,
              date_created: knex.raw("NOW()"),
              createdBy: userId,
              uuid: knex.raw("UUID()")
            })
            .into("course")
            .then(resp => {
              return resp[0];
            })
            .asCallback(callback);
        });
      }
    ],
    next
  );
};

exports.updateCourse = (courseId, data, next) => {
  knex.transaction(trx => {
    return trx
      .update({
        courseName: data.courseName,
        courseCode: data.courseCode,
        courseSlug: data.courseSlug,
        courseCitation: data.courseCitation,
        courseHours: data.courseHours
      })
      .from("course")
      .where(sql => {
        sql.where("courseId", courseId).orWhere("uuid", courseId);
      })
      .asCallback(next);
  });
};

exports.getCourseDetails = (courseId, next) => {
  knex("course")
    .where("courseId", courseId)
    .first()
    .asCallback(next);
};

exports.deleteCourse = (courseId, next) => {
  knex("course")
    .where("courseId", courseId)
    .delete()
    .asCallback(next);
};

exports.getAllCourses = (params, next) => {
  if (!_.isEmpty(params)) {
    if (params.limit <= func.PAGINATE_MAX_COUNT) {
      params.limit = func.PAGINATE_MAX_COUNT;
    }

    if (params.startWith == 1) {
      params.startWith = 0;
    }
  } else {
    params.limit = func.PAGINATE_MAX_COUNT;
    params.startWith = 0;
  }

  let numRows;
  const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
  let page = parseInt(params.startWith) || 1;
  page = page - 1;
  let numPages;
  const skip = page * numPerPage;
  const limit = skip + "," + numPerPage;

  if (page == 0) {
    page = 1;
  } else {
    page += 1;
  }

  async.waterfall(
    [
      callback => {
        let strSQL = knex("course").count("* as numRows");
        if (params.search) {
          strSQL.where(sql => {
            sql
              .where("courseName", "LIKE", "%" + params.search + "%")
              .orWhere("courseCode", "LIKE", "%" + params.search + "%");
          });
        }
        strSQL.asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }
          numRows =
            results && results[0] && results[0].numRows
              ? results[0].numRows
              : 0;
          numPages = Math.ceil(numRows / numPerPage);
          return callback(null, numRows, numPages);
        });
      },
      (numRows, numPages, callback) => {
        let strSQL = knex.select("*").from("course");
        if (params.search) {
          strSQL.where(sql => {
            sql
              .where("courseName", "LIKE", "%" + params.search + "%")
              .orWhere("courseCode", "LIKE", "%" + params.search + "%");
          });
        }
        if (params.limit && params.startWith) {
          strSQL = strSQL.limit(numPerPage).offset(skip);
        } else if (params.limit) {
          strSQL = strSQL.limit(numPerPage);
        }

        strSQL.orderBy("date_created", "desc").asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }
          return callback(null, numRows, numPages, results);
        });
      },
      (numRows, numPages, dataArr, callback) => {
        if (params.limit && params.startWith) {
          if (page < numPages) {
            callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages
              }
            });
          } else {
            callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages || 0
              }
            });
          }
        } else {
          callback(null, {
            data: dataArr,
            pagination: {
              total: numRows,
              startAt: page,
              page_size: numPerPage,
              maxResult: numPages
            }
          });
        }
      }
    ],
    next
  );
};
