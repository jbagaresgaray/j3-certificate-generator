'use strict';

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const _ = require("lodash");
const async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.getDashboardReport = (next) => {
    async.waterfall([
        (callback) => {
            knex.count("* AS courseCount").from("course").first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                }
                console.log("course count: ", response.courseCount);
                callback(null, response.courseCount);
            });
        },
        (courseCount, callback) => {
            knex.count("* AS trainingCount").from("training").first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                }
                console.log("training count: ", response.trainingCount);
                callback(null, courseCount, response.trainingCount);
            });
        },
        (courseCount, trainingCount, callback) => {
            knex.count("* AS traineeCount").from("trainees").first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                }
                console.log("trainee count: ", response.traineeCount);
                callback(null, {
                    courses: courseCount,
                    training: trainingCount,
                    trainee: response.traineeCount
                });
            });
        }
    ], next);
};