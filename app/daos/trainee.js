"use strict";

const Database = require("../../app/utils/database").Database;
const db = new Database();
const func = require("../utils/functions");
const config = require("../../config/config");

const _ = require("lodash");
const async = require("async");
const fs = require("fs");
const path = require("path");
const fetch = require("isomorphic-fetch");

const AWS = require("aws-sdk");
AWS.config.update({
  accessKeyId: config.SPACES_ACCESS_KEY_ID,
  secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint
});

const knex = require("knex")({
  client: "mysql",
  connection: db.configuration
});

exports.checkTraineeName = (data, next) => {
  knex("trainees")
    .where(sql => {
      sql.where("lastName", data.lastName);
      sql.andWhere("firstName", data.firstName);
      sql.andWhere("middleName", data.middleName);
      sql.andWhere("suffix", data.suffix);
    })
    .asCallback(next);
};

exports.checkTraineeNameOnUpdate = (traineesId, data, next) => {
  knex("trainees")
    .where(sql => {
      sql.where("lastName", data.lastName);
      sql.andWhere("firstName", data.firstName);
      sql.andWhere("middleName", data.middleName);
      sql.andWhere("suffix", data.suffix);
      sql.andWhere("traineesId", "<>", traineesId);
    })
    .asCallback(next);
};

exports.checkORNumber = (orNumber, next) => {
  knex("trainee_trainings")
    .where("orNumber", orNumber)
    .asCallback(next);
};

exports.checkTransactionNumber = (orNumber, next) => {
  knex("trainee_trainings")
    .where("paymentTransInfo", orNumber)
    .asCallback(next);
};

exports.checkTraineePaid = (traineesId, trainingId, next) => {
  knex("trainee_trainings")
    .where("traineeId", traineesId)
    .andWhere("isPaid", 1)
    .andWhere("trainingId", trainingId)
    .asCallback(next);
};

exports.createTrainee = (user, data, next) => {
  async.waterfall(
    [
      callback => {
        knex
          .select("userId")
          .from("users")
          .where("uuid", user.uuid)
          .first()
          .asCallback((err, result) => {
            if (err) {
              return next(err);
            }

            console.log("result: ", result);
            return callback(null, result.userId);
          });
      },
      (userId, callback) => {
        const traineeObj = {
          lastName: data.lastName,
          firstName: data.firstName,
          middleName: data.middleName,
          companyId: data.companyId,
          detailAddress: data.detailAddress,
          state: data.state,
          city: data.city,
          zipcode: data.zipcode,
          country: data.country,
          traineePosition: data.traineePosition,
          traineeNickname: data.traineeNickname,
          traineesIndustry: data.traineesIndustry,
          traineeSex: data.traineeSex,
          traineeContactNo: data.traineeContactNo,
          traineeTIN: data.traineeTIN,
          traineeEmail: data.traineeEmail,
          shirtSize: data.shirtSize,
          suffix: data.suffix,
          date_created: knex.raw("NOW()"),
          createdBy: userId,
          uuid: knex.raw("UUID()")
        };

        if (!_.isEmpty(data.traineeBirtdate)) {
          traineeObj.traineeBirtdate = data.traineeBirtdate;
        }else{
          traineeObj.traineeBirtdate = null;
        }

        knex.transaction(sql => {
          return sql
            .insert(traineeObj)
            .into("trainees")
            .then(resp => {
              return resp[0];
            })
            .asCallback(callback);
        });
      }
    ],
    next
  );
};

exports.updateTrainee = (traineesId, data, next) => {
  const traineeObj = {
    lastName: data.lastName,
    firstName: data.firstName,
    middleName: data.middleName,
    companyId: data.companyId,
    detailAddress: data.detailAddress,
    state: data.state,
    city: data.city,
    zipcode: data.zipcode,
    country: data.country,
    traineePosition: data.traineePosition,
    traineeNickname: data.traineeNickname,
    traineesIndustry: data.traineesIndustry,
    traineeSex: data.traineeSex,
    traineeContactNo: data.traineeContactNo,
    traineeTIN: data.traineeTIN,
    traineeEmail: data.traineeEmail,
    shirtSize: data.shirtSize,
    suffix: data.suffix
  };

  if (!_.isEmpty(data.traineeBirtdate)) {
    traineeObj.traineeBirtdate = data.traineeBirtdate;
  }else{
    traineeObj.traineeBirtdate = null;
  }

  knex.transaction(trx => {
    return trx
      .update(traineeObj)
      .from("trainees")
      .where(sql => {
        sql.where("traineesId", traineesId).orWhere("uuid", traineesId);
      })
      .asCallback(next);
  });
};

exports.deleteTrainee = (traineesId, next) => {
  async.waterfall(
    [
      callback => {
        knex("trainees_images")
          .where("traineesId", traineesId)
          .first()
          .asCallback((err, result) => {
            if (err) {
              return next(err, null);
            }

            if (!_.isEmpty(result)) {
              return callback(null, result);
            } else {
              return callback(null, {});
            }
          });
      },
      (trainee, callback) => {
        if (!_.isEmpty(trainee)) {
          const params = {
            Bucket: config.SPACES_BUCKET,
            Key: trainee.img_key
          };
          s3.deleteObject(params, (err, data) => {
            if (err) {
              console.log(err, err.stack);
              return callback(null, trainee);
            } // an error occurred
            console.log("deleteObject: ", data);
            return callback(null, trainee);
          });
        } else {
          return callback(null, trainee);
        }
      },
      (trainee, callback) => {
        if (!_.isEmpty(trainee)) {
          async.parallel(
            [
              callback2 => {
                knex("trainees_images")
                  .where(sql => {
                    sql.where("traineesId", traineesId);
                    sql.andWhere("fileid", trainee.imagesId);
                  })
                  .delete()
                  .asCallback(callback2);
              },
              callback2 => {
                knex.transaction(trx => {
                  return trx
                    .from("trainees")
                    .where("traineesId", traineesId)
                    .delete()
                    .asCallback(callback2);
                });
              }
            ],
            callback
          );
        } else {
          knex.transaction(trx => {
            return trx
              .from("trainees")
              .where("traineesId", traineesId)
              .delete()
              .asCallback(callback);
          });
        }
      }
    ],
    next
  );
};

exports.getTraineeDetails = (traineesId, next) => {
  knex("trainees")
    .where("traineesId", traineesId)
    .first()
    .asCallback((err, result) => {
      if (err) {
        return next(err);
      }

      if (result) {
        async.parallel(
          [
            callback => {
              knex("companies")
                .where("companyId", result.companyId)
                .first()
                .asCallback((err, company) => {
                  if (err) {
                    return next(err);
                  }

                  if (company) {
                    result.company = company;
                  } else {
                    result.company = {};
                  }
                  return callback();
                });
            },
            callback => {
              knex("trainees_images")
                .where("traineesId", result.traineesId)
                .first()
                .asCallback((err, trainees_image) => {
                  if (err) {
                    return next(err);
                  }

                  if (trainees_image) {
                    trainees_image.public_img_path = trainees_image.img_path;
                    result.image = trainees_image;
                  } else {
                    result.image = {};
                  }
                  return callback();
                });
            }
          ],
          () => {
            return next(null, result);
          }
        );
      } else {
        return next(null, {});
      }
    });
};

exports.getAlltrainees = (params, next) => {
  if (!_.isEmpty(params)) {
    if (params.limit <= func.PAGINATE_MAX_COUNT) {
      params.limit = func.PAGINATE_MAX_COUNT;
    }
  } else {
    params.limit = func.PAGINATE_MAX_COUNT;
    params.startWith = 0;
  }

  let numRows;
  const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
  let page = parseInt(params.startWith) || 1;
  page = page - 1;
  let numPages;
  const skip = page * numPerPage;
  const limit = skip + "," + numPerPage;

  if (page == 0) {
    page = 1;
  } else {
    page += 1;
  }

  async.waterfall(
    [
      callback => {
        let strSQL = knex
          .select(knex.raw("COUNT(*) as numRows"))
          .from("trainees AS t")
          .leftJoin("companies AS c", "c.companyId", "t.companyId");

        if (params.courseId) {
          strSQL.leftJoin(
            "trainee_trainings AS tt",
            "tt.traineeId",
            "t.traineesId"
          );
          strSQL.where(
            knex.raw(
              "(select courseId FROM training WHERE trainingId= tt.trainingId limit 1)"
            ),
            params.courseId
          );
        }

        if (params.dateFrom && params.dateTo) {
          if (params.courseId) {
            strSQL.where(sql => {
              sql.whereBetween(
                knex.raw(
                  "(select trainingSchedFrom FROM training WHERE trainingId= `tt`.`trainingId` limit 1)"
                ),
                [params.dateFrom, params.dateTo]
              );
              sql.orWhere(sql1 => {
                sql1.whereBetween(
                  knex.raw(
                    "(select trainingSchedTo FROM training WHERE trainingId= `tt`.`trainingId` limit 1)"
                  ),
                  [params.dateFrom, params.dateTo]
                );
              });
            });
          } else {
            strSQL.whereBetween("t.date_created", [
              params.dateFrom,
              params.dateTo
            ]);
          }
        }

        if (params.search) {
          strSQL.where(sql => {
            sql
              .where(knex.raw("t.lastName LIKE ?", ["%" + params.search + "%"]))
              .orWhere(
                knex.raw("t.firstName LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.middleName LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.traineeEmail LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.traineeNickname LIKE ?", [
                  "%" + params.search + "%"
                ])
              );
          });
        }

        strSQL.asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }
          numRows =
            results && results[0] && results[0].numRows
              ? results[0].numRows
              : 0;
          numPages = Math.ceil(numRows / numPerPage);
          return callback(null, numRows, numPages);
        });
      },
      (numRows, numPages, callback) => {
        let strSQL = knex
          .select("t.*")
          .from("trainees AS t")
          .leftJoin("companies AS c", "c.companyId", "t.companyId");

        if (params.courseId) {
          strSQL.leftJoin(
            "trainee_trainings AS tt",
            "tt.traineeId",
            "t.traineesId"
          );
          strSQL.where(
            knex.raw(
              "(select courseId FROM training WHERE trainingId= tt.trainingId limit 1)"
            ),
            params.courseId
          );
        }

        if (params.dateFrom && params.dateTo) {
          if (params.courseId) {
            strSQL.where(sql => {
              sql.whereBetween(
                knex.raw(
                  "(select trainingSchedFrom FROM training WHERE trainingId= `tt`.`trainingId` limit 1)"
                ),
                [params.dateFrom, params.dateTo]
              );
              sql.orWhere(sql1 => {
                sql1.whereBetween(
                  knex.raw(
                    "(select trainingSchedTo FROM training WHERE trainingId= `tt`.`trainingId` limit 1)"
                  ),
                  [params.dateFrom, params.dateTo]
                );
              });
            });
          } else {
            strSQL.whereBetween("t.date_created", [
              params.dateFrom,
              params.dateTo
            ]);
          }
        }

        if (params.search) {
          strSQL.where(sql => {
            sql
              .where(knex.raw("t.lastName LIKE ?", ["%" + params.search + "%"]))
              .orWhere(
                knex.raw("t.firstName LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.middleName LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.traineeEmail LIKE ?", ["%" + params.search + "%"])
              )
              .orWhere(
                knex.raw("t.traineeNickname LIKE ?", [
                  "%" + params.search + "%"
                ])
              );
          });
        }

        if (params.limit && params.startWith) {
          strSQL = strSQL.limit(numPerPage).offset(skip);
        } else if (params.limit) {
          strSQL = strSQL.limit(numPerPage);
        }
        strSQL.orderBy("t.date_created", "desc");
        strSQL.asCallback((err, results) => {
          if (err) {
            return next(err, null);
          }

          async.eachSeries(
            results,
            (item, callback1) => {
              async.parallel(
                [
                  callback2 => {
                    knex("companies")
                      .where("companyId", item.companyId)
                      .first()
                      .asCallback((err, company) => {
                        if (err) {
                          return next(err);
                        }
                        item.company = company;
                        return callback2();
                      });
                  },
                  callback2 => {
                    knex("trainees_images")
                      .where("traineesId", item.traineesId)
                      .first()
                      .asCallback((err, trainees_image) => {
                        if (err) {
                          return next(err);
                        }

                        if (trainees_image) {
                          trainees_image.public_img_path =
                            trainees_image.img_path;
                          item.image = trainees_image;
                        } else {
                          item.image = {};
                        }
                        return callback2();
                      });
                  }
                ],
                () => {
                  return callback1();
                }
              );
            },
            () => {
              return callback(null, numRows, numPages, results);
            }
          );
        });
      },
      (numRows, numPages, dataArr, callback) => {
        if (params.limit && params.startWith) {
          if (page < numPages) {
            callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages
              }
            });
          } else {
            callback(null, {
              data: dataArr,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages || 0
              }
            });
          }
        } else {
          callback(null, {
            data: dataArr,
            pagination: {
              total: numRows,
              startAt: page,
              page_size: numPerPage,
              maxResult: numPages
            }
          });
        }
      }
    ],
    next
  );
};

// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================

exports.uploadTraineeImage = async (uuid, data, next) => {
  async.waterfall(
    [
      callback => {
        knex("trainees_images")
          .where("traineesId", uuid)
          .first()
          .asCallback((err, result) => {
            if (err) {
              return next(err, null);
            }

            if (!_.isEmpty(result)) {
              return callback(null, result);
            } else {
              return callback(null, {});
            }
          });
      },
      (trainee, callback) => {
        if (!_.isEmpty(trainee)) {
          const params = {
            Bucket: config.SPACES_BUCKET,
            Key: trainee.img_key
          };
          s3.deleteObject(params, (err, data) => {
            if (err) {
              console.log(err, err.stack);
              return callback(null, trainee);
            } // an error occurred
            return callback(null, trainee);
          });
        } else {
          return callback(null, trainee);
        }
      },
      (trainee, callback) => {
        knex.transaction(trx => {
          const traineeObj = {
            img_name: data.img_name,
            img_path: data.img_path,
            img_type: data.img_type,
            img_size: data.img_size,
            img_key: data.img_key,
            fileid: data.fileid,
            folderid: data.folderid,
            traineesId: uuid || data.traineeId,
            uuid: knex.raw("UUID()")
          };

          if (_.isEmpty(trainee)) {
            return trx
              .insert(traineeObj, "imagesId")
              .into("trainees_images")
              .asCallback(callback);
          } else {
            return trx
              .update(traineeObj)
              .from("trainees_images")
              .where("traineesId", uuid)
              .asCallback(callback);
          }
        });
      }
    ],
    next
  );
};

exports.deleteTraineeImage = async (uuid, data, next) => {
  async.waterfall(
    [
      callback => {
        if (!_.isEmpty(trainee)) {
          s3.deleteObject(params, (err, data) => {
            if (err) {
              console.log(err, err.stack);
              return callback(null, trainee);
            } // an error occurred
            return callback(null, trainee);
          });
        } else {
          return callback(null, trainee);
        }
      },
      callback => {
        knex("trainees_images")
          .where(sql => {
            sql.where("traineesId", uuid);
            sql.andWhere("fileid", data.fileid);
          })
          .delete()
          .asCallback(callback);
      }
    ],
    next
  );
};
