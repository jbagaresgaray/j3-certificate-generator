'use strict';

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const config = require('../../config/config');
const _ = require("lodash");
const async = require('async');
const fs = require('fs');

const knex = require("knex")({
	client: "mysql",
	connection: db.configuration
});


exports.checkTraineeOnTraining = (traineeId, trainingId, next) => {
	knex("trainee_trainings").where("trainingId", trainingId).andWhere("traineeId", traineeId).first().asCallback(next);
};

exports.createTraineeTraining = (traineeId, data, next) => {
	async.waterfall([
		(callback) => {
			knex.select(knex.raw("MAX(CAST(tt.traineeNum AS SIGNED)) AS lastNumber")).from("trainee_trainings AS tt").innerJoin("training AS t", "tt.trainingId", "t.trainingId")
				.where("t.courseId", knex.raw("(SELECT courseId FROM training WHERE trainingId = ? LIMIT 1)", [data.trainingId])).first()
				.asCallback((err, response) => {
					if (err) {
						return next(err);
					}
					let lastNumber;
					if (_.isNull(response.lastNumber)) {
						lastNumber = "1";
					} else {
						// const padNum = parseInt(response.lastNumber) + 1;
						// lastNumber = func.padNumber(padNum);
						lastNumber = parseInt(response.lastNumber) + 1;
					}
					callback(null, lastNumber);
				});

		},
		(lastNumber, callback) => {
			knex.transaction(trx => {
				return trx.insert({
						traineeId: traineeId,
						trainingId: data.trainingId,
						traineeNum: lastNumber,
						isPass: 0,
						uuid: knex.raw('UUID()')
					}).into("trainee_trainings")
					.then((resp) => {
						return resp[0];
					})
					.asCallback(callback);
			});
		}
	], next);
};

exports.deleteTraineeTraining = (traineeId, trainingId, next) => {
	knex.transaction(trx => {
		return trx.from('trainee_trainings').where("traineeId", traineeId).andWhere("trainingId", trainingId).delete().asCallback(next);
	});
};

exports.payTraineeTraining = (traineesId, trainingId, data, next) => {
	knex.transaction(trx => {
		return trx.update({
				amountPay: data.amountPay,
				orNumber: data.orNumber,
				paymentType: data.paymentType,
				paymentTransInfo: data.paymentTransInfo,
				paymentDate: knex.raw('NOW()')
			}).from("trainee_trainings")
			.where(sql => {
				sql.where("traineeId", traineesId).orWhere("uuid", traineesId)
			}).andWhere("trainingId", trainingId)
			.asCallback(next);
	});
};

exports.markTraineeAsPass = (traineesId, trainingId, next) => {
	knex.transaction(trx => {
		return trx.update({
				isPass: 1
			}).from("trainee_trainings")
			.where(sql => {
				sql.where("traineeId", traineesId).orWhere("uuid", traineesId)
			}).andWhere("trainingId", trainingId)
			.asCallback(next);
	});
};

exports.markTraineeAsAttended = (traineesId, trainingId, next) => {
	knex.transaction(trx => {
		return trx.update({
				isAttended: 1
			}).from("trainee_trainings")
			.where(sql => {
				sql.where("traineeId", traineesId).orWhere("uuid", traineesId)
			}).andWhere("trainingId", trainingId)
			.asCallback(next);
	});
};

exports.getTraineeTrainings = (traineeId, next) => {
	knex('trainees').where("traineesId", traineeId).first().asCallback((err, result) => {
		if (err) {
			return next(err);
		}

		if (result) {
			async.parallel([
				callback => {
					knex('companies').where("companyId", result.companyId).first().asCallback((err, company) => {
						if (err) {
							return next(err);
						}

						result.company = company;
						return callback();
					});
				},
				callback => {
					knex('trainees_images').where("traineesId", result.traineesId).first().asCallback((err, trainees_image) => {
						if (err) {
							return next(err);
						}

						if (trainees_image) {
							result.image = trainees_image;
						} else {
							result.image = {};
						}
						return callback();
					});
				},
				callback => {
					knex('training AS t')
						.innerJoin("trainee_trainings AS tt", "t.trainingId", "tt.trainingId")
						.where("tt.traineeId", result.traineesId)
						.orderBy("t.date_created", "desc")
						.asCallback((err, trainings) => {
							if (err) {
								return next(err);
							}

							if (!_.isEmpty(trainings)) {
								async.eachSeries(trainings, (training, callbackEach) => {
									console.log("training: ", training);
									async.parallel([
										(callback1) => {
											knex('course').where("courseId", training.courseId).first().asCallback((err, course) => {
												if (err) {
													return next(err);
												}
												training.course = course;
												return callback1();
											});
										},
										(callback1) => {
											knex.select('s.*', 'st.trainingId').from("speakers AS s")
												.innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId")
												.where("st.trainingId", training.trainingId).asCallback((err, training_speakers) => {
													if (err) {
														return next(err);
													}
													if (training_speakers) {
														training.training_speakers = training_speakers;
													} else {
														training.training_speakers = [];
													}
													return callback1();
												});
										},
										callback1 => {
											knex('trainee_training_ledger').where(sql => {
												sql.where("traineeId", training.traineeId)
												sql.andWhere("trainingId", training.trainingId)
											}).asCallback((err, payments) => {
												console.log("err: ", err);
												console.log("payments: ", payments);
												if (err) {
													return next(err);
												}

												const trainingPrice = training.trainingPrice;
												if (payments) {
													const totalPayments = _.sumBy(payments, (row) => {
														return row.amountPay;
													});
													const totalBalance = trainingPrice - totalPayments;
													if (totalPayments > 0) {
														if (totalBalance > 0) {
															// PARTIAL
															training.isPaid = false;
															training.isPartialPaid = true;
														} else if (totalBalance < 1) {
															// PAID
															training.isPaid = true;
															training.isPartialPaid = false;
														}
													} else {
														training.isPaid = false;
														training.isPartialPaid = false;
													}
													training.totalPayments = totalPayments;
													training.totalBalance = totalBalance;

												} else {
													training.totalBalance = trainingPrice;
													training.totalPayments = 0;
													if (training.totalBalance > 0 && training.totalPayments == 0) {
														training.isPaid = false;
													}
												}
												return callback1();
											});
										},
									], () => {
										training.isPass = (training.isPass == 0) ? false : true;
										training.isAttended = (training.isAttended == 0) ? false : true;
										return callbackEach();
									});
								}, () => {
									result.trainings = trainings;
									return callback();
								});
							} else {
								result.trainings = [];
								return callback();
							}
						});
				}
			], () => {
				return next(null, result);
			});
		} else {
			return next(null, {});
		}
	});
};


// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================
// ==========================================================================================================


exports.getTraineeTrainingCertificate = (traineeId, trainingId, next) => {
	knex.select("t.*", "tt.traineeNum", "tr.trainingId", "tr.trainingPrice", "tt.isPass", "tt.isAttended")
		.from("trainees AS t")
		.innerJoin("trainee_trainings AS tt", "t.traineesId", "tt.traineeId")
		.innerJoin("training AS tr", "tr.trainingId", "tt.trainingId")
		.where("t.traineesId", traineeId)
		.andWhere("tt.trainingId", trainingId).first()
		.asCallback((err, result) => {
			if (err) {
				return next(err);
			}
			async.parallel([
				callback => {
					if (result && result.companyId) {
						knex('companies').where("companyId", result.companyId).first().asCallback((err, company) => {
							if (err) {
								return next(err);
							}

							result.company = company;
							return callback();
						});
					} else {
						result.company = {};
						return callback();
					}
				},
				callback => {
					knex('trainees_images').where("traineesId", result.traineesId).first().asCallback((err, trainees_image) => {
						if (err) {
							return next(err);
						}

						if (trainees_image) {
							trainees_image.public_img_path = trainees_image.img_path;
							result.image = trainees_image;
							return callback();
						} else {
							result.image = {};
							return callback();
						}
					});
				},
				callback => {
					knex('trainee_training_ledger').where(sql => {
						sql.where("traineeId", result.traineesId)
						sql.andWhere("trainingId", result.trainingId)
					}).asCallback((err, payments) => {
						if (err) {
							return next(err);
						}

						const trainingPrice = result.trainingPrice;
						if (payments) {
							const totalPayments = _.sumBy(payments, (row) => {
								return row.amountPay;
							});
							const totalPaymentBalance = trainingPrice - totalPayments;
							result.totalPaymentBalance = totalPaymentBalance;
							result.totalPayments = totalPayments;
							if (result.totalPaymentBalance < 1 && totalPayments > 0) {
								result.isPaid = true;
							} else {
								result.isPaid = false;
							}
						} else {
							result.totalPaymentBalance = trainingPrice;
							result.totalPayments = 0;
							if (result.totalPaymentBalance > 0 && result.totalPayment == 0) {
								result.isPaid = false;
							}
						}
						return callback();
					});
				},
				callback => {
					knex.select("t.*", "tt.traineeId", "tt.traineeTrainingId", "tt.traineeNum", "tt.isPass", "tt.isAttended")
						.from('training AS t')
						.innerJoin("trainee_trainings AS tt", "t.trainingId", "tt.trainingId")
						.where("t.trainingId", trainingId).andWhere("tt.traineeId", traineeId).first()
						.asCallback((err, training) => {
							if (err) {
								return next(err);
							}

							if (training) {
								console.log("training: ", training);
								async.parallel([
									(callback1) => {
										knex('course').where("courseId", training.courseId).first().asCallback((err, course) => {
											if (err) {
												return next(err);
											}
											training.course = course;
											return callback1();
										});
									},
									(callback1) => {
										knex.select('s.*', 'st.trainingId').from("speakers AS s").innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId").where("st.trainingId", trainingId).asCallback((err, training_speakers) => {
											if (err) {
												return next(err);
											}

											if (training_speakers) {
												training.training_speakers = training_speakers;
											} else {
												training.training_speakers = [];
											}
											return callback1();
										});
									},
									callback => {
										knex('trainee_training_ledger').where(sql => {
											sql.where("traineeId", training.traineeId)
											sql.andWhere("trainingId", training.trainingId)
										}).asCallback((err, payments) => {
											if (err) {
												return next(err);
											}

											const trainingPrice = training.trainingPrice;
											if (payments) {
												const totalPayments = _.sumBy(payments, (row) => {
													return row.amountPay;
												});
												const totalBalance = trainingPrice - totalPayments;
												if (totalPayments > 0) {
													if (totalBalance > 0) {
														// PARTIAL
														training.isPaid = false;
														training.isPartialPaid = true;
													} else if (totalBalance < 1) {
														// PAID
														training.isPaid = true;
														training.isPartialPaid = false;
													}
												} else {
													training.isPaid = false;
													training.isPartialPaid = false;
												}
												training.totalPayments = totalPayments;
												training.totalBalance = totalBalance;

											} else {
												training.totalBalance = trainingPrice;
												training.totalPayments = 0;
												if (training.totalBalance > 0 && training.totalPayments == 0) {
													training.isPaid = false;
												}
											}
											return callback();
										});
									},
									callback1 => {
										knex('payment_attachments').where("traineeTrainingId", training.traineeTrainingId).asCallback((err, payment_attachments) => {
											if (err) {
												return next(err);
											}

											if (payment_attachments) {
												_.each(payment_attachments, (row) => {
													row.public_img_path = row.img_path;
												});
												training.payment_attachments = payment_attachments;
											} else {
												training.payment_attachments = [];
											}
											return callback1();
										});
									},
								], () => {
									training.isPass = (training.isPass == 0) ? false : true;
									training.isAttended = (training.isAttended == 0) ? false : true;

									result.training = training;
									return callback();
								});
							} else {
								result.training = {};
								return callback();
							}
						});
				}
			], () => {
				return next(null, result);
			});
		});
};

exports.getTraineeTrainingCertificates = (traineeId, next) => {
	knex.select("t.*", "tt.traineeNum", "tt.trainingId", "tt.isPass", "tt.isAttended")
		.from("trainees AS t")
		.innerJoin("trainee_trainings AS tt", "t.traineesId", "tt.traineeId")
		.where("t.traineesId", traineeId)
		.orderBy("t.date_created", "desc").asCallback((err, result) => {
			if (err) {
				return next(err);
			}

			async.eachSeries(result, (item, callbackEach) => {
				async.parallel([
					callback => {
						knex('companies').where("companyId", item.companyId).first().asCallback((err, company) => {
							if (err) {
								return next(err);
							}
							item.company = company;
							return callback();
						});
					},
					callback => {
						knex('trainees_images').where("traineesId", item.traineesId).first().asCallback((err, trainees_image) => {
							if (err) {
								return next(err);
							}
							if (trainees_image) {
								trainees_image.public_img_path = trainees_image.img_path;
								item.image = trainees_image;
							} else {
								item.image = {};
							}
							return callback();
						});
					},
					callback => {
						knex.select("t.*").from('training AS t')
							.innerJoin("trainee_trainings AS tt", "t.trainingId", "tt.trainingId")
							.where("t.trainingId", item.trainingId).first()
							.asCallback((err, training) => {
								if (err) {
									return next(err);
								}

								if (training) {
									async.parallel([
										(callback1) => {
											knex('course').where("courseId", training.courseId).first().asCallback((err, course) => {
												if (err) {
													return next(err);
												}
												training.course = course;
												return callback1();
											});
										},
										(callback1) => {
											knex.select('s.*', 'st.trainingId').from("speakers AS s").innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId")
												.where("st.trainingId", item.trainingId).asCallback((err, training_speakers) => {
													if (err) {
														return next(err);
													}

													if (training_speakers) {
														training.training_speakers = training_speakers;
													} else {
														training.training_speakers = [];
													}
													return callback1();
												});
										},
										callback1 => {
											knex('trainee_training_ledger').where(sql => {
												sql.where("traineeId", item.traineesId)
												sql.andWhere("trainingId", item.trainingId)
											}).asCallback((err, payments) => {
												if (err) {
													return next(err);
												}

												const trainingPrice = training.trainingPrice;
												if (payments) {
													const totalPayments = _.sumBy(payments, (row) => {
														return row.amountPay;
													});
													const totalBalance = trainingPrice - totalPayments;
													if (totalPayments > 0) {
														if (totalBalance > 0) {
															// PARTIAL
															training.isPaid = false;
															training.isPartialPaid = true;
														} else if (totalBalance < 1) {
															// PAID
															training.isPaid = true;
															training.isPartialPaid = false;
														}
													} else {
														training.isPaid = false;
														training.isPartialPaid = false;
													}
													training.totalPayments = totalPayments;
													training.totalBalance = totalBalance;

												} else {
													training.totalBalance = trainingPrice;
													training.totalPayments = 0;
													if (training.totalBalance > 0 && training.totalPayments == 0) {
														training.isPaid = false;
													}
												}
												return callback1();
											});
										},
									], () => {
										training.isPass = (training.isPass == 0) ? false : true;
										training.isAttended = (training.isAttended == 0) ? false : true;

										item.training = training;
										return callback();
									});
								} else {
									item.training = {};
									return callback();
								}
							});
					}
				], () => {
					return callbackEach(null, item);
				});
			}, () => {
				return next(null, result);
			});
		});
};