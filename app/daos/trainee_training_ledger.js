"use strict";

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const config = require('../../config/config');

const _ = require("lodash");
const async = require('async');
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({
	accessKeyId: config.SPACES_ACCESS_KEY_ID,
	secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
	endpoint: spacesEndpoint
});

const knex = require("knex")({
	client: "mysql",
	connection: db.configuration
});


exports.checkORNumber = (orNumber, trainingId, next) => {
	console.log("checkORNumber data: ", orNumber);
	knex('trainee_training_ledger').where("orNumber", orNumber).andWhere("trainingId", trainingId).first().asCallback(next);
};

exports.checkTransactionNumber = (orNumber, trainingId, next) => {
	console.log("checkTransactionNumber data: ", orNumber);
	knex('trainee_training_ledger').where("paymentTransInfo", orNumber).asCallback(next);
};


exports.checkTraineeTrainingPaid = (traineesId, trainingId, next) => {
	async.waterfall([
		callback => {
			knex('training').where(sql => {
				sql.andWhere("trainingId", trainingId)
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err);
				}

				return callback(null, response);
			});
		},
		(training, callback) => {
			knex('trainee_training_ledger').where(sql => {
				sql.where("traineeId", traineesId)
				sql.andWhere("trainingId", trainingId)
			}).asCallback((err, response) => {
				if (err) {
					return next(err);
				}

				const totalPayment = _.sumBy(response, (row) => {
					return row.amountPay;
				});
				const totalBalance = training.trainingPrice - totalPayment;
				console.log("totalBalance: ", totalBalance);

				if (totalBalance > 0) {
					// PARTIAL
					return callback(null, false);
				} else {
					// PAID
					return callback(null, true);
				}
			});
		}
	], next);
};

exports.payTraineeTraining = (currentUser, traineesId, trainingId, data, next) => {
	async.waterfall([
		callback => {
			knex
				.select("userId")
				.from("users")
				.where("uuid", currentUser.uuid)
				.first()
				.asCallback((err, result) => {
					if (err) {
						return next(err);
					}

					return callback(null, result.userId);
				});
		},
		(userId, callback) => {
			knex.transaction(trx => {
				return trx.insert({
						traineeId: traineesId,
						trainingId: trainingId,
						traineeNum: data.traineeNum,
						paymentType: data.paymentType,
						paymentTransInfo: data.paymentTransInfo,
						amountPay: data.amountPay,
						orNumber: data.orNumber,
						paymentDate: data.paymentDate,
						paidBy: data.paidBy,
						receivedBy: data.receivedBy,
						receivingDate: data.receivingDate,
						createdBy: userId,
						date_created: knex.raw('NOW()'),
						uuid: knex.raw("UUID()")
					})
					.into("trainee_training_ledger")
					.then((resp) => {
						return resp[0];
					})
					.asCallback(next);
			});
		}
	]);
};

exports.uploadTraineeTrainingPaymentProof = (uuid, trainingId, data, next) => {
	knex.transaction(trx => {
		const traineeObj = {
			img_name: data.img_name,
			img_path: data.img_path,
			img_type: data.img_type,
			img_size: data.img_size,
			fileid: data.fileid,
			folderid: data.folderid,
			img_key: data.img_key,
			traineeTrainingId: data.traineeTrainingId,
			uuid: knex.raw('UUID()')
		};
		return trx.insert(traineeObj, 'payment_attachmentId').into("payment_attachments").asCallback(next);
	});
};

exports.deleteTraineeTrainingPaymentProof = (uuid, trainingId, imageId, next) => {
	async.waterfall([
		(callback) => {
			let sSQL = knex("payment_attachments").where("traineeTrainingId", knex.select("traineeTrainingId").from("trainee_trainings").where(sql => {
				sql.where("traineeId", uuid).andWhere("trainingId", trainingId).first()
			})).andWhere("payment_attachmentId", imageId).first();
			sSQL.asCallback((err, result) => {
				if (err) {
					return next(err, null);
				}
				console.log("payment_attachments: ", result);
				if (!_.isEmpty(result)) {
					return callback(null, result);
				} else {
					return callback(null, {});
				}
			});
		},
		(trainee, callback) => {
			if (!_.isEmpty(trainee)) {
				const params = {
					Bucket: config.SPACES_BUCKET,
					Key: trainee.img_key
				}
				console.log("params: ", params);
				s3.deleteObject(params, (err, data) => {
					if (err) {
						console.log(err, err.stack);
						return callback(null, trainee);
					} // an error occurred
					console.log("file deleted Successfully");
					return callback(null, trainee);
				});
			} else {
				return callback(null, trainee);
			}
		},
		(trainee, callback) => {
			knex('payment_attachments').where("traineeTrainingId", knex.select("traineeTrainingId").from("trainee_trainings").where(sql => {
				sql.where("traineeId", uuid).andWhere("trainingId", trainingId).first()
			})).andWhere('payment_attachmentId', imageId).delete().asCallback(callback);
		}
	], next);
};

exports.getTraineeTrainingPayments = (traineeId, trainingId, next) => {
	knex('trainee_training_ledger').where("traineeId", traineeId).andWhere('trainingId', trainingId)
		.asCallback((err, results) => {
			if (err) {
				return next(err);
			}

			async.eachSeries(results, (item, callback) => {
				knex
					.select("userId", "username", "firstname", "lastname", "email").from("users")
					.where("userId", item.createdBy).first()
					.asCallback((err, result) => {
						if (err) {
							return next(err);
						}

						item.createdByUser = result;
						return callback();
					});
			}, () => {
				const totalPayments = _.sumBy(results, (row) => {
					return row.amountPay;
				});

				return next(null, {
					payments: results,
					totalPayments: totalPayments
				});
			});
		});
};