"use strict";

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const config = require('../../config/config');

const _ = require("lodash");
const async = require('async');
const fs = require('fs');

const AWS = require('aws-sdk');
AWS.config.update({
	accessKeyId: config.SPACES_ACCESS_KEY_ID,
	secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
	endpoint: spacesEndpoint
});

const knex = require("knex")({
	client: "mysql",
	connection: db.configuration
});



exports.insertReimbursement = (currentUser, trainingId, data, next) => {
	async.waterfall([
		callback => {
			knex
				.select("userId")
				.from("users")
				.where("uuid", currentUser.uuid)
				.first()
				.asCallback((err, result) => {
					if (err) {
						return next(err);
					}

					return callback(null, result.userId);
				});
		},
		(userId, callback) => {
			knex.transaction(trx => {
				return trx.insert({
						trainingId: trainingId,
						traineeId: data.traineeId,
						traineeNum: data.traineeNum,
						account_groupId: data.account_groupId,
						account_classId: data.account_classId,
						paymentType: data.paymentType,
						TransDate: data.TransDate,
						TransCode: data.TransCode,
						Referenceno: data.Referenceno,
						orNumber: data.orNumber,
						Amount: data.Amount,
						reimburseDate: data.reimburseDate,
						Remarks: data.Remarks,
						receivedBy: data.receivedBy,
						receivingDate: data.receivingDate,
						createdBy: userId,
						createdOn: knex.raw('NOW()'),
						uuid: knex.raw("UUID()")
					})
					.into("training_reimbursement")
					.then((resp) => {
						return resp[0];
					})
					.asCallback(callback);
			});
		}
	], next);
};

exports.updateReimbursement = (transId, trainingId, data, next) => {
	knex.transaction(trx => {
		return trx.update({
				paymentType: data.paymentType,
				TransDate: data.TransDate,
				TransCode: data.TransCode,
				Referenceno: data.Referenceno,
				orNumber: data.orNumber,
				Amount: data.Amount,
				reimburseDate: data.reimburseDate,
				Remarks: data.Remarks,
				receivedBy: data.receivedBy,
				receivingDate: data.receivingDate,
			})
			.from("training_reimbursement").where(sql => {
				sql.where("uuid", transId)
				sql.orWhere("training_reimbursementId", transId);
			}).andWhere("trainingId", trainingId)
			.asCallback(next);
	});
};

exports.deleteReimbursement = (transId, trainingId, next) => {
	knex.transaction(trx => {
		return trx.from('training_reimbursement').where(sql => {
				sql.where("uuid", transId)
				sql.orWhere("training_reimbursementId", transId);
			}).andWhere("trainingId", trainingId)
			.delete()
			.asCallback(next);
	});
};

exports.checkIfReimbursementIsPosted = (transId, trainingId, next) => {
	knex("training_reimbursement").where(sql => {
			sql.where("uuid", transId)
			sql.orWhere("training_reimbursementId", transId);
		}).andWhere("trainingId", trainingId).whereNotNull("PostedDate")
		.first()
		.asCallback(next);
};

exports.uploadTraineeTrainingReimbursementProof = (uuid, trainingId, data, next) => {
	knex.transaction(trx => {
		return trx.insert({
				img_name: data.img_name,
				img_path: data.img_path,
				img_type: data.img_type,
				img_size: data.img_size,
				fileid: data.fileid,
				folderid: data.folderid,
				img_key: data.img_key,
				traineeTrainingId: data.traineeTrainingId,
				uuid: knex.raw('UUID()')
			}).into("reimbursement_attachments")
			.then((resp) => {
				return resp[0];
			})
			.asCallback(next);
	});
};

exports.postTransactionReimbursement = (currentUser, transId, trainingId, next) => {
	async.waterfall([
		callback => {
			knex
				.select("userId")
				.from("users")
				.where("uuid", currentUser.uuid)
				.first()
				.asCallback((err, result) => {
					if (err) {
						return next(err);
					}

					return callback(null, result.userId);
				});
		},
		(userId, callback) => {
			knex.transaction(trx => {
				return trx.update({
						PostedBy: userId,
						PostedDate: knex.raw('NOW()'),
					})
					.from("training_reimbursement").where(sql => {
						sql.where("uuid", transId)
						sql.orWhere("training_reimbursementId", transId);
					}).andWhere("trainingId", trainingId)
					.asCallback(callback);
			});
		}
	], next);
};


exports.getTrainingReimbursementDetail = (transId, trainingId, next) => {
	knex("training_reimbursement").where(sql => {
			sql.where("uuid", transId)
			sql.orWhere("training_reimbursementId", transId);
		}).andWhere("trainingId", trainingId)
		.first()
		.asCallback((err, result) => {
			if (err) {
				return next(err);
			}

			async.parallel([
				callback1 => {
					knex('account_groups').where("account_groupId", result.account_groupId).first().asCallback((err, group) => {
						if (err) {
							return next(err);
						}
						result.accountGroup = group;
						return callback1();
					});
				},
				callback1 => {
					knex('trainees').where("traineesId", result.traineeId).first().asCallback((err, trainee) => {
						if (err) {
							return next(err);
						}
						result.trainee = trainee;
						return callback1();
					});
				},
				callback1 => {
					knex
						.select("userId", "username", "firstname", "lastname", "email").from("users")
						.where("userId", result.createdBy).first()
						.asCallback((err, user) => {
							if (err) {
								return next(err);
							}

							result.createdByUser = user;
							return callback1();
						});
				}
			], () => {
				return result;
			});
		})
		.asCallback(next);
};

exports.deleteTraineeTrainingReimbursementProof = (uuid, trainingId, imageId, next) => {
	async.waterfall([
		(callback) => {
			let sSQL = knex("reimbursement_attachments").where("traineeTrainingId", knex.select("training_reimbursementId").from("training_reimbursement").where(sql => {
				sql.where("traineeId", uuid).andWhere("trainingId", trainingId).first()
			})).andWhere("reimbursement_attachmentId", imageId).first();
			sSQL.asCallback((err, result) => {
				if (err) {
					return next(err, null);
				}
				console.log("reimbursement_attachments: ", result);
				if (!_.isEmpty(result)) {
					return callback(null, result);
				} else {
					return callback(null, {});
				}
			});
		},
		(trainee, callback) => {
			if (!_.isEmpty(trainee)) {
				const params = {
					Bucket: config.SPACES_BUCKET,
					Key: trainee.img_key
				}
				console.log("params: ", params);
				s3.deleteObject(params, (err, data) => {
					if (err) {
						console.log(err, err.stack);
						return callback(null, trainee);
					} // an error occurred
					console.log("file deleted Successfully");
					return callback(null, trainee);
				});
			} else {
				return callback(null, trainee);
			}
		},
		(trainee, callback) => {
			knex('reimbursement_attachments').where("traineeTrainingId", knex.select("traineeTrainingId").from("training_reimbursement").where(sql => {
				sql.where("traineeId", uuid).andWhere("trainingId", trainingId).first()
			})).andWhere('reimbursement_attachmentId', imageId).delete().asCallback(callback);
		}
	], next);
};


exports.getAllTrainingReimbursements = (params, trainingId, next) => {
	if (!_.isEmpty(params)) {
		if (params.limit <= func.PAGINATE_MAX_COUNT) {
			params.limit = func.PAGINATE_MAX_COUNT;
		}
	} else {
		params.limit = func.PAGINATE_MAX_COUNT;
		params.startWith = 0;
	}

	let numRows;
	const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
	let page = parseInt(params.startWith) || 1;
	page = page - 1;
	let numPages;
	const skip = page * numPerPage;
	const limit = skip + "," + numPerPage;

	async.waterfall(
		[
			(callback) => {
				let strSQL = knex.select(knex.raw('COUNT(*) AS numRows')).from("vw_training_transactions AS t").where("trainingId", trainingId);

				strSQL.asCallback((err, results) => {
					if (err) {
						return next(err, null);
					}
					numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
					numPages = Math.ceil(numRows / numPerPage);
					return callback(null, numRows, numPages);
				});
			},
			(numRows, numPages, callback) => {
				let strSQL = knex.select("t.*").from("vw_training_transactions AS t").where("trainingId", trainingId);
				if (params.limit && params.startWith) {
					strSQL = strSQL.limit(numPerPage).offset(skip);
				} else if (params.limit) {
					strSQL = strSQL.limit(numPerPage);
				}
				strSQL.orderBy('t.createdOn', "desc")
					.asCallback((err, results) => {
						if (err) {
							return next(err, null);
						}

						async.eachSeries(results, (item, cb) => {
							async.parallel([
								callback1 => {
									knex('account_groups').where("account_groupId", item.account_groupId).first().asCallback((err, group) => {
										if (err) {
											return next(err);
										}
										item.accountGroup = group;
										return callback1();
									});
								},
								callback1 => {
									knex
										.select("userId", "username", "firstname", "lastname", "email").from("users")
										.where("userId", item.createdBy).first()
										.asCallback((err, user) => {
											if (err) {
												return next(err);
											}

											item.createdByUser = user;
											return callback1();
										});
								}
							], () => {
								return cb();
							});
						}, () => {
							return callback(null, numRows, numPages, results);
						});
					});
			},
			(numRows, numPages, dataArr, callback) => {
				if (params.limit && params.startWith) {
					if (page < numPages) {
						callback(null, {
							data: dataArr,
							pagination: {
								total: numRows,
								startAt: page + 1,
								page_size: numPerPage,
								maxResult: numPages
							}
						});
					} else {
						callback(null, {
							data: dataArr,
							pagination: {
								total: numRows,
								startAt: page,
								page_size: numPerPage,
								maxResult: numPages || 0
							}
						});
					}
				} else {
					callback(null, {
						data: dataArr,
						pagination: {
							total: numRows,
							startAt: 0,
							page_size: 0,
							maxResult: 0
						}
					});
				}
			}
		],
		next
	);
};