'use strict';

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const _ = require("lodash");
const async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkSpeakerName = (data, next) => {
    knex('speakers').where((sql) => {
        sql.where("trainingSpeaker", "LIKE", "'%" + data.trainingSpeaker + "%'")
    }).asCallback(next);
};

exports.checkSpeakerNameOnUpdate = (speakerId, data, next) => {
    knex('speakers').where((sql) => {
        sql.where("trainingSpeaker", "LIKE", "'%" + data.trainingSpeaker + "%'")
        sql.andWhere("speakerId", "<>", speakerId)
    }).asCallback(next);
};


exports.createTrainingSpeaker = (user, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId').from('users').where("uuid", user.uuid).first().asCallback((err, result) => {
                if (err) {
                    return next(err);
                }

                console.log("result: ", result);
                return callback(null, result.userId);
            });
        },
        (userId, callback) => {
            knex.transaction(sql => {
                return sql.insert({
                        trainingSpeaker: data.trainingSpeaker,
                        trainingSpeakerSalutation: data.trainingSpeakerSalutation,
                        trainingSpeakerAccrNo: data.trainingSpeakerAccrNo,
                        date_created: knex.raw('NOW()'),
                        createdBy: userId,
                        uuid: knex.raw('UUID()'),
                    })
                    .into('speakers')
                    .then((resp) => {
                        return resp[0];
                    })
                    .asCallback(callback);
            });
        }
    ], next);
};


exports.updateTrainingSpeaker = (speakerId, data, next) => {
    knex.transaction(trx => {
        return trx.update({
                trainingSpeaker: data.trainingSpeaker,
                trainingSpeakerSalutation: data.trainingSpeakerSalutation,
                trainingSpeakerAccrNo: data.trainingSpeakerAccrNo,
                trainingId: data.trainingId,
            })
            .from('speakers')
            .where(sql => {
                sql.where("speakerId", speakerId).orWhere("uuid", speakerId)
            })
            .asCallback(next);
    });
};

exports.getSpeakerDetails = (speakerId, next) => {
    knex('speakers').where(sql => {
        sql.where("speakerId", speakerId).orWhere("uuid", speakerId)
    }).first().asCallback(next);
};

exports.deleteTrainingSpeaker = (speakerId, next) => {
    knex.transaction(trx => {
        return trx.from('speakers').where(sql => {
            sql.where("speakerId", speakerId).orWhere("uuid", speakerId)
        }).delete().asCallback(next);
    });
};

exports.getAllSpeakersByTraining = (params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= func.PAGINATE_MAX_COUNT) {
            params.limit = func.PAGINATE_MAX_COUNT;
        }
    } else {
        params.limit = func.PAGINATE_MAX_COUNT;
        params.startWith = 0;
    }

    let numRows;
    const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
    let page = parseInt(params.startWith) || 1;
    page = page - 1;
    let numPages;
    const skip = page * numPerPage;
    const limit = skip + "," + numPerPage;

    if (page == 0) {
        page = 1;
    } else {
        page += 1;
    }

    async.waterfall(
        [
            callback => {
                let strSQL = knex("speakers").count("* as numRows");
                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("trainingSpeaker", "LIKE", "%" + params.search + "%")
                            .orWhere("trainingSpeakerSalutation", "LIKE", "%" + params.search + "%")
                            .orWhere("trainingSpeakerAccrNo", "LIKE", "%" + params.search + "%");
                    });
                }
                strSQL.asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }

                    numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("speakers");
                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("trainingSpeaker", "LIKE", "%" + params.search + "%")
                            .orWhere("trainingSpeakerSalutation", "LIKE", "%" + params.search + "%")
                            .orWhere("trainingSpeakerAccrNo", "LIKE", "%" + params.search + "%");
                    });
                }
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }
                strSQL.orderBy("date_created", "desc").asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }
                    return callback(null, numRows, numPages, results);
                });
            },
            (numRows, numPages, dataArr, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        data: dataArr,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                }
            }
        ],
        next
    );
};