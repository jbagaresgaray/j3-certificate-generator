"use strict";

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const config = require('../../config/config');
const _ = require("lodash");
const async = require('async');
const fs = require('fs');

const knex = require("knex")({
	client: "mysql",
	connection: db.configuration
});



exports.insertIncomeExpense = (currentUser, trainingId, data, next) => {
	async.waterfall([
		callback => {
			knex
				.select("userId")
				.from("users")
				.where("uuid", currentUser.uuid)
				.first()
				.asCallback((err, result) => {
					if (err) {
						return next(err);
					}

					return callback(null, result.userId);
				});
		},
		(userId, callback) => {
			knex.transaction(trx => {
				return trx.insert({
						trainingId: trainingId,
						account_groupId: data.account_groupId,
						account_classId: data.account_classId,
						TransDate: data.TransDate,
						TransCode: data.TransCode,
						Referenceno: data.Referenceno,
						Amount: data.Amount,
						Remarks: data.Remarks,
						createdBy: userId,
						createdOn: knex.raw('NOW()'),
						uuid: knex.raw("UUID()")
					})
					.into("training_transactions")
					.then((resp) => {
						return resp[0];
					})
					.asCallback(callback);
			});
		}
	], next);
};

exports.updateIncomeExpense = (transId, trainingId, data, next) => {
	knex.transaction(trx => {
		return trx.update({
				account_groupId: data.account_groupId,
				TransDate: data.TransDate,
				TransCode: data.TransCode,
				Referenceno: data.Referenceno,
				Amount: data.Amount,
				Remarks: data.Remarks,
			})
			.from("training_transactions").where(sql => {
				sql.where("uuid", transId)
				sql.orWhere("training_transactions", transId);
			}).andWhere("trainingId", trainingId)
			.asCallback(next);
	});
};

exports.deleteIncomeExpense = (transId, trainingId, next) => {
	knex.transaction(trx => {
		return trx.from('training_transactions').where(sql => {
				sql.where("uuid", transId)
				sql.orWhere("training_transactions", transId);
			}).andWhere("trainingId", trainingId)
			.delete()
			.asCallback(next);
	});
};

exports.checkIfTransactionIncomeExpenseIsPosted = (transId, trainingId, next) => {
	knex("training_transactions").where(sql => {
			sql.where("uuid", transId)
			sql.orWhere("training_transactions", transId);
		}).andWhere("trainingId", trainingId).whereNotNull("PostedDate")
		.first()
		.asCallback(next);
};

exports.postTransactionIncomeExpense = (currentUser, transId, trainingId, next) => {
	async.waterfall([
		callback => {
			knex
				.select("userId")
				.from("users")
				.where("uuid", currentUser.uuid)
				.first()
				.asCallback((err, result) => {
					if (err) {
						return next(err);
					}

					return callback(null, result.userId);
				});
		},
		(userId, callback) => {
			knex.transaction(trx => {
				return trx.update({
						PostedBy: userId,
						PostedDate: knex.raw('NOW()'),
					})
					.from("training_transactions").where(sql => {
						sql.where("uuid", transId)
						sql.orWhere("training_transactions", transId);
					}).andWhere("trainingId", trainingId)
					.asCallback(callback);
			});
		}
	], next);
};


exports.getTrainingTransaction = (transId, trainingId, next) => {
	knex("training_transactions").where(sql => {
			sql.where("uuid", transId)
			sql.orWhere("training_transactions", transId);
		}).andWhere("trainingId", trainingId)
		.first()
		.asCallback((err, result) => {
			if (err) {
				return next(err);
			}

			async.parallel([
				callback1 => {
					knex('account_groups').where("account_groupId", result.account_groupId).first().asCallback((err, group) => {
						if (err) {
							return next(err);
						}
						result.accountGroup = group;
						return callback1();
					});
				},
				callback1 => {
					knex
						.select("userId", "username", "firstname", "lastname", "email").from("users")
						.where("userId", result.createdBy).first()
						.asCallback((err, user) => {
							if (err) {
								return next(err);
							}

							result.createdByUser = user;
							return callback1();
						});
				}
			], () => {
				return result;
			});
		})
		.asCallback(next);
};


exports.getAllTrainingTransactions = (params, trainingId, next) => {
	if (!_.isEmpty(params)) {
		if (params.limit <= func.PAGINATE_MAX_COUNT) {
			params.limit = func.PAGINATE_MAX_COUNT;
		}
	} else {
		params.limit = func.PAGINATE_MAX_COUNT;
		params.startWith = 0;
	}

	let numRows;
	const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
	let page = parseInt(params.startWith) || 1;
	page = page - 1;
	let numPages;
	const skip = page * numPerPage;
	const limit = skip + "," + numPerPage;

	async.waterfall(
		[
			(callback) => {
				let strSQL = knex.select(knex.raw('COUNT(*) AS numRows')).from("vw_training_transactions AS t").where("trainingId", trainingId);

				strSQL.asCallback((err, results) => {
					if (err) {
						return next(err, null);
					}
					numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
					numPages = Math.ceil(numRows / numPerPage);
					return callback(null, numRows, numPages);
				});
			},
			(numRows, numPages, callback) => {
				let strSQL = knex.select("t.*").from("vw_training_transactions AS t").where("trainingId", trainingId);
				if (params.limit && params.startWith) {
					strSQL = strSQL.limit(numPerPage).offset(skip);
				} else if (params.limit) {
					strSQL = strSQL.limit(numPerPage);
				}
				strSQL.orderBy('t.createdOn', "desc")
					.asCallback((err, results) => {
						if (err) {
							return next(err, null);
						}

						async.eachSeries(results, (item, cb) => {
							async.parallel([
								callback1 => {
									knex('account_groups').where("account_groupId", item.account_groupId).first().asCallback((err, group) => {
										if (err) {
											return next(err);
										}
										item.accountGroup = group;
										return callback1();
									});
								},
								callback1 => {
									knex
										.select("userId", "username", "firstname", "lastname", "email").from("users")
										.where("userId", item.createdBy).first()
										.asCallback((err, user) => {
											if (err) {
												return next(err);
											}

											item.createdByUser = user;
											return callback1();
										});
								}
							], () => {
								return cb();
							});
						}, () => {
							return callback(null, numRows, numPages, results);
						});
					});
			},
			(numRows, numPages, dataArr, callback) => {
				if (params.limit && params.startWith) {
					if (page < numPages) {
						callback(null, {
							data: dataArr,
							pagination: {
								total: numRows,
								startAt: page + 1,
								page_size: numPerPage,
								maxResult: numPages
							}
						});
					} else {
						callback(null, {
							data: dataArr,
							pagination: {
								total: numRows,
								startAt: page,
								page_size: numPerPage,
								maxResult: numPages || 0
							}
						});
					}
				} else {
					callback(null, {
						data: dataArr,
						pagination: {
							total: numRows,
							startAt: 0,
							page_size: 0,
							maxResult: 0
						}
					});
				}
			}
		],
		next
	);
};