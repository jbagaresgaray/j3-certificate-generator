'use strict';

const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const config = require("../../config/config");

const _ = require("lodash");
const async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

/* exports.checkCourseCode = (data, next) => {
    knex('users').where("courseCode", data.courseCode).asCallback(next);
};

exports.checkCourseCodeUpdate = (courseId, data, next) => {
    knex('users').where("courseCode", data.courseCode).andWhere("courseId", "<>", courseId).asCallback(next);
}; */

exports.createTrainings = (user, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId').from('users').where("uuid", user.uuid).first().asCallback((err, result) => {
                if (err) {
                    return next(err);
                }

                return callback(null, result.userId);
            });
        },
        (userId, callback) => {
            knex.transaction(sql => {
                return sql.insert({
                        courseId: data.courseId,
                        trainingDesc: data.trainingDesc,
                        trainingSchedFrom: data.trainingSchedFrom,
                        trainingSchedTo: data.trainingSchedTo,
                        trainingVenue: data.trainingVenue,
                        trainingHours: data.trainingHours,
                        trainingPrice: data.trainingPrice,
                        status: func.TRAINING_STATUS.OPEN,
                        date_created: knex.raw('NOW()'),
                        createdBy: userId,
                        uuid: knex.raw('UUID()'),
                    })
                    .into('training')
                    .then((resp) => {
                        return resp[0];
                    })
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.updateTrainings = (trainingId, data, next) => {
    knex.transaction(trx => {
        return trx.update({
                courseId: data.courseId,
                trainingDesc: data.trainingDesc,
                trainingSchedFrom: data.trainingSchedFrom,
                trainingSchedTo: data.trainingSchedTo,
                trainingVenue: data.trainingVenue,
                trainingHours: data.trainingHours,
                trainingPrice: data.trainingPrice,
                status: data.status,
            })
            .from('training')
            .where(sql => {
                sql.where("trainingId", trainingId).orWhere("uuid", trainingId)
            })
            .asCallback(next);
    });
};

exports.updateTrainingStatus = (trainingId, data, next) => {
    knex.transaction(trx => {
        return trx.update({
                status: data.status
            })
            .from('training')
            .where(sql => {
                sql.where("trainingId", trainingId).orWhere("uuid", trainingId)
            })
            .asCallback(next);
    });
};

exports.createTrainingSpeakers = (trainingId, data, next) => {
    knex.transaction(trx => {
        return trx.insert({
                speakerId: data.speakerId,
                trainingId: trainingId,
                date_created: knex.raw('NOW()'),
                uuid: knex.raw('UUID()')
            })
            .into('speakers_training')
            .then((resp) => {
                return resp[0];
            })
            .asCallback(next);
    });
};

exports.getTrainingDetails = (trainingId, next) => {
    knex('training').where("trainingId", trainingId).first().asCallback((err, result) => {
        if (err) {
            return next(err);
        }

        if (result) {
            async.parallel([
                (callback) => {
                    knex('course').where("courseId", result.courseId).first().asCallback((err, course) => {
                        if (err) {
                            return next(err);
                        }
                        result.course = course;
                        return callback();
                    });
                },
                (callback) => {
                    knex.select('s.*', 'st.trainingId').from("speakers AS s").innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId").where("st.trainingId", trainingId).asCallback((err, training_speakers) => {
                        if (err) {
                            return next(err);
                        }

                        if (training_speakers) {
                            result.training_speakers = training_speakers;
                        } else {
                            result.training_speakers = [];
                        }
                        return callback();
                    });
                },
                (callback) => {
                    knex.select("*").from('trainees AS t')
                        .innerJoin("trainee_trainings AS tr", "t.traineesId", "tr.traineeId")
                        .where("tr.trainingId", result.trainingId)
                        .orderBy("tr.date_created", "asc")
                        .asCallback((err, trainees) => {
                            if (err) {
                                return next(err);
                            }
                            result.total_trainees = _.size(trainees);
                            result.total_xxs = _.size(_.filter(trainees, {
                                'shirtSize': "XXS"
                            }));
                            result.total_xs = _.size(_.filter(trainees, {
                                'shirtSize': "XS"
                            }));
                            result.total_s = _.size(_.filter(trainees, {
                                'shirtSize': "S"
                            }));
                            result.total_m = _.size(_.filter(trainees, {
                                'shirtSize': "M"
                            }));
                            result.total_l = _.size(_.filter(trainees, {
                                'shirtSize': "L"
                            }));
                            result.total_xl = _.size(_.filter(trainees, {
                                'shirtSize': "XL"
                            }));
                            result.total_xxl = _.size(_.filter(trainees, {
                                'shirtSize': "XXL"
                            }));
                            result.total_xxxl = _.size(_.filter(trainees, {
                                'shirtSize': "XXXL"
                            }));

                            async.eachSeries(trainees, (item, callbackEach) => {
                                async.parallel([
                                    callback1 => {
                                        knex('companies').where("companyId", item.companyId).first().asCallback((err, company) => {
                                            if (err) {
                                                return next(err);
                                            }
                                            item.company = company;
                                            return callback1();
                                        });
                                    },
                                    callback1 => {
                                        knex('trainee_training_ledger').where("traineeId", item.traineesId).andWhere("trainingId", trainingId).asCallback((err, payments) => {
                                            if (err) {
                                                return next(err);
                                            }

                                            const totalPayments = _.sumBy(payments, (row) => {
                                                return row.amountPay;
                                            });
                                            const totalBalance = result.trainingPrice - totalPayments;
                                            if (totalPayments > 0) {
                                                if (totalBalance > 0) {
                                                    // PARTIAL
                                                    item.isPaid = false;
                                                    item.isPartialPaid = true;
                                                } else if (totalBalance < 1) {
                                                    // PAID
                                                    item.isPaid = true;
                                                    item.isPartialPaid = false;
                                                }
                                            } else {
                                                item.isPaid = false;
                                                item.isPartialPaid = false;
                                            }

                                            item.totalPayments = totalPayments;
                                            item.totalBalance = totalBalance;

                                            return callback1();
                                        });
                                    },
                                    callback1 => {
                                        knex('trainees_images').where("traineesId", item.traineesId).first().asCallback((err, trainees_image) => {
                                            if (err) {
                                                return next(err);
                                            }
                                            if (trainees_image) {
                                                trainees_image.public_img_path = trainees_image.img_path;
                                                item.image = trainees_image;
                                            } else {
                                                item.image = {};
                                            }
                                            return callback1();
                                        });
                                    }
                                ], () => {
                                    item.isPass = (item.isPass == 1) ? true : false;
                                    item.isAttended = (item.isAttended == 1) ? true : false;
                                    return callbackEach(null, item);
                                });
                            }, () => {
                                result.total_paid_trainess = _.size(_.filter(trainees, {
                                    'isPaid': true
                                }));
                                result.total_passed_trainess = _.size(_.filter(trainees, {
                                    'isPass': true
                                }));
                                result.trainees = trainees;
                                return callback();
                            });

                        });
                },
            ], () => {
                return next(null, result);
            });
        } else {
            return next(null, {});
        }
    });
};

exports.deleteTraining = (trainingId, next) => {
    knex.transaction(trx => {
        return trx.from('training').where("trainingId", trainingId).delete()
    }).asCallback(next);
};

exports.deleteTrainingSpeakers = (trainingId, speakerId, next) => {
    knex.transaction(trx => {
        return trx.from('speakers_training').where("trainingId", trainingId).andWhere("speakerId", speakerId).delete()
    }).asCallback(next);
};

exports.getTrainingTrainees = (trainingId, next) => {
    knex('training AS t').innerJoin("trainee_trainings AS tt", "t.trainingId", "tt.trainingId").where("t.trainingId", trainingId).asCallback(next);
};

exports.getAllTrainings = (params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= func.PAGINATE_MAX_COUNT) {
            params.limit = func.PAGINATE_MAX_COUNT;
        }
    } else {
        params.limit = func.PAGINATE_MAX_COUNT;
        params.startWith = 0;
    }

    let numRows;
    const numPerPage = parseInt(params.limit) || func.PAGINATE_MAX_COUNT;
    let page = parseInt(params.startWith) || 1;
    page = page - 1;
    let numPages;
    const skip = page * numPerPage;
    const limit = skip + "," + numPerPage;

    if (page == 0) {
        page = 1;
    } else {
        page += 1;
    }

    async.waterfall(
        [
            (callback) => {
                let strSQL = knex.select(knex.raw('COUNT(*) AS numRows')).from("training AS t").innerJoin("course AS c", "t.courseId", "c.courseId");

                if (params.courseId) {
                    strSQL.where("t.courseId", params.courseId);
                }

                if (params.dateFrom && params.dateTo) {
                    strSQL.where(sql => {
                        sql.whereBetween("t.trainingSchedFrom", [params.dateFrom, params.dateTo])
                        sql.orWhere(sql1 => {
                            sql1.whereBetween("t.trainingSchedTo", [params.dateFrom, params.dateTo])
                        });
                    })
                }

                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("c.courseCode", "LIKE", "%" + params.search + "%")
                            .orWhere("c.courseName", "LIKE", "%" + params.search + "%")
                            .orWhere("c.courseSlug", "LIKE", "%" + params.search + "%")
                            .orWhere("t.trainingDesc", "LIKE", "%" + params.search + "%")
                            .orWhere("t.trainingVenue", "LIKE", "%" + params.search + "%");
                    });
                }

                strSQL.asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }
                    numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("t.*").from("training AS t").innerJoin("course AS c", "t.courseId", "c.courseId");

                if (params.courseId) {
                    strSQL.where("t.courseId", params.courseId);
                }

                if (params.dateFrom && params.dateTo) {
                    strSQL.where(sql => {
                        sql.whereBetween("t.trainingSchedFrom", [params.dateFrom, params.dateTo])
                        sql.orWhere(sql1 => {
                            sql1.whereBetween("t.trainingSchedTo", [params.dateFrom, params.dateTo])
                        });
                    })
                }

                if (params.search) {
                    strSQL.where(sql => {
                        sql.where("c.courseCode", "LIKE", "%" + params.search + "%")
                            .orWhere("c.courseName", "LIKE", "%" + params.search + "%")
                            .orWhere("c.courseSlug", "LIKE", "%" + params.search + "%")
                            .orWhere("t.trainingDesc", "LIKE", "%" + params.search + "%")
                            .orWhere("t.trainingVenue", "LIKE", "%" + params.search + "%");
                    });
                }

                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }
                strSQL.orderBy('t.date_created', "desc")

                console.log("strSQL: ", strSQL.toString());
                strSQL.asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    }

                    async.eachSeries(results, (item, cb) => {
                        async.parallel([
                            (callback1) => {
                                knex('course').where("courseId", item.courseId).first().asCallback((err, course) => {
                                    if (err) {
                                        return next(err);
                                    }
                                    item.course = course;
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex.select('s.*', 'st.trainingId').from("speakers AS s").innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId").where("st.trainingId", item.trainingId).asCallback((err, training_speakers) => {
                                    if (err) {
                                        return next(err);
                                    }
                                    if (training_speakers) {
                                        item.training_speakers = training_speakers;
                                    } else {
                                        item.training_speakers = [];
                                    }
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex.select(knex.raw('COUNT(*) AS numRows'))
                                    .from('trainees AS t')
                                    .innerJoin("trainee_trainings AS tr", "t.traineesId", "tr.traineeId")
                                    .where("tr.trainingId", item.trainingId)
                                    .first()
                                    .asCallback((err, trainees) => {
                                        if (err) {
                                            return next(err);
                                        }
                                        item.total_trainees = trainees.numRows;
                                        // item.total_trainees = _.size(trainees);
                                        // item.trainees = trainees;
                                        return callback1();
                                    });
                            },
                        ], () => {
                            return cb();
                        });
                    }, () => {
                        return callback(null, numRows, numPages, results);
                    });
                });
            },
            (numRows, numPages, dataArr, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        data: dataArr,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                }
            }

        ],
        next
    );
};

exports.getTrainingTraineesCertificates = (trainingId, params, next) => {
    let strSQL = knex.select("t.*", "tt.traineeNum", "tr.trainingId", "tr.trainingPrice", "tt.isPass","tt.isAttended")
        .from("trainees AS t")
        .innerJoin("trainee_trainings AS tt", "t.traineesId", "tt.traineeId")
        .innerJoin("training AS tr", "tr.trainingId", "tt.trainingId")
        .where("tt.trainingId", trainingId)
        .orderBy("t.date_created", "desc");

    strSQL.asCallback((err, result) => {
        if (err) {
            return next(err);
        }
        async.eachSeries(result, (item, callbackEach) => {
            async.parallel([
                callback => {
                    knex('companies').where("companyId", item.companyId).first().asCallback((err, company) => {
                        if (err) {
                            return next(err);
                        }
                        item.company = company;
                        return callback();
                    });
                },
                callback => {
                    knex('trainees_images').where("traineesId", item.traineesId).first().asCallback((err, trainees_image) => {
                        if (err) {
                            return next(err);
                        }
                        if (trainees_image) {
                            trainees_image.public_img_path = trainees_image.img_path;
                            item.image = trainees_image;
                        } else {
                            item.image = {};
                        }
                        return callback();
                    });
                },
                callback => {
                    knex('trainee_training_ledger').where(sql => {
                        sql.where("traineeId", item.traineesId)
                        sql.andWhere("trainingId", item.trainingId)
                    }).asCallback((err, payments) => {
                        if (err) {
                            return next(err);
                        }

                        const trainingPrice = item.trainingPrice;
                        if (payments) {
                            const totalPayments = _.sumBy(payments, (row) => {
                                return row.amountPay;
                            });
                            const totalBalance = trainingPrice - totalPayments;
                            if (totalPayments > 0) {
                                if (totalBalance > 0) {
                                    // PARTIAL
                                    item.isPaid = false;
                                    item.isPartialPaid = true;
                                } else if (totalBalance < 1) {
                                    // PAID
                                    item.isPaid = true;
                                    item.isPartialPaid = false;
                                }
                            } else {
                                item.isPaid = false;
                                item.isPartialPaid = false;
                            }
                            item.totalPayments = totalPayments;
                            item.totalBalance = totalBalance;

                        } else {
                            item.totalBalance = trainingPrice;
                            item.totalPayments = 0;
                            if (item.totalBalance > 0 && item.totalPayments == 0) {
                                item.isPaid = false;
                            }
                        }
                        return callback();
                    });
                },
                callback => {
                    knex.select("t.*").from('training AS t').innerJoin("trainee_trainings AS tt", "t.trainingId", "tt.trainingId").where("t.trainingId", trainingId).first()
                        .asCallback((err, training) => {
                            if (err) {
                                return next(err);
                            }

                            if (training) {
                                async.parallel([
                                    (callback1) => {
                                        knex('course').where("courseId", training.courseId).first().asCallback((err, course) => {
                                            if (err) {
                                                return next(err);
                                            }
                                            training.course = course;
                                            return callback1();
                                        });
                                    },
                                    (callback1) => {
                                        knex.select('s.*', 'st.trainingId').from("speakers AS s").innerJoin("speakers_training AS st", "st.speakerId", "s.speakerId").where("st.trainingId", trainingId).asCallback((err, training_speakers) => {
                                            if (err) {
                                                return next(err);
                                            }

                                            if (training_speakers) {
                                                training.training_speakers = training_speakers;
                                            } else {
                                                training.training_speakers = [];
                                            }
                                            return callback1();
                                        });
                                    }
                                ], () => {
                                    item.training = training;
                                    return callback();
                                });
                            } else {
                                item.training = {};
                                return callback();
                            }
                        }).catch(err => {
                            return next(err);
                        });
                }
            ], () => {
                item.isPass = (item.isPass == 0) ? false : true;
                item.isAttended = (item.isAttended == 0) ? false : true;
                return callbackEach(null, item);
            });
        }, () => {
            const resultPaid = _.filter(result, (row) => {
                return row.isPaid && row.isAttended;
            });
            return next(null, resultPaid);
        });
    });
};