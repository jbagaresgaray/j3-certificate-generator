'use strict';


const Database = require('../../app/utils/database').Database;
const db = new Database();
const func = require('../utils/functions');
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkUserUsername = (data, next) => {
    console.log("checkUserUsername: ", data);
    knex('users').where(knex.raw("username LIKE ?", ['%' + data.username + '%'])).asCallback(next);
};

exports.checkUserUsernameUpdate = (userId, data, next) => {
    console.log("checkUserUsername: ", data);
    knex('users').where(knex.raw("username LIKE ?", ['%' + data.username + '%'])).andWhere((sql) => {
        sql.where("userId", "<>", userId).orWhere("userId", "<>", userId);
    }).asCallback(next);
};

exports.checkUserUserEmail = (data, next) => {
    knex('users').where("email", data.email).asCallback(next);
};

exports.checkUserUserEmailUpdate = (userId, data, next) => {
    knex('users').where("email", data.email).andWhere((sql) => {
        sql.where("userId", "<>", userId).orWhere("userId", "<>", userId);
    }).asCallback(next);
};

exports.checkUserStatus = (code, uuid, next) => {
    knex.select('dateVerify', "isVerify", "firstname", "lastname", "email", "phone", "uuid").from("users").where({
        verificationCode: code,
        uuid: uuid
    }).first().asCallback(next);
};

exports.getUserByUsernameAndEmail = (email, next) => {
    const sSQL = knex.select(
            "username",
            "email",
            "firstname",
            "lastname",
            "verificationCode",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid"
        )
        .from("users").where("email", email).first();

    sSQL.asCallback(next);
};

exports.getUserByUUID = (uuid, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "verificationCode",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid",
            "userId"
        )
        .from("users")
        .where(sql => {
            sql.where("userId", uuid).orWhere("uuid", uuid)
        })
        .first()
        .asCallback(next);
};

exports.getUserByUUIDChangeCode = (uuid, code, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "verificationCode",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid",
            "userId"
        )
        .from("users")
        .where(sql => {
            sql.where("userId", uuid).orWhere("uuid", uuid)
        }).andWhere("verificationCode", code)
        .first()
        .asCallback(next);
};


exports.inviteUser = (data, next) => {
    const verificationCode = func.generateString(42);

    async.waterfall([
        callback => {
            knex.transaction(trx => {
                return trx.insert({
                    email: data.email,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    phone: data.phone,
                    verificationCode: verificationCode,
                    isVerify: 0,
                    date_created: knex.raw('NOW()'),
                    uuid: knex.raw('UUID()')
                }, 'userId').into('users').asCallback((err, response) => {
                    if (err) {
                        return next(err);
                    }

                    const userId = response[0];
                    return callback(null, userId);
                });
            });
        },
        (userId, callback) => {
            knex.select("verificationCode", "uuid").from("users").where("userId", userId).first().asCallback((err, result) => {
                if (err) {
                    return next(err);
                }
                return callback(null, result);
            });
        }
    ], next);
};

exports.createUserProfile = (userId, data, next) => {
    const verificationCode = func.generateString(42);
    async.waterfall([
        callback => {
            knex.select("userId").from("users").where(sql => {
                sql.where("userId", userId).orWhere("uuid", userId)
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                }
                return callback(null, response);
            });
        },
        (user, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        email: data.email,
                        firstname: data.firstname,
                        lastname: data.lastname,
                        gender: data.gender,
                        birthday: _.isEmpty(data.birthday) ? null : data.birthday,
                        phone: data.phone,
                        address: data.address,
                        city: data.city,
                        state: data.state,
                        zipcode: data.zipcode,
                        country: data.country,
                        verificationCode: verificationCode,
                        isVerify: 0,
                        date_created: knex.raw('NOW()'),
                        uuid: knex.raw('UUID()')
                    }, 'userId')
                    .from('users').where(sql => {
                        sql.where("userId", user.userId)
                    })
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.createUserAccount = (userId, data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    async.waterfall([
        callback => {
            knex.select("userId").from("users").where(sql => {
                sql.where("userId", userId).orWhere("uuid", userId)
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                }
                return callback(null, response);
            });
        },
        (user, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        gender: data.gender,
                        birthday: _.isEmpty(data.birthday) ? null : data.birthday,
                        phone: data.phone,
                        address: data.address,
                        city: data.city,
                        state: data.state,
                        zipcode: data.zipcode,
                        country: data.country,
                        username: data.username,
                        password: data.password,
                        isVerify: 1,
                        dateVerify: knex.raw('NOW()')
                    })
                    .from('users')
                    .where(sql => {
                        sql.where("userId", user.userId)
                    })
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.getAllUsers = (params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    const numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    const skip = page * numPerPage;
    const limit = skip + "," + numPerPage;

    if (page == 0) {
        page = 1;
    } else {
        page += 1;
    }

    async.waterfall(
        [
            callback => {
                knex("users")
                    .count("* as numRows")
                    .asCallback((err, results) => {
                        if (err) {
                            return next(err, null);
                        }

                        numRows =
                            results && results[0] && results[0].numRows ?
                            results[0].numRows :
                            0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("users");
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }

                strSQL.orderBy("date_created", "desc")
                    .asCallback((err, results) => {
                        if (err) {
                            console.log("err: ", err);
                            return next(err, null);
                        }

                        return callback(null, numRows, numPages, results);
                    });
            },
            (numRows, numPages, dataArr, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            data: dataArr,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        data: dataArr,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.getUser = (user_id, next) => {
    knex('users').where((sql) => {
        // sql.where("userId", user_id).orWhere("uuid", user_id)
        sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
    }).first().asCallback(next);
};

exports.deleteUser = (user_id, next) => {
    knex.transaction(trx => {
        return trx.where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .from("users")
            .delete()
            .asCallback(next);
    });
};

exports.updateUserProfile = (user_id, data, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                email: data.email,
                firstname: data.firstname,
                lastname: data.lastname,
                gender: data.gender,
                birthday: data.birthday,
                phone: data.phone,
                address: data.address,
                city: data.city,
                state: data.state,
                zipcode: data.zipcode,
                country: data.country
            })
            .from("users")
            .where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .asCallback(next);
    });
};

exports.updateUserAccount = (user_id, data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    knex.transaction(trx => {
        return trx
            .update({
                username: data.username,
                password: data.password
            })
            .from("users")
            .where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .asCallback(next);
    });
};

exports.updateUserName = (user_id, data, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                username: data.username,
            })
            .from("users")
            .where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .asCallback(next);
    });
};

exports.updateUserPassword = (user_id, data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    knex.transaction(trx => {
        return trx
            .update({
                password: data.password,
                verificationCode: null
            })
            .from("users")
            .where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .asCallback(next);
    });
};

exports.resetPasswordRequest = (user_id, verificationCode, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                verificationCode: verificationCode
            })
            .from("users")
            .where((sql) => {
                // sql.where("userId", user_id).orWhere("uuid", user_id)
                sql.where(knex.raw("CAST(`userId` AS CHAR)"), user_id).orWhere("uuid", user_id)
            })
            .asCallback(next);
    });
};