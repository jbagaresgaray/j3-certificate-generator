'use strict';

const fs = require('fs-extra');

module.exports = function(main, middleware) {
    main.route('/docx')
        .get(function onRequest(req, res) {
            const file = 'docs/index.html';
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                res.send(data);
            });
        });

    main.route('/docx-v1')
        .get(function onRequest(req, res) {
            const file = 'docs/swagger.json';
            console.log('file: ', file);
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                data = JSON.parse(data);
                res.send(data);
            });
        });

};
