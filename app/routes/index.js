/*jshint camelcase: false */

"use strict";
const fs = require("fs-extra");
const middleware = require("../utils/middleware");

// ======================== VALIDATION ============================ //
const validatorUser = require("../validation/users");

// ======================== ROUTING ============================ //
const users = require("./routing/users");
const courses = require("./routing/courses");
const trainings = require("./routing/trainings");
const training_transactions = require("./routing/training_transactions");
const training_reimbursements = require("./routing/training_reimbursement");
const company = require("./routing/company");
const trainee = require("./routing/trainee");
const trainee_training = require("./routing/trainee_training");
const trainee_training_ledger = require("./routing/trainee_training_ledger");
const speakers = require("./routing/training_speaker");
const miscellaenous = require("./routing/misc");
const auth = require("./routing/auth");
const account_groups = require("./routing/account_groups");
const account_classification = require("./routing/account_classification");

module.exports = (main, api, config, passport) => {
  console.log("ROUTES");

  main.route("*").get(function onRequest(req, res) {
    const file = "site/index.html";
    console.log("file: ", file);
    fs.readFile(file, "utf8", function(err, data) {
      if (err) {
        console.log("Error: " + err);
        return;
      }
      res.send(data);
    });
  });


  api
    .route("/auth")
    .get(middleware.authorizeTokenAuth, auth.getCurrentUser)
    .post(
      validatorUser.validateBasicAuth,
      passport.authenticate("user"),
      auth.login
    );
  api.route("/auth/signout").get(middleware.authorizeTokenAuth, auth.logout);
  api
    .route("/auth/validate")
    .post(middleware.authorizeTokenAuth, auth.validateToken);
  api
    .route("/verify/:code/users/:uuid")
    .get(users.checkUserStatus)
    .post(users.createUserAccount);

  // ======================== FORGOT PASSWORD ============================ //
  // ======================== FORGOT PASSWORD ============================ //
  // ======================== FORGOT PASSWORD ============================ //
  api.route("/forgot").post(users.forgotPassword);
  api.route("/reset").post(users.resetPassword);
  api.route("/reset/:code/validate/:uuid").get(users.checkResetPassword);

  api
    .route("/dashboard/report")
    .get(middleware.authorizeTokenAuth, miscellaenous.getDashboardReport);

  api
    .route("/users")
    .get(middleware.authorizeTokenAuth, users.getAllUsers)
    .post(middleware.authorizeTokenAuth, users.inviteUser);
  api
    .route("/users/:user_id")
    .get(middleware.authorizeTokenAuth, users.getUser)
    .delete(middleware.authorizeTokenAuth, users.deleteUser)
    .put(middleware.authorizeTokenAuth, users.updateUser);
  api
    .route("/users/:user_id/profile")
    .put(middleware.authorizeTokenAuth, users.updateUser);
  api
    .route("/users/:user_id/account")
    .put(middleware.authorizeTokenAuth, users.updateUserAccount);
  api
    .route("/users/:user_id/username")
    .put(middleware.authorizeTokenAuth, users.updateUserName);
  api
    .route("/users/:user_id/password")
    .put(middleware.authorizeTokenAuth, users.updateUserPassword);
  api
    .route("/users/:user_id/resend_verification")
    .post(middleware.authorizeTokenAuth, users.resendEmailVerification);

  api
    .route("/course")
    .get(middleware.authorizeTokenAuth, courses.getAllCourses)
    .post(middleware.authorizeTokenAuth, courses.createCourse);
  api
    .route("/course/:courseId")
    .get(middleware.authorizeTokenAuth, courses.getCourseDetails)
    .delete(middleware.authorizeTokenAuth, courses.deleteCourse)
    .put(middleware.authorizeTokenAuth, courses.updateCourse);

  api
    .route("/trainings")
    .get(middleware.authorizeTokenAuth, trainings.getAllTrainings)
    .post(middleware.authorizeTokenAuth, trainings.createTrainings);
  api
    .route("/trainings/:trainingId")
    .get(middleware.authorizeTokenAuth, trainings.getTrainingDetails)
    .delete(middleware.authorizeTokenAuth, trainings.deleteTraining)
    .put(middleware.authorizeTokenAuth, trainings.updateTrainings);
  api
    .route("/trainings/:trainingId/status")
    .put(middleware.authorizeTokenAuth, trainings.updateTrainingStatus);
  api
    .route("/trainings/:trainingId/trainees")
    .get(middleware.authorizeTokenAuth, trainings.getTrainingTrainees);
  api
    .route("/trainings/:trainingId/speakers")
    .post(middleware.authorizeTokenAuth, trainings.createTrainingSpeakers);
  api
    .route("/trainings/:trainingId/speakers/:speakerId")
    .delete(middleware.authorizeTokenAuth, trainings.deleteTrainingSpeakers);
  api
    .route("/trainings/:trainingId/certificates")
    .get(
      middleware.authorizeTokenAuth,
      trainings.getTrainingTraineesCertificates
    );

  api
    .route("/trainings/:trainingId/transactions")
    .post(
      middleware.authorizeTokenAuth,
      training_transactions.insertIncomeExpense
    )
    .get(
      middleware.authorizeTokenAuth,
      training_transactions.getAllTrainingTransactions
    );

  api
    .route("/trainings/:trainingId/transactions/:transId")
    .get(
      middleware.authorizeTokenAuth,
      training_transactions.getTrainingTransaction
    )
    .delete(
      middleware.authorizeTokenAuth,
      training_transactions.deleteIncomeExpense
    )
    .put(
      middleware.authorizeTokenAuth,
      training_transactions.updateIncomeExpense
    );

  api
    .route("/trainings/:trainingId/transactions/:transId/post")
    .post(
      middleware.authorizeTokenAuth,
      training_transactions.postTransactionIncomeExpense
    );

  api
    .route("/trainings/:trainingId/reimbursements")
    .post(
      middleware.authorizeTokenAuth,
      training_reimbursements.insertReimbursement
    )
    .get(
      middleware.authorizeTokenAuth,
      training_reimbursements.getAllTrainingReimbursements
    );

  api
    .route("/trainings/:trainingId/reimbursements/:transId")
    .get(
      middleware.authorizeTokenAuth,
      training_reimbursements.getTrainingReimbursementDetail
    )
    .delete(
      middleware.authorizeTokenAuth,
      training_reimbursements.deleteReimbursement
    )
    .put(
      middleware.authorizeTokenAuth,
      training_reimbursements.updateReimbursement
    );

  api
    .route("/trainings/:trainingId/reimbursements/:transId/post")
    .post(
      middleware.authorizeTokenAuth,
      training_reimbursements.postTransactionReimbursement
    );
  api
    .route("/trainings/:trainingId/reimbursements/:transId/upload")
    .post(
      middleware.authorizeTokenAuth,
      training_reimbursements.uploadTraineeTrainingReimbursementProof
    );
  api
    .route("/trainee/:traineeId/reimbursements/:transId/upload/:imageId")
    .delete(
      middleware.authorizeTokenAuth,
      training_reimbursements.deleteTraineeTrainingReimbursementProof
    );

  api
    .route("/speakers")
    .get(middleware.authorizeTokenAuth, speakers.getAllSpeakersByTraining)
    .post(middleware.authorizeTokenAuth, speakers.createTrainingSpeaker);
  api
    .route("/speakers/:speakerId")
    .get(middleware.authorizeTokenAuth, speakers.getSpeakerDetails)
    .delete(middleware.authorizeTokenAuth, speakers.deleteTrainingSpeaker)
    .put(middleware.authorizeTokenAuth, speakers.updateTrainingSpeaker);

  api
    .route("/company")
    .get(middleware.authorizeTokenAuth, company.getAllCompanies)
    .post(middleware.authorizeTokenAuth, company.createCompany);
  api
    .route("/company/:companyId")
    .get(middleware.authorizeTokenAuth, company.getCompanyDetails)
    .delete(middleware.authorizeTokenAuth, company.deleteCompany)
    .put(middleware.authorizeTokenAuth, company.updateCompany);

  api
    .route("/trainee")
    .get(middleware.authorizeTokenAuth, trainee.getAlltrainees)
    .post(middleware.authorizeTokenAuth, trainee.createTrainee);
  api
    .route("/trainee/:traineeId")
    .get(middleware.authorizeTokenAuth, trainee.getTraineeDetails)
    .delete(middleware.authorizeTokenAuth, trainee.deleteTrainee)
    .put(middleware.authorizeTokenAuth, trainee.updateTrainee);
  api
    .route("/trainee/:traineeId/image")
    .post(middleware.authorizeTokenAuth, trainee.uploadTraineeImage)
    .post(middleware.authorizeTokenAuth, trainee.deleteTraineeImage);

  api
    .route("/trainee/:traineeId/training")
    .post(middleware.authorizeTokenAuth, trainee_training.createTraineeTraining)
    .get(middleware.authorizeTokenAuth, trainee_training.getTraineeTrainings);
  api
    .route("/trainee/:traineeId/certificates")
    .get(
      middleware.authorizeTokenAuth,
      trainee_training.getTraineeTrainingCertificates
    );
  api
    .route("/trainee/:traineeId/training/:trainingId")
    .get(
      middleware.authorizeTokenAuth,
      trainee_training.getTraineeTrainingCertificate
    )
    .delete(
      middleware.authorizeTokenAuth,
      trainee_training.deleteTraineeTraining
    );

  api
    .route("/trainee/:traineeId/attended/:trainingId")
    .post(middleware.authorizeTokenAuth, trainee_training.markTraineeAsAttended);
  api
    .route("/trainee/:traineeId/pass/:trainingId")
    .post(middleware.authorizeTokenAuth, trainee_training.markTraineeAsPass);
  api
    .route("/trainee/:traineeId/certificate/:trainingId")
    .get(
      middleware.authorizeTokenAuth,
      trainee_training.getTraineeTrainingCertificate
    );

  api
    .route("/trainee/:traineeId/pay/:trainingId")
    .get(
      middleware.authorizeTokenAuth,
      trainee_training_ledger.getTraineeTrainingPayments
    )
    .post(
      middleware.authorizeTokenAuth,
      trainee_training_ledger.payTraineeTraining
    );
  api
    .route("/trainee/:traineeId/pay/:trainingId/upload")
    .post(
      middleware.authorizeTokenAuth,
      trainee_training_ledger.uploadTraineeTrainingPaymentProof
    );
  api
    .route("/trainee/:traineeId/pay/:trainingId/upload/:imageId")
    .delete(
      middleware.authorizeTokenAuth,
      trainee_training_ledger.deleteTraineeTrainingPaymentProof
    );

  api
    .route("/account_groups")
    .get(middleware.authorizeTokenAuth, account_groups.getAllAccountGroups);
  api
    .route("/account_classification")
    .get(
      middleware.authorizeTokenAuth,
      account_classification.getAllAccountClassification
    );

  api.route("/upload-directories").get(miscellaenous.getAllSpacesDirectories);
};