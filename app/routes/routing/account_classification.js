'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/account_classification').AccountClassification;
const ctrl = new dataCtrl();

exports.getAllAccountClassification = (req, res) => {
    if (req.user) {
        ctrl.getAllAccountClassification(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};