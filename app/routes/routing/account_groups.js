'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/account_groups').AccountGroups;
const ctrl = new dataCtrl();

exports.getAllAccountGroups = (req, res) => {
    if (req.user) {
        ctrl.getAllAccountGroups(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};