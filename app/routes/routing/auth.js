'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/users').Users;
const ctrl = new dataCtrl();

exports.login = (req, res) => {
    if (req.isAuthenticated()) {
        if (req.user && !req.user.success) {
            res.status(200).json(req.user);
            return;
        }
        res.status(200).json(req.user);
    } else {
        console.log('login at else');
        res.sendStatus(401);
    }
};


exports.getCurrentUser = (req, res) => {
    if (req.isAuthenticated()) {
        ctrl.getUser(req.user.result.user.uuid,cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        return res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.logout = (req, res) => {

};

exports.validateToken = (req, res) => {

};