'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/company').Company;
const ctrl = new dataCtrl();

exports.createCompany = (req, res) => {
    req.checkBody('companyName', 'Please provide Company Name').notEmpty();
    /* req.checkBody('contactPerson', 'Please provide Contact person').notEmpty();
    req.checkBody('contactNum', 'Please provide Contact Number').notEmpty();
    req.checkBody('contactEmail', 'Please provide Email Address').notEmpty();
    req.checkBody('contactEmail', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.checkBody('companyTIN', 'Please provide Company TIN').notEmpty(); */
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.createCompany(req.user.result.user, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getAllCompanies = (req, res) => {
    if (req.user) {
        ctrl.getAllCompanies(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getCompanyDetails = (req, res) => {
    req.checkParams('companyId', 'Please provide Company ID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getCompanyDetails(req.params.companyId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.deleteCompany = (req, res) => {
    req.checkParams('companyId', 'Please provide Company ID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.deleteCompany(req.params.companyId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.updateCompany = (req, res) => {
    req.checkParams('companyId', 'Please provide Company ID or UUID parameter').notEmpty();

    req.checkBody('companyName', 'Please provide Company Name').notEmpty();
    /* req.checkBody('contactPerson', 'Please provide Contact person').notEmpty();
    req.checkBody('contactNum', 'Please provide Contact Number').notEmpty();
    req.checkBody('contactEmail', 'Please provide Email Address').notEmpty();
    req.checkBody('contactEmail', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.checkBody('companyTIN', 'Please provide Company TIN').notEmpty(); */
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.updateCompany(req.params.companyId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};