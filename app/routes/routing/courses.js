'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/courses').Courses;
const ctrl = new dataCtrl();

exports.createCourse = (req, res) => {
    req.checkBody('courseName', 'Please provide Course Name').notEmpty();
    req.checkBody('courseCode', 'Please provide Course Code').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.createCourse(req.user.result.user,req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getAllCourses = (req, res) => {
    if (req.user) {
        ctrl.getAllCourses(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getCourseDetails = (req, res) => {
    req.checkParams('courseId', 'Please provide CourseID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getCourseDetails(req.params.courseId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.deleteCourse = (req, res) => {
    req.checkParams('courseId', 'Please provide CourseID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.deleteCourse(req.params.courseId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.updateCourse = (req, res) => {
    req.checkParams('courseId', 'Please provide CourseID or UUID parameter').notEmpty();
    req.checkBody('courseName', 'Please provide Course Name').notEmpty();
    req.checkBody('courseCode', 'Please provide Course Code').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.updateCourse(req.params.courseId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};