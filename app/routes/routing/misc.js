"use strict";

const cb = require("./../../utils/callback");
const dataCtrl = require("../../controllers/misc").Misc;
const ctrl = new dataCtrl();

const config = require("../../../config/config");

const fs = require("fs");
const async = require("async");
const AWS = require('aws-sdk');
AWS.config.update({
	accessKeyId: config.SPACES_ACCESS_KEY_ID,
	secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
});
const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
const s3 = new AWS.S3({
	endpoint: spacesEndpoint
});

exports.getDashboardReport = (req, res) => {
	if (req.user) {
		ctrl.getDashboardReport(cb.setupResponseCallback(res));
	} else {
		res.status(401).json({
			msg: "User not found or Token had expired",
			result: null,
			success: false
		});
	}
};


exports.getAllSpacesDirectories = (req, res) => {
	const s3params = {
		Bucket: config.SPACES_BUCKET,
		MaxKeys: 20,
		Delimiter: '/',
	};

	s3.listObjectsV2(s3params, (err, data) => {
		if (err) {
			console.log("S3 Error: ", err);
		}
		console.log("S3 Lists: ", data);
	});
};