"use strict";

const cb = require("./../../utils/callback");
const dataCtrl = require("../../controllers/trainee").Trainee;
const ctrl = new dataCtrl();

const config = require("../../../config/config");

const _ = require("lodash");
const mkdirp = require("mkdirp");
const fs = require("fs");
const path = require("path");

const multer = require("multer");
const sharp = require("sharp");
// const multerS3 = require('multer-s3');
// const AWS = require('aws-sdk');
// AWS.config.update({
//   accessKeyId: config.SPACES_ACCESS_KEY_ID,
//   secretAccessKey: config.SPACES_SECRET_ACCESS_KEY
// });
// const spacesEndpoint = new AWS.Endpoint(config.SPACES_ORIGIN);
// const s3 = new AWS.S3({
//   endpoint: spacesEndpoint
// });


exports.createTrainee = (req, res) => {
  req.checkBody("lastName", "Please provide Lastname").notEmpty();
  req.checkBody("firstName", "Please provide Firstname").notEmpty();
  // req.checkBody("middleName", "Please provide Middlename").notEmpty();
  req.checkBody("traineeSex", "Please provide Gender/Sex").notEmpty();
  // req.checkBody('traineeContactNo', 'Please provide Contact number').notEmpty();
  // req.checkBody('traineeEmail', 'Please provide Email Address').notEmpty();
  // req.checkBody('traineeEmail', 'Email address needs to be in the format yourname@domain.com.').isEmail();
  const errors = req.validationErrors();
  if (errors) {
    res.status(200).send({
      result: errors,
      msg: "",
      success: false
    });
  } else {
    if (req.user) {
      ctrl.createTrainee(
        req.user.result.user,
        req.body,
        cb.setupResponseCallback(res)
      );
    } else {
      res.status(401).json({
        msg: "User not found or Token had expired",
        result: null,
        success: false
      });
    }
  }
};

exports.getAlltrainees = (req, res) => {
  if (req.user) {
    ctrl.getAlltrainees(req.query, cb.setupResponseCallback(res));
  } else {
    res.status(401).json({
      msg: "User not found or Token had expired",
      result: null,
      success: false
    });
  }
};

exports.getTraineeDetails = (req, res) => {
  req.checkParams("traineeId", "Please provide Trainee ID or UUID parameter").notEmpty();

  const errors = req.validationErrors();
  if (errors) {
    res.status(200).send({
      result: errors,
      msg: "",
      success: false
    });
  } else {
    if (req.user) {
      ctrl.getTraineeDetails(
        req.params.traineeId,
        cb.setupResponseCallback(res)
      );
    } else {
      res.status(401).json({
        msg: "User not found or Token had expired",
        result: null,
        success: false
      });
    }
  }
};

exports.deleteTrainee = (req, res) => {
  req
    .checkParams("traineeId", "Please provide Trainee ID or UUID parameter")
    .notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    res.status(200).send({
      result: errors,
      msg: "",
      success: false
    });
  } else {
    if (req.user) {
      ctrl.deleteTrainee(req.params.traineeId, cb.setupResponseCallback(res));
    } else {
      res.status(401).json({
        msg: "User not found or Token had expired",
        result: null,
        success: false
      });
    }
  }
};

exports.updateTrainee = (req, res) => {
  req
    .checkParams("traineeId", "Please provide Trainee ID or UUID parameter")
    .notEmpty();

  req.checkBody("lastName", "Please provide Lastname").notEmpty();
  req.checkBody("firstName", "Please provide Firstname").notEmpty();
  // req.checkBody("middleName", "Please provide Middlename").notEmpty();
  req.checkBody("traineeSex", "Please provide Gender/Sex").notEmpty();
  // req.checkBody('traineeContactNo', 'Please provide Contact number').notEmpty();
  // req.checkBody('traineeEmail', 'Please provide Email Address').notEmpty();
  // req.checkBody('traineeEmail', 'Email address needs to be in the format yourname@domain.com.').isEmail();
  const errors = req.validationErrors();
  if (errors) {
    res.status(200).send({
      result: errors,
      msg: "",
      success: false
    });
  } else {
    if (req.user) {
      ctrl.updateTrainee(
        req.params.traineeId,
        req.body,
        cb.setupResponseCallback(res)
      );
    } else {
      res.status(401).json({
        msg: "User not found or Token had expired",
        result: null,
        success: false
      });
    }
  }
};

exports.uploadTraineeImage = (req, res) => {
  
  const limits = {
    fileSize: 1024 * 1024 * 10 // 10MB
  };

  const fileFilter = (req, file, cb) => {
    if (
      file.mimetype !== "image/png" &&
      file.mimetype !== "image/jpeg" &&
      file.mimetype !== "image/jpg" &&
      file.mimetype !== "image/gif" &&
      file.mimetype !== "image/x-ms-bmp"
    ) {
      req.fileValidationError =
        "Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp";
      return cb(
        null,
        false,
        new Error("Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp")
      );
    }
    cb(null, true);
  };

  // const storage = multerS3({
  //   s3: s3,
  //   bucket: config.SPACES_BUCKET,
  //   acl: "public-read",
  //   limits: limits,
  //   contentType: multerS3.AUTO_CONTENT_TYPE,
  //   key: function(req, file, cb) {
  //     const fileName = "400_" + req.params.traineeId + "_" + file.originalname;
  //     const fileKey = config.PCLOUD_APP_FOLDER + "/" + config.PCLOUD_APP_TRAINEE_FOLDER + "/" + fileName;
  //     cb(null, fileKey);
  //   },
  //   fileFilter: fileFilter,
  //   shouldTransform: function(req, file, cb) {
  //     cb(null, /^image/i.test(file.mimetype));
  //   },
  //   transforms: [{
  //     id: 'original',
  //     transform: function(req, file, cb) {
  //       //Perform desired transformations
  //       cb(
  //         null,
  //         sharp()
  //         .resize(400, 400, {
  //           kernel: sharp.kernel.lanczos2,
  //           fit: sharp.fit.contain,
  //           withoutEnlargement: true
  //         })
  //         .max()
  //       );
  //     }
  //   }]
  // });

  const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      let dir = 'public/uploads/trainees/';
      mkdirp(dir, (err) => {
        cb(err, dir);
      });
    },
    filename: (req, file, cb) => {
      cb(null, req.params.traineeId + "_" + file.originalname);
    },
  });

  const uploadImg = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: limits
  }).single("images");

  if (req.user) {
    uploadImg(req, res, err => {
      if (err) {
        if (err.code == "LIMIT_FILE_SIZE") {
          return res.status(200).json({
            result: "LIMIT_FILE_SIZE",
            success: false,
            msg: "File Size Limit Exceeded"
          });
        } else {
          return res.status(500).json({
            result: err,
            success: false,
            msg: err.inner
          });
        }
      }

      if (req.fileValidationError) {
        return res.status(200).json({
          result: null,
          msg: req.fileValidationError,
          success: false
        });
      }

      req.body.toDeleteFile = false;
      if (!_.isUndefined(req.file)) {

        req.body.toDeleteFile = true;
        // req.body.img_path = req.file.path;
        req.body.img_name = req.file.originalname;
        req.body.img_type = req.file.mimetype;
        req.body.img_size = req.file.size;
        req.body.traineeId = req.params.traineeId;
        req.body.img_path = path.resolve(
          req.file.destination,
          req.file.filename
        );

        if (req.user) {
          req
            .checkParams(
              "traineeId",
              "Please provide Trainee ID or UUID parameter"
            )
            .notEmpty();
          const errors = req.validationErrors();
          if (errors) {
            res.status(200).send({
              result: errors,
              msg: "",
              success: false
            });
          } else {
            ctrl.uploadTraineeImage(
              req.params.traineeId,
              req.body,
              cb.setupResponseCallback(res)
            );
          }
        }
      } else {
        return res.status(400).json({
          result: null,
          msg: "Not a valid file!",
          success: false
        });
      }
    });
  } else {
    res.status(401).json({
      msg: "User not found or Token had expired",
      result: null,
      success: false
    });
  }
};

exports.deleteTraineeImage = (req, res) => {
  req
    .checkParams("traineeId", "Please provide Trainee ID or UUID parameter")
    .notEmpty();
  req
    .checkBody("fileid", "Please provide fileid parameter to delete")
    .notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    res.status(200).send({
      result: errors,
      msg: "",
      success: false
    });
  } else {
    if (req.user) {
      ctrl.deleteTraineeImage(
        req.params.traineeId,
        req.body,
        cb.setupResponseCallback(res)
      );
    } else {
      res.status(401).json({
        msg: "User not found or Token had expired",
        result: null,
        success: false
      });
    }
  }
};