'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/trainee_training').TraineeTraining;
const ctrl = new dataCtrl();
const multer = require('multer');
const mkdirp = require('mkdirp');
const _ = require("lodash");
const path = require("path");

exports.markTraineeAsPass = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.markTraineeAsPass(req.params.traineeId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.markTraineeAsAttended = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.markTraineeAsAttended(req.params.traineeId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.createTraineeTraining = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkBody('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();

	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.createTraineeTraining(req.params.traineeId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.getTraineeTrainings = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTraineeTrainings(req.params.traineeId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.deleteTraineeTraining = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.deleteTraineeTraining(req.params.traineeId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.getTraineeTrainingCertificate = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();

	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTraineeTrainingCertificate(req.params.traineeId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.getTraineeTrainingCertificates = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTraineeTrainingCertificates(req.params.traineeId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};
