'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/trainee_training_ledger').TraineeTrainingLedger;
const ctrl = new dataCtrl();

const multer = require('multer');
const mkdirp = require('mkdirp');
const _ = require("lodash");
const path = require("path");
const fs = require("fs");


exports.payTraineeTraining = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	req.checkBody('traineeNum', 'Please provide Training Trainee ID Number').notEmpty();
	req.checkBody('paymentType', 'Please provide Method of Payment').notEmpty();
	req.checkBody('amountPay', 'Please provide Amount to pay').notEmpty();
	req.checkBody('paymentDate', 'Please provide Payment Date').notEmpty();
	req.checkBody('paidBy', 'Please provide Payer detail').notEmpty();
	req.checkBody('receivedBy', 'Please provide Payee detail').notEmpty();
	req.checkBody('receivingDate', 'Please provide payment receiving date').notEmpty();


	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.payTraineeTraining(req.user.result.user, req.params.traineeId, req.params.trainingId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.deleteTraineeTrainingPaymentProof = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	req.checkParams('imageId', 'Please provide Image ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			console.log("deleteTraineeTrainingPaymentProof")
			ctrl.deleteTraineeTrainingPaymentProof(req.params.traineeId, req.params.trainingId, req.params.imageId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.uploadTraineeTrainingPaymentProof = (req, res) => {
	const storage = multer.diskStorage({
		destination: (req, file, cb) => {
			let dir = 'public/uploads/payment/';
			mkdirp(dir, (err) => {
				cb(err, dir);
			});
		},
		filename: (req, file, cb) => {
			cb(null, req.params.traineeId + '_' + req.params.trainingId + '_' + file.originalname);
		}
	});

	const limits = {
		fileSize: 1024 * 1024 * 10 // 10MB
	};

	const fileFilter = (req, file, cb) => {
		if ((file.mimetype !== 'image/png') &&
			(file.mimetype !== 'image/jpeg') &&
			(file.mimetype !== 'image/jpg') &&
			(file.mimetype !== 'image/gif') &&
			(file.mimetype !== 'image/x-ms-bmp')) {
			req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
			return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
		}
		cb(null, true);
	};

	const uploadImg = multer({
		storage: storage,
		fileFilter: fileFilter,
		limits: limits
	}).single('images');

	if (req.user) {
		uploadImg(req, res, (err) => {
			if (err) {
				if (err.code == 'LIMIT_FILE_SIZE') {
					return res.status(200).json({
						result: 'LIMIT_FILE_SIZE',
						success: false,
						msg: 'File Size Limit Exceeded'
					});
				} else {
					return res.status(500).json({
						result: err,
						success: false,
						msg: err.inner
					});
				}
			}

			if (req.fileValidationError) {
				return res.status(200).json({
					result: null,
					msg: req.fileValidationError,
					success: false
				});
			}

			req.body.toDeleteFile = false;
			if (!_.isUndefined(req.file)) {
				req.body.toDeleteFile = true;
				// req.body.img_path = req.file.path;
				req.body.img_name = req.file.originalname;
				req.body.img_type = req.file.mimetype;
				req.body.img_size = req.file.size;
				req.body.img_path = path.resolve(
					req.file.destination,
					req.file.filename
				);
				req.body.content = fs.readFileSync(req.body.img_path);

				req.checkParams('traineeId', 'Please provide Trainee ID or Trainee UUID parameter').notEmpty();
				req.checkParams('trainingId', 'Please provide Training ID or Training UUID parameter').notEmpty();
				req.checkBody('traineeTrainingId', 'Please provide Trainee Training ID or Trainee Training UUID parameter').notEmpty();
				const errors = req.validationErrors();
				if (errors) {
					res.status(200).send({
						result: errors,
						msg: '',
						success: false
					});
				} else {
					ctrl.uploadTraineeTrainingPaymentProof(req.params.traineeId, req.params.trainingId, req.body, cb.setupResponseCallback(res));
				}
			} else {
				return res.status(400).json({
					result: null,
					msg: 'Not a valid file!',
					success: false
				});
			}
		});
	} else {
		res.status(401).json({
			msg: 'User not found or Token had expired',
			result: null,
			success: false
		});
	}
};

exports.getTraineeTrainingPayments = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTraineeTrainingPayments(req.params.traineeId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};