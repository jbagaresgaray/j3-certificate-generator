'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/training_reimbursement').TrainingReimbursement;
const ctrl = new dataCtrl();

const multer = require('multer');
const mkdirp = require('mkdirp');
const _ = require("lodash");
const path = require("path");
const fs = require("fs");


exports.uploadTraineeTrainingReimbursementProof = (req, res) => {
	const storage = multer.diskStorage({
		destination: (req, file, cb) => {
			let dir = 'public/uploads/reimbursement/';
			mkdirp(dir, (err) => {
				cb(err, dir);
			});
		},
		filename: (req, file, cb) => {
			cb(null, req.params.traineeId + '_' + req.params.trainingId + '_' + file.originalname);
		}
	});

	const limits = {
		fileSize: 1024 * 1024 * 10 // 10MB
	};

	const fileFilter = (req, file, cb) => {
		if ((file.mimetype !== 'image/png') &&
			(file.mimetype !== 'image/jpeg') &&
			(file.mimetype !== 'image/jpg') &&
			(file.mimetype !== 'image/gif') &&
			(file.mimetype !== 'image/x-ms-bmp')) {
			req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
			return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
		}
		cb(null, true);
	};

	const uploadImg = multer({
		storage: storage,
		fileFilter: fileFilter,
		limits: limits
	}).single('images');

	if (req.user) {
		uploadImg(req, res, (err) => {
			if (err) {
				if (err.code == 'LIMIT_FILE_SIZE') {
					return res.status(200).json({
						result: 'LIMIT_FILE_SIZE',
						success: false,
						msg: 'File Size Limit Exceeded'
					});
				} else {
					return res.status(500).json({
						result: err,
						success: false,
						msg: err.inner
					});
				}
			}

			if (req.fileValidationError) {
				return res.status(200).json({
					result: null,
					msg: req.fileValidationError,
					success: false
				});
			}

			req.body.toDeleteFile = false;
			if (!_.isUndefined(req.file)) {
				req.body.toDeleteFile = true;
				// req.body.img_path = req.file.path;
				req.body.img_name = req.file.originalname;
				req.body.img_type = req.file.mimetype;
				req.body.img_size = req.file.size;
				req.body.img_path = path.resolve(
					req.file.destination,
					req.file.filename
				);
				req.body.content = fs.readFileSync(req.body.img_path);

				req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID parameter').notEmpty();
				req.checkParams('trainingId', 'Please provide Training ID or Training UUID parameter').notEmpty();
				req.checkBody('traineeTrainingId', 'Please provide Trainee Training ID or Trainee Training UUID parameter').notEmpty();
				const errors = req.validationErrors();
				if (errors) {
					res.status(200).send({
						result: errors,
						msg: '',
						success: false
					});
				} else {
					ctrl.uploadTraineeTrainingReimbursementProof(req.params.transId, req.params.trainingId, req.body, cb.setupResponseCallback(res));
				}
			} else {
				return res.status(400).json({
					result: null,
					msg: 'Not a valid file!',
					success: false
				});
			}
		});
	} else {
		res.status(401).json({
			msg: 'User not found or Token had expired',
			result: null,
			success: false
		});
	}
};


exports.insertReimbursement = (req, res) => {
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();

	req.checkBody('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkBody('account_groupId', 'Please provide Reimbursement Account Group').notEmpty();
	req.checkBody('paymentType', 'Please provide Reimbursement payment type').notEmpty();
	req.checkBody('TransDate', 'Please provide Reimbursement Date').notEmpty();
	req.checkBody('TransCode', 'Please provide Reimbursement Code').notEmpty();
	req.checkBody('Amount', 'Please provide Reimbursement Amount').notEmpty();
	req.checkBody('Remarks', 'Please provide Reimbursement Remarks or Description').notEmpty();
	req.checkBody('receivedBy', 'Please provide Reimbursement Received by').notEmpty();
	req.checkBody('receivingDate', 'Please provide Reimbursement Received Date').notEmpty();


	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.insertReimbursement(req.user.result.user, req.params.trainingId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.updateReimbursement = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();

	req.checkBody('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkBody('account_groupId', 'Please provide Reimbursement Account Group').notEmpty();
	req.checkBody('paymentType', 'Please provide Reimbursement payment type').notEmpty();
	req.checkBody('TransDate', 'Please provide Reimbursement Date').notEmpty();
	req.checkBody('TransCode', 'Please provide Reimbursement Code').notEmpty();
	req.checkBody('Amount', 'Please provide Reimbursement Amount').notEmpty();
	req.checkBody('Remarks', 'Please provide Reimbursement Remarks or Description').notEmpty();
	req.checkBody('receivedBy', 'Please provide Reimbursement Received by').notEmpty();
	req.checkBody('receivingDate', 'Please provide Reimbursement Received Date').notEmpty();

	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.updateReimbursement(req.params.transId, req.params.trainingId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.getTrainingReimbursementDetail = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTrainingReimbursementDetail(req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.deleteReimbursement = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.deleteReimbursement(req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.postTransactionReimbursement = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.postTransactionReimbursement(req.user.result.user, req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.getAllTrainingReimbursements = (req, res) => {
	if (req.user) {
		ctrl.getAllTrainingTransactions(req.query, req.params.trainingId, cb.setupResponseCallback(res));
	} else {
		res.status(401).json({
			msg: 'User not found or Token had expired',
			result: null,
			success: false
		});
	}
};

exports.deleteTraineeTrainingReimbursementProof = (req, res) => {
	req.checkParams('traineeId', 'Please provide Trainee ID or UUID parameter').notEmpty();
	req.checkParams('transId', 'Please provide Transaction ID or UUID parameter').notEmpty();
	req.checkParams('imageId', 'Please provide Image ID or UUID parameter').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			console.log("deleteTraineeTrainingReimbursementProof")
			ctrl.deleteTraineeTrainingReimbursementProof(req.params.traineeId, req.params.transId, req.params.imageId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};
