'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/training_speakers').Speakers;
const ctrl = new dataCtrl();

exports.createTrainingSpeaker = (req, res) => {
    req.checkBody('trainingSpeaker', 'Please fill in the required field/s').notEmpty();
    req.checkBody('trainingSpeakerAccrNo', 'Please fill in the required field/s').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.createTrainingSpeaker(req.user.result.user, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getAllSpeakersByTraining = (req, res) => {
    if (req.user) {
        ctrl.getAllSpeakersByTraining(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getSpeakerDetails = (req, res) => {
    req.checkParams('speakerId', 'Please provide Speaker ID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getSpeakerDetails(req.params.speakerId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.deleteTrainingSpeaker = (req, res) => {
    req.checkParams('speakerId', 'Please provide Speaker ID or UUID parameter').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.deleteTrainingSpeaker(req.params.speakerId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.updateTrainingSpeaker = (req, res) => {
    req.checkParams('speakerId', 'Please provide speakerId or UUID parameter').notEmpty();
    req.checkBody('trainingSpeaker', 'Please fill in the required field/s').notEmpty();
    req.checkBody('trainingSpeakerAccrNo', 'Please fill in the required field/s').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.updateTrainingSpeaker(req.params.speakerId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};