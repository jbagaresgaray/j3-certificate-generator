'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/training_transactions').TrainingTransactions;
const ctrl = new dataCtrl();


exports.insertIncomeExpense = (req, res) => {
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	req.checkBody('account_groupId', 'Please provide transaction Account Group').notEmpty();
	// req.checkBody('account_classId', 'Please provide transaction Account Classification').notEmpty();
	req.checkBody('TransDate', 'Please provide Transaction Date!').notEmpty();
	req.checkBody('TransCode', 'Please provide Transaction Code').notEmpty();
	req.checkBody('Amount', 'Please provide Transaction Amount').notEmpty();
	req.checkBody('Remarks', 'Please provide Transaction Remarks or Description').notEmpty();


	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.insertIncomeExpense(req.user.result.user, req.params.trainingId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.updateIncomeExpense = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or UUID parameter').notEmpty();
	req.checkBody('account_groupId', 'Please provide transaction Account Group').notEmpty();
	req.checkBody('TransDate', 'Please provide Transaction Date!').notEmpty();
	req.checkBody('TransCode', 'Please provide Transaction Code').notEmpty();
	req.checkBody('Amount', 'Please provide Transaction Amount').notEmpty();
	req.checkBody('Remarks', 'Please provide Transaction Remarks or Description').notEmpty();

	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.updateIncomeExpense(req.params.transId, req.params.trainingId, req.body, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.getTrainingTransaction = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.getTrainingTransaction(req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.deleteIncomeExpense = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.deleteIncomeExpense(req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};

exports.postTransactionIncomeExpense = (req, res) => {
	req.checkParams('transId', 'Please provide Transaction ID or Transaction UUID').notEmpty();
	req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
	const errors = req.validationErrors();
	if (errors) {
		res.status(200).send({
			result: errors,
			msg: '',
			success: false
		});
	} else {
		if (req.user) {
			ctrl.postTransactionIncomeExpense(req.user.result.user, req.params.transId, req.params.trainingId, cb.setupResponseCallback(res));
		} else {
			res.status(401).json({
				msg: 'User not found or Token had expired',
				result: null,
				success: false
			});
		}
	}
};


exports.getAllTrainingTransactions = (req, res) => {
	if (req.user) {
		ctrl.getAllTrainingTransactions(req.query, req.params.trainingId, cb.setupResponseCallback(res));
	} else {
		res.status(401).json({
			msg: 'User not found or Token had expired',
			result: null,
			success: false
		});
	}
};