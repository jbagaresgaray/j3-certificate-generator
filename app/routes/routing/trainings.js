'use strict';

const cb = require('./../../utils/callback');
const dataCtrl = require('../../controllers/trainings').Trainings;
const ctrl = new dataCtrl();

exports.createTrainings = (req, res) => {
    req.checkBody('courseId', 'Please provide Training Course').notEmpty();
    req.checkBody('trainingDesc', 'Please provide Training Description').notEmpty();
    req.checkBody('trainingSchedFrom', 'Please provide Training Schedule From').notEmpty();
    req.checkBody('trainingSchedTo', 'Please provide Training Schedule To').notEmpty();
    req.checkBody('trainingVenue', 'Please provide Training Venue').notEmpty();
    req.checkBody('trainingPrice', 'Please provide Training Price').notEmpty().isDecimal();
    // req.checkBody('trainingSpeakerSalutation', 'Please provide Training Speaker Salutation').notEmpty();
    req.checkBody('trainingHours', 'Please provide Training Hours').notEmpty().isInt();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.createTrainings(req.user.result.user, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getAllTrainings = (req, res) => {
    if (req.user) {
        ctrl.getAllTrainings(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getTrainingDetails = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getTrainingDetails(req.params.trainingId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.deleteTraining = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.deleteTraining(req.params.trainingId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.updateTrainings = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();

    req.checkBody('courseId', 'Please provide Training Course').notEmpty();
    req.checkBody('trainingDesc', 'Please provide Training Description').notEmpty();
    req.checkBody('trainingSchedFrom', 'Please provide Training Schedule From').notEmpty();
    req.checkBody('trainingSchedTo', 'Please provide Training Schedule To').notEmpty();
    req.checkBody('trainingVenue', 'Please provide Training Venue').notEmpty();
    req.checkBody('trainingHours', 'Please provide Training Hours').notEmpty().isInt();
    req.checkBody('trainingPrice', 'Please provide Training Price').notEmpty().isDecimal();

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.updateTrainings(req.params.trainingId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.updateTrainingStatus = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    req.checkBody('status', 'Please provide Training status').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.updateTrainingStatus(req.params.trainingId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getTrainingTrainees = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getTrainingTrainees(req.params.trainingId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.createTrainingSpeakers = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    req.checkBody('speakerId', 'Please provide Speaker ID or UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.createTrainingSpeakers(req.params.trainingId, req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.deleteTrainingSpeakers = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    req.checkParams('speakerId', 'Please provide Speaker ID or Speaker UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.deleteTrainingSpeakers(req.params.trainingId, req.params.speakerId, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.getTrainingTraineesCertificates = (req, res) => {
    req.checkParams('trainingId', 'Please provide Training ID or Training UUID').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            ctrl.getTrainingTraineesCertificates(req.params.trainingId, req.query, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};
