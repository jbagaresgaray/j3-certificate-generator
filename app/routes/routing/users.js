'use strict';

const cb = require('./../../utils/callback');
const usersCtrl = require('../../controllers/users').Users;
const users = new usersCtrl();

exports.inviteUser = (req, res) => {
    req.checkBody('lastname', 'Please fill in the required field/s').notEmpty();
    req.checkBody('firstname', 'Please fill in the required field/s').notEmpty();
    req.checkBody('email', 'Please fill in the required field/s').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();

    const errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        if (req.user) {
            users.inviteUser(req.body, cb.setupResponseCallback(res));
        } else {
            res.status(401).json({
                msg: 'User not found or Token had expired',
                result: null,
                success: false
            });
        }
    }
};

exports.createUserProfile = (req, res) => {
    req.checkBody('lastname', 'Please fill in the required field/s').notEmpty();
    req.checkBody('firstname', 'Please fill in the required field/s').notEmpty();
    req.checkBody('email', 'Please fill in the required field/s').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();

    const errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.createUserProfile(req.params.user_id, req.body, cb.setupResponseCallback(res));
    }
};

exports.createUserAccount = (req, res) => {
    req.checkParams('code', 'Please provide Verification parameters').notEmpty();
    req.checkParams('uuid', 'Please provide UUID parameters').notEmpty();

    req.checkBody('username', 'Please provide Username').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.createUserAccount(req.params.code, req.params.uuid, req.body, cb.setupResponseCallback(res));
    }
};

exports.getAllUsers = (req, res) => {
    users.getAllUsers(req.query, cb.setupResponseCallback(res));
};

exports.getUser = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.getUser(req.params.user_id, cb.setupResponseCallback(res));
    }
};

exports.deleteUser = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.deleteUser(req.params.user_id, cb.setupResponseCallback(res));
    }
};

exports.updateUser = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();

    req.checkBody('lastname', 'Please provide your lastname').notEmpty();
    req.checkBody('firstname', 'Please provide your firstname').notEmpty();
    req.checkBody('email', 'Please provide Email Address').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        console.log("req.body: ", req.body);
        users.updateUser(req.params.user_id, req.body, cb.setupResponseCallback(res));
    }
};


exports.updateUserAccount = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();

    req.checkBody('username', 'Please provide Username').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.updateUserAccount(req.params.user_id, req.body, cb.setupResponseCallback(res));
    }
};


exports.updateUserName = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();
    req.checkBody('username', 'Please provide Username').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.updateUserName(req.params.user_id, req.body, cb.setupResponseCallback(res));
    }
};

exports.updateUserPassword = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.updateUserPassword(req.params.user_id, req.body, cb.setupResponseCallback(res));
    }
};

exports.checkUserStatus = (req, res) => {
    req.checkParams('code', 'Please provide Verification parameters').notEmpty();
    req.checkParams('uuid', 'Please provide UUID parameters').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.checkUserStatus(req.params.code, req.params.uuid, cb.setupResponseCallback(res));
    }
};

exports.forgotPassword = (req, res) => {
    req.checkBody('email', 'Please provide your email address').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    const errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.forgotPassword(req.body, cb.setupResponseCallback(res));
    }
};

exports.resetPassword = (req, res) => {
    req.checkBody('uuid', 'Please provide UUID').notEmpty();
    req.checkBody('code', 'Please provide Verification').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.resetPassword(req.body, cb.setupResponseCallback(res));
    }
};

exports.checkResetPassword = (req, res) => {
    req.checkParams('code', 'Please provide Verification parameters').notEmpty();
    req.checkParams('uuid', 'Please provide UUID parameters').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        res.status(200).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.checkResetPassword(req.params.code, req.params.uuid, cb.setupResponseCallback(res));
    }
};

exports.resendEmailVerification = (req, res) => {
    req.checkParams('user_id', 'Please provide user_id parameters').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        users.resendEmailVerification(req.params.user_id, cb.setupResponseCallback(res));
    }
};