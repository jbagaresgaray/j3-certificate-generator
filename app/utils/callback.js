'use strict';

exports.setupResponseCallback = (res) => {
    return (error, returnValue) => {
        if (error) {
            return res.status(500).json(error);
        }

        return res.status(200).json(returnValue);
    };
};