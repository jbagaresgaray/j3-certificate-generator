'use strict';

const config = require('../../config/config');

const Database = function () {
    const self = this;

    self.configuration = {
        host: config.db_host || process.env.DB_HOST,
        user: config.db_user || process.env.DB_USER,
        password: config.db_password || process.env.DB_PASS,
        database: config.db_name || process.env.DB_NAME,
        port: config.db_port || process.env.DB_PORT,
        connectTimeout: 20000,
        dateStrings: true,
        multipleStatements: true
    };
};


exports.Database = Database;