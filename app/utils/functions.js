'use strict';

const config = require('../../config/config');

const fs = require('fs');
const ejs = require('ejs');
const multer = require('multer');
const mkdirp = require('mkdirp');
const uuidv1 = require('uuid/v1');

const existsSync = (filePath) => {
    if (!filePath) {
        return false;
    }
    try {
        return fs.statSync(filePath).isFile();
    } catch (e) {
        return false;
    }
};

const generateString = (length) => {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

const generateNumberString = (length) => {
    let text = '';
    const possible = '0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

const sendMail = (data, next) => {
    const request = require('request');
    const helper = require('sendgrid').mail;

    let objectData = {};

    if (data.attachment) {
        const file = request(data.attachment);
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: file
        };

    } else {
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: null
        };
    }

    const from_email = new helper.Email(objectData.from);
    const to_email = new helper.Email(objectData.to);
    const subject = objectData.subject;
    const content = new helper.Content('text/html', objectData.html);
    const mail = new helper.Mail(from_email, subject, to_email, content);

    if (objectData.attachment) {
        const attachment = new helper.Attachment();
        const sfile = fs.readFileSync(objectData.attachment);
        const base64File = new Buffer(sfile).toString('base64');
        attachment.setContent(base64File);
        attachment.setType('application/text');
        attachment.setFilename(objectData.filename);
        attachment.setDisposition('attachment');
        mail.addAttachment(attachment);
    }

    const sg = require('sendgrid')(config.sendgrid_key);
    const requests = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(requests, (error, response) => {
        if (error) {
            next(error, null);
        }
        if (response.statusCode === 200 || response.statusCode === 202) {
            next(null, {
                result: response.body,
                msg: 'Email Successfully Sent',
                success: true
            });
        } else {
            next({
                result: error,
                msg: error.message,
                success: false
            }, null);
        }
    });
};

const sendMailTemplate = (ejstemplate, data, from, email, subject) => {
    ejs.renderFile(ejstemplate, data, (err, html) => {
        if (err) {
            console.log(err); // Handle error
        }

        const mailData = {
            from: from,
            email: email,
            subject: subject,
            message: html,
            attachment: null
        };

        this.sendMail(mailData, (err, mailresponse) => {
            if (err) {
                console.log('err: ', err.body);
            }
            console.log('mailresponse: ', mailresponse);
        });
    });
};

const readJsonFileSync = (filepath, encoding) => {

    if (typeof(encoding) == 'undefined') {
        encoding = 'utf8';
    }
    let file = fs.readFileSync(filepath, encoding);
    return JSON.parse(file);
};

const padNumber = (num) => {
    var s = "000000" + num;
    return s.substr(s.length - 7);
}

const uploadFileLimits = () => {
    return {
        fileSize: 1024 * 1024 * 10 // 10MB
    };
}

const uploadDirectory = (directory, req, res) => {
    return multer.diskStorage({
        destination: (req, file, cb) => {
            mkdirp(directory, (err) => {
                cb(err, directory);
            });
        },
        filename: (req, file, cb) => {
            cb(null, uuidv1() + '_' + file.originalname);
        }
    });
};


const TRAINING_STATUS = {
    OPEN: "open",
    CLOSED: "closed",
    DONE: "done"
};

const SHIRT_SIZE = [{
    name: "XXSMALL",
    slug: "XXS"
}, {
    name: "XSMALL",
    slug: "XS"
}, {
    name: "SMALL",
    slug: "S"
}, {
    name: "MEDIUM",
    slug: "M"
}, {
    name: "LARGE",
    slug: "L"
}, {
    name: "XLARGE",
    slug: "XL"
}, {
    name: "XXLARGE",
    slug: "XXL"
}, {
    name: "XXXLARGE",
    slug: "XXXL"
}];

const PAGINATE_MAX_COUNT = 20;


exports.existsSync = existsSync;
exports.generateString = generateString;
exports.generateNumberString = generateNumberString;
exports.sendMail = sendMail;
exports.readJsonFileSync = readJsonFileSync;
exports.sendMailTemplate = sendMailTemplate;
exports.uploadFileLimits = uploadFileLimits;
exports.uploadDirectory = uploadDirectory;
exports.TRAINING_STATUS = TRAINING_STATUS;
exports.SHIRT_SIZE = SHIRT_SIZE;
exports.PAGINATE_MAX_COUNT = PAGINATE_MAX_COUNT;
exports.padNumber = padNumber;