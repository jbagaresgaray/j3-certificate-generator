'use strict';

module.exports = (server, config, log) => {
    server.listen(process.env.PORT || config.port, (err) => {
        if (err instanceof Error) {
            log.error('ENVIRONMENT: ' + config.env + ' Unable to start Server', app.get('port'));
        } else {
            log.info('ENVIRONMENT: ' + config.env + ' Server started at PORT: ' + config.port + ' Using API VERSION: ' + config.api_version + ' APP VERSION: ' + config.app_version);
            log.info('DB URL: ' + config.host + ' DATABASE: ' + config.database + ' PORT: ' + config.db_port +' API URL: ' + config.api_host_url + ' APP URL: ' + config.app_host_url);
        }
    });
};