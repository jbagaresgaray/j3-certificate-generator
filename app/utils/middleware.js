'use strict';

const config = require('../../config/config');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const _ = require('lodash');

const allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept, Cache-Control,AccessKey,AccessCode,Auth_Token,timezone,timezone2');
    // next();
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
};

const authorizeTokenAuth = (req, res, next) => {
    if (!req.header('authorization')) {
        return res.status(401).json({
            msg: 'Access to this service or resource is forbidden with the given authorization. Authorization header is missing!',
            result: null,
            success: false
        });
    }
    if (_.isEmpty(req.header('authorization'))) {
        return res.status(401).json( {
            msg: 'Authorization header must not be empty!',
            result: null,
            success: false
        });
    }

    let payload = null;
    try {
        const token = req.header('Authorization').split(' ')[1];
        payload = jwt.decode(token, config.token_secret_mobile);
        if (payload && payload.exp <= moment().unix()) {
            return res.status(401).send({
                msg: 'Token has expired',
                result: null,
                success: false
            });
        }
        delete payload.iat;
        delete payload.exp;
        delete payload.jit;
        delete payload.aud;
        req.user = {
            msg: 'Login successfully',
            success: true,
            result: {
                user: payload,
                token: token
            }
        };
        next();
    } catch (err) {
        console.log('authorizeMobileBasicAuth err: ', err);
        return res.status(403).send({
            msg: err.message,
            result: err,
            success: false
        });
    }
};

exports.allowCrossDomain = allowCrossDomain;
exports.authorizeTokenAuth = authorizeTokenAuth;