'use strict';

exports.validateCourse = function(req, res, next) {
	req.checkBody('courseName', 'Please specify coure name.' ).notEmpty();
	req.checkBody('courseCode', 'Please specify couse code.').notEmpty();

	var errors = req.validationErrors();
	if (errors) {
		res.status(400).send({
			response: {
				result: errors,
				msg: '',
				success: false
			},
			statusCode: 400
		});
	} else {
		next();
	}
};