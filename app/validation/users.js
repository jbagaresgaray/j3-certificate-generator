'use strict';


const _ = require("lodash");

exports.validateBasicAuth = (req, res, next) => {
    var auth = req.headers.authorization;

    var uname = {
        param: 'username',
        msg: 'Please provide your Username'
    };

    var pword = {
        param: 'password',
        msg: 'Please provide your Password'
    };

    if (!auth) {
        res.status(200).send({
            result: [uname, pword],
            msg: '',
            success: false
        });
    } else if (auth) {
        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();
        var creds = plain_auth.split(':');
        var username = creds[0];
        var password = creds[1];

        if (_.isEmpty(username) && _.isEmpty(password)) {
            res.status(200).send({
                result: [uname, pword],
                msg: '',
                success: false
            });
        } else if (_.isEmpty(username) && !_.isEmpty(password)) {
            res.status(200).send({
                result: [uname],
                msg: '',
                success: false
            });
        } else if (!_.isEmpty(username) && _.isEmpty(password)) {
            res.status(200).send({
                result: [pword],
                msg: '',
                success: false
            });
        } else if (username === 'undefined' && password === 'undefined') {
            res.status(200).send({
                result: [uname, pword],
                msg: '',
                success: false
            });
        } else if (!_.isEmpty(username) && password === 'undefined') {
            res.status(200).send({
                result: [pword],
                msg: '',
                success: false
            });
        } else if (username === 'undefined' && !_.isEmpty(password)) {
            res.status(200).send({
                result: [uname],
                msg: '',
                success: false
            });
        } else {
            next();
        }
    }
};