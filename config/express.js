/*jshint camelcase: false */

"use strict";

const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const methodOverride = require("method-override");
const middleware = require("../app/utils/middleware");
const flash = require("connect-flash");
const expressValidator = require("express-validator");
const helmet = require("helmet");
const path = require("path");
const compression = require('compression');

const { enableProdMode } = require("@angular/core");
// Express Engine
const { ngExpressEngine } = require("@nguniversal/express-engine");
// Import module map for lazy loading
const {
  provideModuleMap
} = require("@nguniversal/module-map-ngfactory-loader");

const DIST_FOLDER = path.join(process.cwd(), "site");
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
// const {
//   AppServerModuleNgFactory,
//   LAZY_MODULE_MAP
// } = require("../site/server/main");

module.exports = (main, app, api, config, passport) => {
  app.use(middleware.allowCrossDomain);
  main.use(app);

  // main.engine(
  //   "html",
  //   ngExpressEngine({
  //     bootstrap: AppServerModuleNgFactory,
  //     providers: [provideModuleMap(LAZY_MODULE_MAP)]
  //   })
  // );

  api.use(helmet());
  api.use(compression());
  api.use(expressValidator());
  api.use(middleware.allowCrossDomain);
  api.use(
    bodyParser.json({
      type: "application/json",
      limit: "50mb"
    })
  );
  api.use(
    bodyParser.urlencoded({
      extended: true,
      limit: "50mb"
    })
  );
  api.use(passport.initialize());
  api.use(passport.session());
  api.set('view engine', 'ejs');
  api.set('views', 'app/views/');


  main.use(config.api_version, api);
  main.use("/", express.static(path.join(__dirname, "..", "/site")));
  main.use(
    "/assets",
    express.static(path.join(__dirname, "..", "/site/assets"))
  );

  main.use("/public", express.static(__dirname + "./../public"));
  main.use("/docs", express.static(__dirname + "./../docs"));
  main.use("/uploads", express.static(__dirname + "./../public/uploads"));
  main.set('view engine', 'ejs');
  main.set('views', path.join(__dirname, "../app/views"));
  
  // main.set("view engine", "html");
  // main.set("views", "app/site");
  // main.set("views", path.join(DIST_FOLDER, "browser"));
  // app.set('view engine', 'html');
  // app.set('views', 'app/site')

  main.use(morgan("dev"));
  main.use(methodOverride());
  main.use(cookieParser());
  main.use(expressValidator());
  main.use(flash());
  main.use(compression());
  main.use(helmet());
  main.enable("trust proxy");
  main.use(passport.initialize());
  main.use(passport.session());

  /*process.on('uncaughtException', function(err) {
        console.log('Caught exception: ', err);
    });*/
};
