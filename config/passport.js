'use strict';


const Database = require('../app/utils/database').Database;
const db = new Database();
const func = require("../app/utils/functions");
const BasicStrategy = require('passport-http').BasicStrategy;
const _ = require('lodash');

const async = require('async');
const bcrypt = require('bcryptjs');
const uuidv1 = require('uuid/v1');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

module.exports = function (passport, jwt, config) {
    passport.use('user', new BasicStrategy(
        (username, password, done) => {
            knex('users').where('username', username).first().then(user => {
                if (_.isEmpty(user)) {
                    return done(null, {
                        msg: 'User does not exist with this user account.',
                        success: false,
                        result: ''
                    });
                }

                if (user.isVerify === 0) {
                    done(null, {
                        msg: 'Your account has not been VERIFIED.',
                        success: false,
                        result: ''
                    });
                } else {
                    bcrypt.compare(password, user.password, (err, res) => {
                        if (res) {
                            const payload = {
                                uuid: user.uuid,
                                email: user.email,
                                firstname: user.firstname,
                                lastname: user.lastname,
                                gender: user.gender,
                                birthday: user.birthday,
                                phone: user.phone,
                                img_name: user.user_img_path,
                                img_path: user.user_img_name,
                                img_type: user.user_img_type,
                                img_size: user.user_img_size
                            };

                            var token = jwt.sign(payload, config.token_secret, {
                                expiresIn: '1d',
                                issuer: config.token_secret,
                                jwtid: uuidv1()
                            });

                            return done(null, {
                                msg: 'Login successfully',
                                success: true,
                                result: {
                                    user: payload,
                                    token: token
                                }
                            });
                        } else {
                            return done(null, {
                                msg: 'Invalid Username or Password',
                                success: false,
                                result: ''
                            });
                        }
                    });
                }
            }, error => {
                return done(error);
            });
        }
    ));

    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });
};