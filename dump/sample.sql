CREATE DATABASE  IF NOT EXISTS `j3certificate` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `j3certificate`;


DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `courseId` INT NOT NULL AUTO_INCREMENT,
  `courseName` TEXT NULL,
  `courseCode` VARCHAR(60) NULL,
  `courseSlug` VARCHAR(100) NULL,
  `courseDesc` TEXT NULL,
  `courseHours` INT NULL,
  `date_created` TIMESTAMP NULL,
  `createdBy` INT NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`courseId`),
  INDEX `fk_course_uuid` (`uuid` ASC)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE TABLE `training` (
  `trainingId` INT NOT NULL AUTO_INCREMENT,
  `courseId` INT NULL,
  `trainingDesc` TEXT NULL,
  `trainingSchedFrom` TIMESTAMP NULL,
  `trainingSchedTo` TIMESTAMP NULL,
  `trainingVenue` TEXT NULL,
  `trainingSpeaker` VARCHAR(120) NULL,
  `trainingSpeakerSalutation` VARCHAR(120) NULL,
  `trainingSpeakerAccrNo` VARCHAR(60) NULL,
  `date_created` TIMESTAMP NULL,
  `createdBy` INT NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`trainingId`),
  INDEX `fk_training_courseId_idx` (`courseId` ASC),
  INDEX `fk_training_uuid` (`uuid` ASC),
  CONSTRAINT `fk_training_courseId`
    FOREIGN KEY (`courseId`)
    REFERENCES `course` (`courseId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `companies` (
  `companyId` INT NOT NULL AUTO_INCREMENT,
  `companyName` VARCHAR(100) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`companyId`),
  INDEX `fk_companies_uuidx` (`uuid` ASC))
ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `paymentType` (
  `paymentTypeId` INT NOT NULL AUTO_INCREMENT,
  `paymentType` VARCHAR(60) NULL,
  `date_created` TIMESTAMP NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`paymentTypeId`),
  INDEX `fk_paymentType_uuidx` (`uuid` ASC));

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;
