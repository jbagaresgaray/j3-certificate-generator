CREATE TABLE `account_classification` (
`accountClassId` INT NOT NULL AUTO_INCREMENT,
`class_code` VARCHAR(45) NULL,
`class_name` VARCHAR(250) NULL,
`class_description` VARCHAR(45) NULL,
`uuid` VARCHAR(45) NULL DEFAULT 'UUID()',
PRIMARY KEY (`accountClassId`));


CREATE TABLE `account_category` (
`account_categoryId` INT NOT NULL AUTO_INCREMENT,
`category_name` VARCHAR(100) NULL,
`category_code` VARCHAR(45) NULL,
`category_short` VARCHAR(45) NULL,
`category_description` VARCHAR(200) NULL,
`uuid` VARCHAR(45) NULL,
PRIMARY KEY (`account_categoryId`));


CREATE TABLE `account_groups` (
`account_groupId` INT NOT NULL AUTO_INCREMENT,
`group_code` VARCHAR(45) NULL,
`group_name` VARCHAR(100) NULL,
`group_short` VARCHAR(45) NULL,
`group_description` VARCHAR(200) NULL,
`uuid` VARCHAR(45) NULL,
PRIMARY KEY (`account_groupId`));


ALTER TABLE `account_classification`
ADD COLUMN `accountGroupID` INT NULL AFTER `uuid`,
ADD INDEX `fk_account_classification_accountGroupID_idx` (`accountGroupID` ASC);
ALTER TABLE `account_classification`
ADD CONSTRAINT `fk_account_classification_accountGroupID`
FOREIGN KEY (`accountGroupID`)
REFERENCES `account_groups` (`account_groupId`)
ON DELETE CASCADE
ON UPDATE CASCADE;
ALTER TABLE `account_classification`
CHANGE COLUMN `class_code` `class_code` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `class_name` `class_name` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `class_description` `class_description` VARCHAR(250) NULL DEFAULT NULL ;


CREATE TABLE `training_transactions` (
`training_transactions` INT NOT NULL AUTO_INCREMENT,
`trainingId` INT NULL,
`account_groupId` INT NULL,
`account_classId` INT NULL,
`TransDate` TIMESTAMP NULL,
`TransCode` VARCHAR(45) NULL,
`Referenceno` VARCHAR(45) NULL,
`Amount` DECIMAL(11,2) NULL,
`Remarks` VARCHAR(200) NULL,
`createdBy` INT NULL,
`createdOn` TIMESTAMP NULL,
`PostedBy` INT NULL,
`PostedDate` TIMESTAMP NULL,
`uuid` VARCHAR(45) NULL,
PRIMARY KEY (`training_transactions`),
INDEX `fk_training_transactions_trainingId_idx` (`trainingId` ASC),
INDEX `fk_training_transactions_account_groupId_idx` (`account_groupId` ASC),
INDEX `fk_training_transactions_account_classId_idx` (`account_classId` ASC),
CONSTRAINT `fk_training_transactions_trainingId`
FOREIGN KEY (`trainingId`)
REFERENCES `training` (`trainingId`)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT `fk_training_transactions_account_groupId`
FOREIGN KEY (`account_groupId`)
REFERENCES `account_groups` (`account_groupId`)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT `fk_training_transactions_account_classId`
FOREIGN KEY (`account_classId`)
REFERENCES `account_classification` (`accountClassId`)
ON DELETE CASCADE
ON UPDATE CASCADE);


CREATE TABLE `trainee_training_ledger` (
`traineeLedgerId` INT NOT NULL AUTO_INCREMENT,
`traineeId` INT NULL,
`trainingId` INT NULL,
`traineeNum` VARCHAR(45) NULL,
`paymentType` VARCHAR(45) NULL,
`paymentTransInfo` VARCHAR(60) NULL,
`amountPay` DECIMAL(10,2) NULL,
`orNumber` VARCHAR(45) NULL,
`paymentDate` TIMESTAMP NULL,
`paidBy` VARCHAR(100) NULL,
`receivedBy` INT NULL,
`receivingDate` TIMESTAMP NULL,
`date_created` TIMESTAMP NULL,
`uuid` VARCHAR(45) NULL,
PRIMARY KEY (`traineeLedgerId`));

ALTER TABLE `trainee_training_ledger`
ADD COLUMN `createdBy` INT NULL AFTER `receivingDate`,
ADD INDEX `fk_ trainee_training_ledger_traineeId_idx` (`traineeId` ASC),
ADD INDEX `fk_trainee_training_ledger_trainingId_idx` (`trainingId` ASC);
ALTER TABLE `trainee_training_ledger`
ADD CONSTRAINT `fk_ trainee_training_ledger_traineeId`
FOREIGN KEY (`traineeId`)
REFERENCES `trainees` (`traineesId`)
ON DELETE CASCADE
ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trainee_training_ledger_trainingId`
FOREIGN KEY (`trainingId`)
REFERENCES `training` (`trainingId`)
ON DELETE CASCADE
ON UPDATE CASCADE;


ALTER TABLE `training`
ADD COLUMN `trainingPrice` DECIMAL(10,2) NULL AFTER `trainingHours`;
ALTER TABLE `trainee_training_ledger` 
CHANGE COLUMN `receivedBy` `receivedBy` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `training_transactions` 
CHANGE COLUMN `TransCode` `TransCode` VARCHAR(150) NULL DEFAULT NULL ;


ALTER TABLE `trainees_images` 
ADD COLUMN `img_key` VARCHAR(150) NULL AFTER `img_name`,
ADD COLUMN `fileid` VARCHAR(45) NULL AFTER `traineesId`,
ADD COLUMN `folderid` VARCHAR(45) NULL AFTER `fileid`;


ALTER TABLE `payment_attachments` 
ADD COLUMN `img_key` VARCHAR(150) NULL AFTER `img_name`,
ADD COLUMN `fileid` VARCHAR(45) NULL AFTER `img_size`,
ADD COLUMN `folderid` VARCHAR(45) NULL AFTER `fileid`;


CREATE TABLE `training_reimbursement` (
  `training_reimbursementId` int(11) NOT NULL AUTO_INCREMENT,
  `trainingId` int(11) DEFAULT NULL,
  `traineeId` int(11) DEFAULT NULL,
  `traineeNum` varchar(45) DEFAULT NULL,
  `paymentType` varchar(45) DEFAULT NULL,
  `account_groupId` int(11) DEFAULT NULL,
  `account_classId` int(11) DEFAULT NULL,
  `TransDate` timestamp NULL DEFAULT NULL,
  `TransCode` varchar(150) DEFAULT NULL,
  `Referenceno` varchar(45) DEFAULT NULL,
  `orNumber` varchar(45) DEFAULT NULL,
  `Amount` decimal(11,2) DEFAULT NULL,
  `reimburseDate` timestamp NULL DEFAULT NULL,
  `Remarks` varchar(200) DEFAULT NULL,
  `receivedBy` varchar(100) DEFAULT NULL,
 `receivingDate` timestamp NULL DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT NULL,
  `PostedBy` int(11) DEFAULT NULL,
  `PostedDate` timestamp NULL DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`training_reimbursementId`),
  KEY `fk_training_reimbursementId_trainingId_idx` (`trainingId`),
  KEY `fk_training_reimbursementId_traineeId_idx` (`traineeId`),
  KEY `fk_training_reimbursementId_account_groupId_idx` (`account_groupId`),
  KEY `fk_training_reimbursementId_classId_idx` (`account_classId`),
  CONSTRAINT `fk_training_reimbursementId_traineeId` FOREIGN KEY (`traineeId`) REFERENCES `trainees` (`traineesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_training_reimbursementId_account_classId` FOREIGN KEY (`account_classId`) REFERENCES `account_classification` (`accountClassId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_training_reimbursementId_account_groupId` FOREIGN KEY (`account_groupId`) REFERENCES `account_groups` (`account_groupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_training_reimbursementId_trainingId` FOREIGN KEY (`trainingId`) REFERENCES `training` (`trainingId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `reimbursement_attachments` (
  `reimbursement_attachmentId` int(11) NOT NULL AUTO_INCREMENT,
  `traineeTrainingId` int(11) DEFAULT NULL,
  `img_name` text,
  `img_path` text,
  `img_type` varchar(45) DEFAULT NULL,
  `img_size` int(11) DEFAULT NULL,
  `fileid` varchar(45) DEFAULT NULL,
  `folderid` varchar(45) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`reimbursement_attachmentId`),
  KEY `fk_reimbursemen_files_trainee_trainingIdx_idx` (`traineeTrainingId`),
  CONSTRAINT `fk_reimbursemen_files_trainee_trainingIdx` FOREIGN KEY (`traineeTrainingId`) REFERENCES `trainee_trainings` (`traineeTrainingId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `reimbursement_attachments` 
ADD COLUMN `img_key` VARCHAR(150) NULL AFTER `img_name`;


CREATE VIEW `vw_training_transactions` AS
SELECT 
    NULL AS `training_transactions`,
    `trainee_training_ledger`.`trainingId` AS `trainingId`,
    `trainee_training_ledger`.`traineeId` AS `traineeId`,
    1 AS `account_groupId`,
    NULL AS `account_classId`,
    'ledger' AS `account_type`,
    `trainee_training_ledger`.`paymentDate` AS `TransDate`,
    CONCAT('Received payment from: ',
            `trainee_training_ledger`.`paidBy`,
            '.  Accepted payment by: ',
            `trainee_training_ledger`.`receivedBy`) AS `TransCode`,
    `trainee_training_ledger`.`paymentTransInfo` AS `Referenceno`,
    `trainee_training_ledger`.`amountPay` AS `Amount`,
    CONCAT('Received payment from: ',
            `trainee_training_ledger`.`paidBy`,
            ' with the amount of Php ',
            `trainee_training_ledger`.`amountPay`,
            ' in a form of ',
            UCASE(`trainee_training_ledger`.`paymentType`),
            ' and Accepted payment by: ',
            `trainee_training_ledger`.`receivedBy`,
            ' Accepted On: ',
            `trainee_training_ledger`.`receivingDate`) AS `Remarks`,
    `trainee_training_ledger`.`createdBy` AS `createdBy`,
    `trainee_training_ledger`.`date_created` AS `createdOn`,
    `trainee_training_ledger`.`createdBy` AS `PostedBy`,
    `trainee_training_ledger`.`date_created` AS `PostedDate`,
    `trainee_training_ledger`.`uuid` AS `uuid`
FROM
    `trainee_training_ledger` 
UNION SELECT 
    `t`.`training_transactions` AS `training_transactions`,
    `t`.`trainingId` AS `trainingId`,
    NULL AS `traineeId`,
    `t`.`account_groupId` AS `account_groupId`,
    `t`.`account_classId` AS `account_classId`,
    'transaction' AS `account_type`,
    `t`.`TransDate` AS `TransDate`,
    `t`.`TransCode` AS `TransCode`,
    `t`.`Referenceno` AS `Referenceno`,
    `t`.`Amount` AS `Amount`,
    `t`.`Remarks` AS `Remarks`,
    `t`.`createdBy` AS `createdBy`,
    `t`.`createdOn` AS `createdOn`,
    `t`.`PostedBy` AS `PostedBy`,
    `t`.`PostedDate` AS `PostedDate`,
    `t`.`uuid` AS `uuid`
FROM
    `training_transactions` `t` 
UNION SELECT 
    `tr`.`training_reimbursementId` AS `training_transactions`,
    `tr`.`trainingId` AS `trainingId`,
    `tr`.`traineeId` AS `traineeId`,
    `tr`.`account_groupId` AS `account_groupId`,
    `tr`.`account_classId` AS `account_classId`,
    'reimbursement' AS `account_type`,
    `tr`.`TransDate` AS `TransDate`,
    `tr`.`TransCode` AS `TransCode`,
    `tr`.`Referenceno` AS `Referenceno`,
    `tr`.`Amount` AS `Amount`,
    `tr`.`Remarks` AS `Remarks`,
    `tr`.`createdBy` AS `createdBy`,
    `tr`.`createdOn` AS `createdOn`,
    `tr`.`PostedBy` AS `PostedBy`,
    `tr`.`PostedDate` AS `PostedDate`,
    `tr`.`uuid` AS `uuid`
FROM
    `training_reimbursement` `tr`
