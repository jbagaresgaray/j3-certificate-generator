ALTER TABLE `trainee_trainings` 
DROP COLUMN `paymentDate`,
DROP COLUMN `orNumber`,
DROP COLUMN `amountPay`,
DROP COLUMN `isPaid`,
DROP COLUMN `paymentType`,
ADD COLUMN `trainingGrade` VARCHAR(45) NULL AFTER `traineeNum`,
ADD COLUMN `isAttended` TINYINT(4) NULL DEFAULT 0 AFTER `isPass`;


ALTER TABLE `trainee_trainings` 
ADD COLUMN `trainingGrade` VARCHAR(45) NULL AFTER `traineeNum`,
ADD COLUMN `isAttended` TINYINT(4) NULL DEFAULT 0 AFTER `isPass`;
