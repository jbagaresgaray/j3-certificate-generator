'use strict';

require('dotenv').config();

const env = process.env.NODE_ENV;
const application = require('./config/application'),
    express = require('express'),
    bunyan = require('bunyan'),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),
    middleware = require('./app/utils/middleware'),
    config = require('./config/config'),
    log = bunyan.createLogger({
        name: 'app_name_here'
    }),
    http = require('http'),
    https = require('https'),
    app = express(),
    api = express(),
    main = express();

let server = null;

if (config.env !== "development") {
    const fs = require('fs');
    const privateKey = fs.readFileSync('/etc/letsencrypt/live/4loop-api.com/privkey.pem', 'utf8');
    const certificate = fs.readFileSync('/etc/letsencrypt/live/4loop-api.com/cert.pem', 'utf8');
    const ca = fs.readFileSync('/etc/letsencrypt/live/4loop-api.com/chain.pem', 'utf8');
    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };
    server = https.createServer(credentials, main);
} else {
    server = http.createServer(main);
}



require(application.utils + 'helper')(server, config, log);
require(application.config + 'express')(main, app, api, config, passport);
require(application.config + 'passport')(passport, jwt, config);
// Routes
require(application.routes + 'documents')(main, middleware);
require(application.routes + '/')(main, api, config, passport);


module.exports = app;